var empty 			= /^\s*$/;
var email_format 	= /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
var number_format	= /^\d+$/;
var url_format      = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
var interval, countdown, process;

jQuery(function() {
	/* initialize tinymce */
	if (jQuery('.text-content')[0]) {
		initTinyMCE();
	}

	/* tooltip */
	$(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
	
	/* slug */
	if (jQuery('#slug')[0]) {
		jQuery(document).on('keyup', '#title', function() {
			slug = (this.value).toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-')
			jQuery('#slug').val(slug);
		});
	}

	// search
	jQuery(document).on('submit', '#search', function() {
		val = jQuery('#keyword').val();
	
        params = [{
            name: 'keyword',
            value: val
        }];

		if(val != '' || val2 != '') {
			jQuery.post(baseurl + module + '/data/' + method, params, function(data) {
				jQuery('#record').html(data);
				console.log(params);
			});
		} else {
			alert_countdown('Please enter a keyword', '.subnavs');
		}

		return false;
	});

	//new search
	jQuery(document).on('submit', '#newSearch', function() {
		val = jQuery('#keyword').val();
		var date_from = jQuery('#date_from').val(),
			date_to = jQuery('#date_to').val(),
			dealer = jQuery('#dealerFilter').val(),
			model = jQuery('#modelFilter').val();
	
        params = [
			{
				name: 'keyword',
				value: val
			},
			{
				name: 'date_from',
				value: date_from
			},
			{
				name: 'date_to',
				value: date_to
			},
			{
				name: 'dealer',
				value: dealer
			},
			{
				name: 'model',
				value: model
			},
		];
		
		// if(val != '' || val2 != '') {
			jQuery.post(baseurl + module + '/data/' + method, params, function(data) {
				jQuery('#record').html(data);
				console.log(params);
			});
		// } else {
			// alert_countdown('Please enter a keyword', '.subnavs');
		// }

		return false;
	});

	/*new clear search */
	jQuery(document).on('click', '#newClear', function() {
		jQuery('#keyword').val('');
		jQuery('#keyword2').val('');
		jQuery('#dealerFilter').val('');
		jQuery('#modelFilter').val('');

		jQuery.post(baseurl + module + '/data/' + method, {keyword : ''}, function(data) {
			jQuery('#record').html(data);
		});
	});

	/* clear search */
	jQuery(document).on('click', '#clear', function() {
		jQuery('#keyword').val('');
		jQuery('#keyword2').val('');

		jQuery.post(baseurl + module + '/data/' + method, {keyword : ''}, function(data) {
			jQuery('#record').html(data);
		});
	});
	

	/* get data onload */
	modules = [];
	if($.inArray(module, modules) < 0 && parseInt(method) > 0) {
		jQuery('#record').html('<small class="wait">Please wait...</small>');

		val = jQuery('#keyword').val();

		if(val != '') {
			params = {keyword : val}
		} else {
			params = '';
		}

		jQuery.post(baseurl + module + '/data/' + method, params, function(data) {
			jQuery('#record').html(data);
		});
	}


	// change password
	jQuery(document).on('click', '#changepw', function() {
		elem = jQuery(this);

		if(elem.text() == 'change') {
			elem.html('cancel');
			jQuery('#password small').before('<input class="req" type="password" id="pword" name="pword" title="Please enter password" /> ');
			jQuery('#pword').focus()
		} else {
			elem.html('change');
			jQuery('#password input').remove();
		}
	});

	// delete
	$(document).on('click', '.dels', function() {
        var data_id = $(this).data('id');
        var data_url = $(this).data('url');
        
        if(confirm("Are you sure yu want to delete this item?") == true){

            $.get(data_url,{id:data_id}, function(data){
                if(data!=0){
                	location.reload();
                }
                
            });
            
        }
    });

    // toggle status
	$(document).on('click', '.chgs', function() {
        var data_id = $(this).data('id');
        var data_url = $(this).data('url');       

        $.get(data_url,{id:data_id}, function(data){	
            if(data!=0){
            	location.reload();
            }
        });       
    });


	// add data
	jQuery('#addform').submit(function() {
		if(jQuery('.alert').length > 0) {
			jQuery('.alert').remove();
		}

		jQuery('#addform .req').each(function() {

			if(empty.test(jQuery(this).val())) {
				alert_countdown(jQuery(this).attr('title'), '#errMsg');
				return false;
			}

		});

		if(jQuery('.alert').length == 0) {
			save_data(method);
		}

		return false;
	});

	/* Slider imgae add  */
	if(module == 'sliders'){ 
		var counter = 0;
		counter = $('#mySlider .row').length + 1;
		$("#addSlider").on("click", function () {
			var newDiv = $("<div class='row'>");
			var cols = "";
			cols+='<div class="col-sm-4 form-group"><input type="file" class="form-control req upload" id="image[]" name="image[]" title="Please enter Slider Image"></div>';
			cols+='<div class="col-sm-6 form-group"><input type="text" class="form-control" id="link[]" name="link[]" title="Please enter URL"></div>';
			cols+='<div class="form-group col-sm-2"><a id="ibtnDel" class="btn btn-danger"><i class="fa fa-trash-o delete"></i> Delete</a></div>';
			newDiv.append(cols);        
			$("#mySlider").append(newDiv);
			counter++;
		});

		$("#mySlider").on("click", "#ibtnDel", function (event) {  
			$(this).closest("div .row").remove(); 
		});
	}

	if(module == 'dealers'){
		var counter = 0;
		counter = $('#myDealer .row').length + 1;
		$("#addDealer").on("click", function () {
			var newDiv = $("<div class='row'>");
			var cols = "";
			cols+='<div class="col-sm-4 form-group"><input type="test" class="form-control req" id="name[]" name="name[]" title="Please enter Dealer Name" placeholder="Dealer Name"></div>';
			cols+='<div class="col-sm-4 form-group"><input type="text" class="form-control req" id="location[]" name="location[]" title="Please enter Location" placeholder="Location"></div>';
			cols+='<div class="col-sm-3 form-group"><input type="text" class="form-control" id="contact[]" name="contact[]" title="Please enter Contact No." placeholder="Contact No."></div>';
			cols+='<div class="form-group col-sm-1" style="margin-top:5px"><a id="ibtnDel" class="btn btn-danger"><i class="fa fa-trash-o delete"></i></a></div>';
			newDiv.append(cols);        
			$("#myDealer").append(newDiv);
			counter++;
		});

		$("#myDealer").on("click", "#ibtnDel", function (event) {  
			$(this).closest("div .row").remove(); 
		});
	}

	if(module == 'stores'){
		$( ".sdate, .edate" ).datepicker({ dateFormat: "yy-mm-dd" }); 
		var counter = 0;
		$("#addStore").on("click", function () {
			counter = $('#myStore .row').length + 1;
			var newDiv = $("<div class='row'>");
			var cols = "";
			cols+='<div class="col-sm-5 form-group"><input type="text" class="form-control req" id="location[]" name="location[]" title="Please enter Location" placeholder="Location"></div>';
			cols+='<div class="col-sm-3 form-group"><div class="input-group margin-bottom-sm"><span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span><input class="form-control req sdate" type="text" id="s_date" name="start_date[]" title="Please enter Start Date" placeholder="Start Date" readonly></div></div>';
			cols+='<div class="col-sm-3 form-group"><div class="input-group margin-bottom-sm"><span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span><input class="form-control req edate" type="text" id="e_date" name="end_date[]" title="Please enter End Date" placeholder="End Date" readonly></div></div>';
			cols+='<div class="form-group col-sm-1" style="margin-top:5px"><a id="ibtnDel" class="btn btn-danger"><i class="fa fa-trash-o delete"></i></a></div>';
			newDiv.append(cols);      
			$("#myStore").append(newDiv);
			counter++;

			$('#s_date', newDiv).each(function(i) {
		        var newID = 's_date_' + counter;
		        $(this).attr('id', newID);
		        $("#"+newID ).datepicker({ dateFormat: "yy-mm-dd" });   
		    });

		    $('#e_date', newDiv).each(function(i) {
		        var newID = 'e_date_' + counter;
		        $(this).attr('id', newID);
		         $("#"+newID ).datepicker({ dateFormat: "yy-mm-dd" });   
		    });


		});

		$("#myStore").on("click", "#ibtnDel", function (event) {  
			$(this).closest("div .row").remove(); 
		});

	}

	if(module == 'vehicles'){ 
		var counter = 0;
		counter = $('#prodFeature .row').length + 1;
		$("#addFeature").on("click", function () {
		  var newDiv = $("<div class='row'>");
		  var cols = "";
		  cols+='<div class="form-group col-sm-3"><input class="form-control req" type="text" name="name[]" title="Please enter Name" id="name[]" placeholder="Name" value="" /></div>';
		  cols+='<div class="form-group col-sm-4"><textarea class="form-control req" cols="5" rows="10" name="desc[]" id="desc[]" title="Please enter Description" placeholder="Description"></textarea></div>';
		  cols+='<div class="form-group col-sm-3"><input class="form-control upload4 req" type="file" name="image[]" id="image[]" title="Please enter Image"  placeholder="Image" value=""><span style="font-size:12px;"><i>Required dimension 939x572</i></span></div>';
		  cols+='<div class="form-group col-sm-2"><a id="ibtnDel" class="btn btn-danger"><i class="fa fa-trash-o delete"></i> Delete</a></div>';
		  newDiv.append(cols);        
		  $("#prodFeature").append(newDiv);
		  counter++;
		});

		$("#prodFeature").on("click", "#ibtnDel", function (event) {  
		    $(this).closest("div .row").remove(); 
		});
	}
	
	if(module == 'articles'){
		if($('#teaser').val()){
    	 	var words = $('#teaser').val().match(/\S+/g).length;
    	 	var rem = 40 - words;
	        $('#word_left').html(rem + ' words remaining');
    	 }

		 $("#teaser").on('keyup', function() {
		 	var words = this.value.match(/\S+/g).length;
	        if (words > 40) {
	        	var trimmed = $(this).val().split(/\s+/, 40).join(" ");
	        	$(this).val(trimmed + " ");
	        }else{
	        	var rem = 40 - words;
	        	$('#word_left').html(rem + ' words remaining');
	        }
		});
	}

	// modal 
	$(document).on('click', '.popModal', function() {
        var data_id = $(this).data('id');
        var data_url = $(this).data('url');        
        $.get(data_url,{id:data_id}, function(data){	
             if(data){
             	$('#pop-content').html(data);
             	$('#form-dialog').modal('show') 
             }
        }); 
    });

    // FB description
    $('#fb_description').keyup(function() {
        var text_length = $('#fb_description').val().length;
        var text_remaining = 300 - text_length;

        $('#cntr').html(text_remaining + ' characters remaining');
    });

    if(module == 'histories'){
    	 if($('#title').val()){
    	 	var text_remaining = 50 - $('#title').val().length;
    	 	$('#cntr').html(text_remaining + ' characters remaining');
    	 }

    	 if($('#details').val()){
    	 	var words = $('#details').val().match(/\S+/g).length;
    	 	var rem = 50 - words;
	        $('#word_left').html(rem + ' words remaining');
    	 }

		 $("#details").on('keyup', function() {
		 	var words = this.value.match(/\S+/g).length;
	        if (words > 50) {
	        	var trimmed = $(this).val().split(/\s+/, 50).join(" ");
	        	$(this).val(trimmed + " ");
	        }else{
	        	var rem = 50 - words;
	        	$('#word_left').html(rem + ' words remaining');
	        }
		});

		$('#title').keyup(function() {
	        var text_length = $('#title').val().length;
	        var text_remaining = 50 - text_length;

	        $('#cntr').html(text_remaining + ' characters remaining');
	    });
	}

});

function delImage($id){
  if(confirm("Are you sure yu want to delete this image?") == true){
      var url = baseurl+module+'/delImage'
      $.get(url,{id:$id}, function(data){               
          //console.log(data);
          if(data!=0){
              location.reload();
          } 
      });
  }
}

function delFbImage($id){
  if(confirm("Are you sure yu want to delete this image?") == true){
      var url = baseurl+module+'/delFbImage'
      $.get(url,{id:$id}, function(data){               
          //console.log(data);
          if(data!=0){
              location.reload();
          } 
      });
  }
}

function delFile($id){
    if(confirm("Are you sure yu want to delete this file?") == true){
        var url = baseurl+module+'/delFile'
        $.get(url,{id:$id}, function(data){               
            //console.log(data);
            if(data!=0){
                location.reload();
            } 
        });
    }
}

function delImg($id,$name){
  if(confirm("Are you sure yu want to delete this image?") == true){
      var url = baseurl+module+'/delImage'
      $.get(url,{id:$id,name:$name}, function(data){               
          //console.log(data);
          if(data!=0){
              location.reload();
          } 
      });
  }
}

function save_data() {
	form = jQuery('#addform');
	save = jQuery('#submit');
	btns = jQuery('.btns');
	file = jQuery('.upload');
	file2 = jQuery('.upload2');
	file3 = jQuery('.upload3');
	file4 = jQuery('.upload4');
	pdf  = jQuery('.pdf');

	btns.hide();

	ser	   = form.serializeArray();
	ser.push({name : 'dataid', value : dataid});

	newdata = new FormData();

    jQuery.each(ser, function(i, v) {
    	newdata.append(v.name, v.value);
    });

    if(jQuery('.upload').length > 0) {
		file.each(function(i, v) {
	    	newdata.append('upload[]', file[i].files[0]);
	    });
	}

	if(jQuery('.upload2').length > 0) {
		file2.each(function(i, v) {
	    	newdata.append('upload2[]', file2[i].files[0]);
	    });
	}

	if(jQuery('.upload3').length > 0) {
		file3.each(function(i, v) {
	    	newdata.append('upload3[]', file3[i].files[0]);
	    });
	}

	if(jQuery('.upload4').length > 0) {
		file4.each(function(i, v) {
	    	newdata.append('upload4[]', file4[i].files[0]);
	    });
	}

	if(jQuery('.pdf').length > 0) {
		pdf.each(function(i, v) {
	    	newdata.append('pdf[]', pdf[i].files[0]);
	    });
	}

    if($(".multiple-upload").length > 0) {
    	var multiple_file = $(".multiple-upload");
    	multiple_file.each(function(i, v) {
    		var elem = $(this);
	    	for (var a = 0; a < multiple_file[i].files.length; a++) {
	    		newdata.append(elem.data('name') + '[]', multiple_file[i].files[a]);
	    	};
	    });
	}

	
	url = baseurl + module + '/process/' + method;

	jQuery.ajax({url            : url,
                 type           : 'POST',
                 data           : newdata,
                 processData    : false,
                 contentType    : false,
                 async          : true,
                 cache          : false,
                 success        : function(data) {
                 	 console.log(data);
                 	d = jQuery.parseJSON(data);
                    if(d.result == 0) {
						alert_countdown('Unable to save data', '#errMsg');
						btns.show();
						jQuery('.wait').remove();
					} else if(d.result == 'exist') {
						alert_countdown('The data is already exists', '#errMsg');
						btns.show();
						jQuery('.wait').remove();
					} else if(isNaN(d.result)) {
						alert_countdown( d.result, '#errMsg');
						btns.show();
						jQuery('.wait').remove();
					} else {
						alert_countdown('The data has been successfully saved', '#errMsg');
						btns.show();
						jQuery('.wait').remove();
						if (method == 'add' || method == 'update') {
							setTimeout(function() {
								redirect(baseurl + module );
							}, 3000);
						}
						
							if (typeof d.photos != 'undefined') {
							if ($(".multiple-file-list").length == 0) {
								$(".multiple-files").append('<ul class="multiple-file-list"></ul>');
								$(".no-data").remove();
							}

							$(".multiple-file-list").append(d.photos);

							$(".multiple-upload").val('');
						}
					}
                 }
    });

    return false;
}


function img_pathUrl(input){
   $('#img_url')[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);
}

// error message
function alert_countdown(msg, pos, fadeout) {
	$('html, body').animate({
        scrollTop: $("#errMsg").offset().top
    }, 1000);
	countdown = 3;
	jQuery(pos).prepend('<small class="alert">' + msg + ' [closing <span id="cd">' + countdown + '</span>]</small>');
	interval = setInterval(function() {
		countdown--;
		jQuery('#cd').html(countdown);
		if(countdown == 0) {
			jQuery('.alert').fadeOut();
			clearInterval(interval);
			if(fadeout == true) {
				jQuery(pos).fadeOut(1000);
			}
		}
	}, 1000);
}

// validates number input
function check_number(evt) {
    var char_code = (evt.which) ? evt.which : evt.keyCode
    if (char_code > 31 && (char_code < 48 || char_code > 57)) {
        return false;
    }
    return true;
}

function capitalize(s) {
    return s.toLowerCase().replace(/\b./g, function(a) { return a.toUpperCase(); });
}

// redirect
function redirect(url) {
	document.location.href = url;
}

function leading_zero(num, places) {
	var zero = places - num.toString().length + 1;
	return Array(+(zero > 0 && zero)).join("0") + num;
}

function initTinyMCE() {
	jQuery(".text-content").tinymce({
		script_url 				: mainurl + 'js/tinymce/tinymce.min.js',
		theme      				: "modern",
		mode       				: "textareas",
		menubar				    : false,
		plugins					: ["advlist autolink lists link image charmap print preview hr anchor pagebreak",
						           "searchreplace wordcount visualblocks visualchars code fullscreen",
						           "insertdatetime media nonbreaking save table contextmenu directionality",
						           "emoticons template paste textcolor colorpicker textpattern jbimages"],
		toolbar1				: "insertfile undo redo | bold italic underline |  forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image media link print code",
	    toolbar2				: "jbimages | styleselect",
		width      				: 850,
		height     				: 200,
		force_br_newlines 		: false,
        force_p_newlines		: true,
        forced_root_block 		: '',
        relative_urls    		: false,
        paste_as_text			: true,
        setup					: function(editor) {
	        
	    }
	});

}