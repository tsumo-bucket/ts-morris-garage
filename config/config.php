<?php
/*
Configuration file for all Facebook Applications of Carbon Digital, Inc.
This is a global class and methods.
*/
class Global_Config {

	function __construct() {
		# constants
		$this->app_constants();
	}

	# constants
	function app_constants() {
		// mysql config
		define('DB_USERNAME', 'root');
		define('DB_PASSWORD', '');
		define('DB_NAME', 'morris');

		// timezone
		$timezone = "Asia/Manila";
		date_default_timezone_set($timezone);

		$this->today = date('Y-m-d H:i:s', time());
		define('TODAY', $this->today);
		define('DATENOW', substr($this->today, 0, 10));

		// app constants
		define('RAND', 'v=' . rand(1000, 9999));
	}

}

# instanciate
$config = new Global_Config();

?>