<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
	    parent::__construct();

		if($this->session->userdata('logged_in') !== NULL){
			redirect('/pages');
		}
	}

	public function index(){
	    $this->load->model('userdata');

	    //check if form was submitted
		if ($this->input->post('login') == NULL) {

			$this->load->view('tpl/header');
			$this->load->view('login');
			$this->load->view('tpl/footer');
		
		}else{

			//form rules
			$this->form_validation->set_rules('username','UserName','required');
			$this->form_validation->set_rules('password','Password','required');

			if ($this->form_validation->run() == FALSE) {

				$this->load->view('tpl/header');
				$this->load->view('login');
				$this->load->view('tpl/footer');

			} else {
				
				//validate user
				$userdetails = $this->userdata->get_user($this->input->post('username'),$this->input->post('password'));

				if (isset($userdetails)) {

					$sessiondata = array(
						'user_id' => $userdetails->uid,
						'username' => $userdetails->username,
						'auth_level' => $userdetails->level,
						'logged_in' => TRUE
					);

					$this->session->set_userdata($sessiondata); 

					//redirect to pages
					redirect('/pages');

				} else {
					//error
					$errordata['error'] = 'Incorrect Username/Password.';
					$this->load->view('tpl/header');
					$this->load->view('login',$errordata);
					$this->load->view('tpl/footer');
					
				}
				
			}
		}
	}

}
