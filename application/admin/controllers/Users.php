<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    $this->load->model('userdata');
	    	   
	    if($this->session->userdata('logged_in') === NULL){
			redirect(base_url());
		}

		$this->module	= $this->uri->segment(1);	// shorten the segment
		$this->table	= 'tbl_users';				// table name
		$this->tbluid	= 'uid';					// uniq id of the table

	}

	public function index(){
		if($this->session->userdata('auth_level') != '1'){
			redirect(base_url());
		}
		
		$whr = 'isactive = 1';
		if($this->session->userdata('auth_level') != 1 ){
			$whr = 'isactive = 1 AND uid = "'.$this->session->userdata('user_id').'" ';
		}

		$this->db->where($whr);
		$pgs = $this->db->get($this->table);

		$nav = array('pgs'	 => $pgs->result());

		$data = array(
			'ptitle'	=> 'Manage Users',
			'navs'		=> $this->load->view('tpl/nav_content', $nav, true),
			'content'	=> $this->load->view($this->module.'/content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function data($cur) {

		if(!empty($_POST['keyword'])) {
			extract($_POST);
			$like 		= array('fname' => $keyword);
			$or_like	= array(
				'lname' => $keyword,
				'email' => $keyword,
				'username' => $keyword
			);

			$_SESSION['keyword'] = $keyword;
		} else {
			$like		= '';
			$or_like	= '';

			$_SESSION['keyword'] = '';
		}

		$limit = '20';

		$ps 	= $this->initials->pagination_settings($cur, $limit);

		if($this->session->userdata('auth_level') != 1 && $this->session->userdata('auth_level') != 2){
			$where = 'uid = "'.$this->session->userdata('user_id').'" ';
			$params = array('table'		=> $this->table,
							'fields'	=> $this->table . '.*,tbl_users_level.level_desc as lvlname',
							'join'		=> array(array('tbljoin'	=> 'tbl_users_level',
													   'onjoin'		=> 'tbl_users.level = tbl_users_level.level')),
							'like'		=> $like,
							'or_like'	=> $or_like,
							'where'		=> $where,
							'order' 	=> 'level ASC',
							'limit' 	=> $ps['limit'],
							'offset' 	=> $ps['offset']);
		}else{
			$params = array('table'		=> $this->table,
							'fields'	=> $this->table . '.*,tbl_users_level.level_desc as lvlname',
							'join'		=> array(array('tbljoin'	=> 'tbl_users_level',
													   'onjoin'		=> 'tbl_users.level = tbl_users_level.level')),
							'like'		=> $like,
							'or_like'	=> $or_like,
							'order' 	=> 'level ASC',
							'limit' 	=> $ps['limit'],
							'offset' 	=> $ps['offset']);
		}

		$d 		= $this->queries->get_data($params); 

		if($d) {
			if($d['rows'] > $ps['limit']) {
				$pagination = $this->initials->display_pagination($d['rows'], $ps['limit'], $cur, '' . $this->module);
			} else {
				$pagination = '';
			} 

			$data = array('data'		=> $d['data'],
						  'tbluid'		=> $this->tbluid,
						  'pagination'	=> $pagination);

			$this->load->view($this->module.'/data_content', $data, false);
		} else {
			echo '<br /><small>No records found.</small>';
		}
	}

	# add new page
	public function add(){
		$level	= $this->queries->get_data(array('table' 	=> 'tbl_users_level',
												 'fields'	=> 'level, level_desc',
												 'order' 	=> 'level ASC')); 

		$vars = array(
			'options' => $this->initials->generate_options($level),
			'level'   => $level
		);

		$data = array(
			'ptitle'	=> 'Add New User',
			'navs'		=> $this->load->view('tpl/nav_content', $vars, true),
			'content'	=> $this->load->view($this->module.'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	# update page
	public function update($id){
		$level	= $this->queries->get_data(array('table' 	=> 'tbl_users_level',
												 'fields'	=> 'level, level_desc',
												 'order' 	=> 'level ASC')); 

		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $id),
			'row'		=> true
		);

		$vars	= array(
			'row'        => $this->queries->get_data($params),
			'options' 	 => $this->initials->generate_options($level),
			'level'		 => $level
		);
		
		$data = array(
			'ptitle'	=> 'Update User',
			'navs'		=> $this->load->view('tpl/nav_content', $vars, true),
			'content'	=> $this->load->view($this->module.'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	# change status
	public function toggle($id){
		$status = $this->input->get('id');
		
		if($status == '1'){
			$stat = 0;
		}else{
			$stat = 1;
		}

		$static = array(
			'modified_by'	=> $this->session->userdata('user_id'),
			'modified_date'	=> date("Y-m-d H:i:s",strtotime("now")),
			'isactive' 		=> $stat
		);

		$result	 = $this->queries->update(
			array(
				'table'		=> $this->table,
				'data'		=> $static,
				'tbluid'	=> $this->tbluid,
			 	'dataid'	=> $id
			 )
		);
		 echo $result;
		
	}

	# delete data
	public function delete(){

		$result	 = $this->queries->delete(
			array(
				'table'		=> $this->table,
				'uniqid'	=> 'uid',
			 	'dataid'	=> $this->input->get('id')
			 )
		);

        echo $result;
    }

	# add and edit process
	function process($type) {
		$data = $_POST;
		unset($data['dataid']);
		
		# start validation
		$result = '';
		if(!empty($data['username'])) {
			$parms['fields'] = array(
				'uid'
			);
			$parms['conditions'] = array(
				'username' => $data['username']
			);

			$userinfo = $this->userdata->check_user($parms); 		
			if(!empty($userinfo)){
				if($userinfo->uid != $data['uid']  && $userinfo->uid){
					$result = '<br />Username already registered.';
				}
			}
		}		

		if(!$this->initials->email($data['email']) && $data['email']){
			$result.= '<br />Invalid Email format.';
		}

		if($this->initials->email($data['email']) && $data['email']){
			$parms['fields'] = array(
				'uid'
			);
			$parms['conditions'] = array(
				'email' => $data['email']
			);

			$userinfo = $this->userdata->check_user($parms); 		
			if(!empty($userinfo)){
				if($userinfo->uid != $data['uid']  && $userinfo->uid){
					$result.= '<br />Email Address already registered.';
				}
			}
		}

		if(!empty($data['password']) && !empty($data['cpassword'])){
			if($data['password'] != $data['cpassword']){
				$result .= '<br />Password did not match.';
			}
		}

		if(!empty($data['password']) && !empty($data['cpassword']) && !empty($data['uid'])){
			$parms['fields'] = array(
				'uid',
				'password'
			);
			$parms['conditions'] = array(
				'uid' => $data['uid']
			);
			$userinfo = $this->userdata->check_user($parms); 		
			if(!empty($userinfo)){
				if($userinfo->password == $data['password']  && $userinfo->uid){
					unset($data['password']);	
				}
			}
		}

		# end validation

		if(!empty($data['password'])) {
			$data['password'] = md5($data['password']);
		}
	
		$params = array(
			'table' 	=> $this->table,
			'tbluid'	=> $this->tbluid
		);

		if(!$result){
			unset($data['cpassword']);	
			unset($data['uid']);	

			if($type == 'add') {
				$static = array(
					'inserted_by'	=> $this->session->userdata('user_id'),
					'inserted_date'	=> date("Y-m-d H:i:s",strtotime("now")),
					'browser' 		=> $_SERVER['HTTP_USER_AGENT'],
					'ip'			=> $this->input->ip_address()
				);

				$result	 = $this->queries->insert(array_merge($params, array('data' => array_merge($data, $static))));
				
				
			} else if($type == 'update') {
				$static = array(
					'modified_by'	=> $this->session->userdata('user_id'),
					'modified_date'	=> date("Y-m-d H:i:s",strtotime("now"))
				);

				$result	 = $this->queries->update(array_merge($params,array(
						'data'		=> array_merge($data, $static),
						'dataid'	=> $_POST['uid']
					))); 
			}
		} 

		$arr = array('result' => $result);

		echo json_encode($arr);
	}


}
