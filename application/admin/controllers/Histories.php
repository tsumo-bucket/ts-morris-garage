<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Histories extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	   
	    if($this->session->userdata('logged_in') === NULL){
			redirect(base_url());
		}

		$this->module	= $this->uri->segment(1);	// shorten the segment
		$this->table	= 'tbl_histories';			// table name
		$this->tbluid	= 'id';						// uniq id of the table
	}

	public function index(){ 
		$whr = 'status = 1';
		$this->db->where($whr);
		$pgs = $this->db->get($this->table);

		$nav = array('pgs'	 => $pgs->result());

		$data = array(
			'ptitle'	=> 'Manage Histories',
			'navs'		=> $this->load->view('tpl/nav_content', $nav, true),
			'content'	=> $this->load->view($this->module.'/content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function data($cur) {

		if(!empty($_POST['keyword'])) {
			extract($_POST);
			$like 		= array('year' => $keyword);
			$or_like	= array('name' => $keyword);

			$_SESSION['keyword'] = $keyword;
		} else {
			$like		= '';
			$or_like	= '';

			$_SESSION['keyword'] = '';
		}

		$limit = '20';

		$ps 	= $this->initials->pagination_settings($cur, $limit);

		$params = array('table'		=> $this->table,
						'fields'	=> '*', 
						'like'		=> $like,
						'or_like'	=> $or_like,
						'order' 	=> 'year DESC',
						'limit' 	=> $ps['limit'],
						'offset' 	=> $ps['offset']);

		$d 		= $this->queries->get_data($params); 

		if($d) {
			if($d['rows'] > $ps['limit']) {
				$pagination = $this->initials->display_pagination($d['rows'], $ps['limit'], $cur, '' . $this->module);
			} else {
				$pagination = '';
			} 

			$data = array('data'		=> $d['data'],
						  'tbluid'		=> $this->tbluid,
						  'pagination'	=> $pagination);

			$this->load->view($this->module.'/data_content', $data, false);
		} else {
			echo '<br /><small>No records found.</small>';
		}
	}

	# add new page
	public function add(){
		$data = array(
			'ptitle'	=> 'Manage Histories',
			'navs'		=> $this->load->view('tpl/nav_content', '', true),
			'content'	=> $this->load->view($this->module.'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	# update page
	public function update($id){

		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $id),
			'row'		=> true
		);

		$vars	= array(
			'row'        => $this->queries->get_data($params)
		);
		
		$data = array(
			'ptitle'	=> 'Manage Histories',
			'navs'		=> $this->load->view('tpl/nav_content', $vars, true),
			'content'	=> $this->load->view($this->module .'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}


	# change status
	public function toggle($id){
		$status = $this->input->get('id');
		
		if($status == '1'){
			$stat = 0;
		}else{
			$stat = 1;
		}

		$static = array(
			'modified_by'	=> $this->session->userdata('user_id'),
			'modified_date'	=> date("Y-m-d H:i:s",strtotime("now")),
			'status' 		=> $stat
		);

		$result	 = $this->queries->update(
			array(
				'table'		=> $this->table,
				'data'		=> $static,
				'tbluid'	=> $this->tbluid,
			 	'dataid'	=> $id
			 )
		);
		 echo $result;
		
	}

	# delete data
	public function delete(){
		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $this->input->get('id')),
			'row'		=> true
		);

		$d = $this->queries->get_data($params);
		// unlink image cover
		@unlink('../uploads/images/milestones/'.$d->image);

		$result	 = $this->queries->delete(
			array(
				'table'		=> $this->table,
				'uniqid'	=> 'id',
			 	'dataid'	=> $this->input->get('id')
			 )
		);

        echo $result;
    }

	# add and edit process
	function process($type) {
		$data = $_POST;
		$file = $_FILES;
		unset($data['dataid']);
		unset($data['upload']);

		/* upload image */
		$img = '';
		if(!empty($_FILES["upload"]["name"])){
			$res = $this->_uploadResize();
			$img = $res['imgs'];
			if($res['error']){
				$arr = array('result' => $res['error']);
				echo json_encode($arr);
				exit();
			}
		}else if(isset($data) && @$data['image'] != null){
			$img = $data['image'];  
		}

		$params = array(
			'table' 	=> $this->table,
			'tbluid'	=> $this->tbluid
		);

		if($type == 'add') {
			$static = array(
				'image'			=> $img,
				'inserted_by'	=> $this->session->userdata('user_id'),
				'inserted_date'	=> date("Y-m-d H:i:s",strtotime("now"))
			);
			
			$result	 = $this->queries->insert(array_merge($params, array('data' => array_merge($data, $static))));
			
		} else if($type == 'update') {
			$static = array(
				'image'			=> $img,
				'modified_by'	=> $this->session->userdata('user_id'),
				'modified_date'	=> date("Y-m-d H:i:s",strtotime("now"))
			);

			$result	 = $this->queries->update(array_merge($params,array(
					'data'		=> array_merge($data, $static),
					'dataid'	=> $_POST['dataid']
				)));
		} 

		$arr = array('result' => $result);

		echo json_encode($arr);
	}

	private function _uploadResize() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			ini_set('memory_limit', '-1');
			$error = '';
			$imgs = '';
			$sep = '';

			$this->load->library('upload'); 

			$config['upload_path']   = realpath('../uploads/');
			$config['allowed_types'] = 'png|jpg|jpeg|gif';
			$config['overwrite']     = TRUE;
			$config['min_width'] 	 = '765';
			$config['min_height'] 	 = '550';
			$config['max_width'] 	 = '765';
			$config['max_height'] 	 = '550';

			for ($i = 0; $i < count($_FILES['upload']['name']); $i++){
			  $_FILES['image']['name']     = $_FILES['upload']['name'][$i];
		      $_FILES['image']['type']     = $_FILES['upload']['type'][$i];
		      $_FILES['image']['tmp_name'] = $_FILES['upload']['tmp_name'][$i];
		      $_FILES['image']['error']    = $_FILES['upload']['error'][$i];
		      $_FILES['image']['size']     = $_FILES['upload']['size'][$i];
		      $config['file_name']         = $_FILES['upload']['name'][$i];

		       $this->upload->initialize($config);

		       if (!$this->upload->do_upload('image','')){
					$error = $this->upload->display_errors();
				}else{
					$upload_data = $this->upload->data();
					$img_id = time().rand(100,999);
					$imgs = $img_id.'_'.$upload_data['file_name'];
					$this->load->library('image_lib'); 
			        $resize_conf = array(
			            'upload_path'  		=> realpath('../uploads/images/milestones/'),
			            'source_image' 		=> $upload_data['full_path'], 
			            'new_image'    		=> $upload_data['file_path'].'/images/milestones/'.$img_id.'_'.$upload_data['file_name'],
			            'maintain_ratio'   	=> TRUE,  
			            'width'        	   	=> 765,
			            'height'       		=> 550
			        );

			        $this->image_lib->initialize($resize_conf);		       
			        if ( !$this->image_lib->resize()){
			             $error = $this->image_lib->display_errors(); 					
			        }else{
			        	unlink('../uploads/'.$upload_data['file_name']);
			        }
					
				}
			}
			return array('error' => $error, 'imgs' => $imgs);
		}
	}

	# delete image
	public function delImage(){
		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $this->input->get('id')),
			'row'		=> true
		);

		$d = $this->queries->get_data($params);
		// unlink image cover
		@unlink('../uploads/images/milestones/'.$d->image);
		// update table
		$param = array(
			'table' 	=> $this->table,
			'tbluid'	=> $this->tbluid
		);	
		$static = array(
			'modified_by'	=> $this->session->userdata('user_id'),
			'modified_date'	=> date("Y-m-d H:i:s",strtotime("now")),
			'image'			=> ''
		);

		$result	 = $this->queries->update(array_merge($param,array(
				'data'		=> $static,
				'dataid'	=> $this->input->get('id')
			)));
		 echo $result;
		 exit();
	}

}
