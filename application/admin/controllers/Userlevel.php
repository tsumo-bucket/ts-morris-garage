<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userlevel extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    $this->load->model('userdata');
	    	   
	    if($this->session->userdata('logged_in') === NULL){
			redirect(base_url());
		}

		if($this->session->userdata('auth_level') != '1'){
			redirect(base_url());
		}

		$this->module	= $this->uri->segment(1);	// shorten the segment
		$this->table	= 'tbl_users_level';				// table name
		$this->tbluid	= 'id';					// uniq id of the table

	}

	public function index(){
		if($this->session->userdata('auth_level') != '1'){
			redirect(base_url());
		}
		$pgs = $this->db->get($this->table);

		$nav = array('pgs'	 => $pgs->result());

		$data = array(
			'ptitle'	=> 'Manage User Level',
			'navs'		=> $this->load->view('tpl/nav_content', $nav, true),
			'content'	=> $this->load->view($this->module .'/content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function data($cur) {

		if(!empty($_POST['keyword'])) {
			extract($_POST);
			$like 		= array('level_name' => $keyword);
			$or_like	= array(
				'level_desc' => $keyword
			);

			$_SESSION['keyword'] = $keyword;
		} else {
			$like		= '';
			$or_like	= '';

			$_SESSION['keyword'] = '';
		}

		$limit = '20';

		$ps 	= $this->initials->pagination_settings($cur, $limit);

		$params = array('table'		=> $this->table,
						'fields'	=> $this->table . '.*',
						'like'		=> $like,
						'or_like'	=> $or_like,
						'order' 	=> 'level ASC',
						'limit' 	=> $ps['limit'],
						'offset' 	=> $ps['offset']);

		$d 		= $this->queries->get_data($params); 

		if($d) {
			if($d['rows'] > $ps['limit']) {
				$pagination = $this->initials->display_pagination($d['rows'], $ps['limit'], $cur, '' . $this->module);
			} else {
				$pagination = '';
			} 

			$data = array('data'		=> $d['data'],
						  'tbluid'		=> $this->tbluid,
						  'pagination'	=> $pagination);

			$this->load->view($this->module.'/data_content', $data, false);
		} else {
			echo '<br /><small>No records found.</small>';
		}
	}

	# add new page
	public function add(){
		$data = array(
			'ptitle'	=> 'Add New User',
			'navs'		=> $this->load->view('tpl/nav_content', '', true),
			'content'	=> $this->load->view($this->module.'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	# update page
	public function update($id){
		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $id),
			'row'		=> true
		);

		$vars	= array(
			'row'        => $this->queries->get_data($params)
		);
		
		$data = array(
			'ptitle'	=> 'Manage Pages',
			'navs'		=> $this->load->view('tpl/nav_content', $vars, true),
			'content'	=> $this->load->view($this->module.'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	# delete data
	public function delete(){

		$result	 = $this->queries->delete(
			array(
				'table'		=> $this->table,
				'uniqid'	=> 'id',
			 	'dataid'	=> $this->input->get('id')
			 )
		);

        echo $result;
    }

	# add and edit process
	function process($type) {
		$data = $_POST;
		unset($data['dataid']);
		
		$params = array(
			'table' 	=> $this->table,
			'tbluid'	=> $this->tbluid
		);

		if($type == 'add') {
			$result	 = $this->queries->insert(array_merge($params, array('data' => $data)));
			// update level
			$static = array(
				'level'	=> $result
			);
			$upd	 = $this->queries->update(
				array(
					'table'		=> $this->table,
					'data'		=> $static,
					'tbluid'	=> $this->tbluid,
				 	'dataid'	=> $result
				 )
			);

		} else if($type == 'update') {

			$result	 = $this->queries->update(array_merge($params,array(
					'data'		=> $data,
					'dataid'	=> $_POST['id']
				)));
		} 

		$arr = array('result' => $result);

		echo json_encode($arr);
	}


}
