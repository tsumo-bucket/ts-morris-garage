<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dealers extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	   
	    if($this->session->userdata('logged_in') === NULL){
			redirect(base_url());
		}

		$this->module	= $this->uri->segment(1);	// shorten the segment
		$this->table	= 'tbl_dealers';			// table name
		$this->tbluid	= 'id';						// uniq id of the table
	}

	public function index(){ 
		$whr = 'status = 1';
		$this->db->where($whr);
		$pgs = $this->db->get($this->table);

		$nav = array('pgs'	 => $pgs->result());

		$data = array(
			'ptitle'	=> 'Manage Dealers',
			'navs'		=> $this->load->view('tpl/nav_content', $nav, true),
			'content'	=> $this->load->view($this->module.'/content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function data($cur) {

		if(!empty($_POST['keyword'])) {
			extract($_POST);
			$like 		= array('name' => $keyword);
			$or_like	= array('location' => $keyword);

			$_SESSION['keyword'] = $keyword;
		} else {
			$like		= '';
			$or_like	= '';

			$_SESSION['keyword'] = '';
		}

		$limit = '20';

		$ps 	= $this->initials->pagination_settings($cur, $limit);

		$params = array('table'		=> $this->table,
						'fields'	=> '*', 
						'like'		=> $like,
						'or_like'	=> $or_like,
						'order' 	=> 'inserted_date DESC',
						'limit' 	=> $ps['limit'],
						'offset' 	=> $ps['offset']);

		$d 		= $this->queries->get_data($params); 

		if($d) {
			if($d['rows'] > $ps['limit']) {
				$pagination = $this->initials->display_pagination($d['rows'], $ps['limit'], $cur, '' . $this->module);
			} else {
				$pagination = '';
			} 

			$data = array('data'		=> $d['data'],
						  'tbluid'		=> $this->tbluid,
						  'pagination'	=> $pagination);

			$this->load->view($this->module.'/data_content', $data, false);
		} else {
			echo '<br /><small>No records found.</small>';
		}
	}

	# add new page
	public function add(){
		$data = array(
			'ptitle'	=> 'Manage Dealers',
			'navs'		=> $this->load->view('tpl/nav_content', '', true),
			'content'	=> $this->load->view($this->module.'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	# update page
	public function update($id){

		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $id),
			'row'		=> true
		);

		$vars	= array(
			'row'        => $this->queries->get_data($params)
		);
		
		$data = array(
			'ptitle'	=> 'Manage Dealers',
			'navs'		=> $this->load->view('tpl/nav_content', $vars, true),
			'content'	=> $this->load->view($this->module .'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}


	# change status
	public function toggle($id){
		$status = $this->input->get('id');
		
		if($status == '1'){
			$stat = 0;
		}else{
			$stat = 1;
		}

		$static = array(
			'modified_by'	=> $this->session->userdata('user_id'),
			'modified_date'	=> date("Y-m-d H:i:s",strtotime("now")),
			'status' 		=> $stat
		);

		$result	 = $this->queries->update(
			array(
				'table'		=> $this->table,
				'data'		=> $static,
				'tbluid'	=> $this->tbluid,
			 	'dataid'	=> $id
			 )
		);
		 echo $result;
		
	}

	# delete data
	public function delete(){

		$result	 = $this->queries->delete(
			array(
				'table'		=> $this->table,
				'uniqid'	=> 'id',
			 	'dataid'	=> $this->input->get('id')
			 )
		);

        echo $result;
    }

	# add and edit process
	function process($type) {
		$data = $_POST;
		
		$params = array(
			'table' 	=> $this->table,
			'tbluid'	=> $this->tbluid
		);


		if(isset($data) && @$data['name'] != null){
			$ctr = 0;
			foreach ($data['name'] as $rec) {
				if($type == 'add') {
					$static = array(
						'status'		=> '1',
						'name'			=> $data['name'][$ctr],
						'location'		=> $data['location'][$ctr],
						'contact'		=> $data['contact'][$ctr],
						'inserted_by'	=> $this->session->userdata('user_id'),
						'inserted_date'	=> date("Y-m-d H:i:s",strtotime("now"))
					);
					
					$result	 = $this->queries->insert(array_merge($params, array('data' =>  $static)));	
					$ctr++;	
				}else{
					$static = array(
						'name'			=> $data['name'][$ctr],
						'location'		=> $data['location'][$ctr],
						'contact'		=> $data['contact'][$ctr],
						'modified_by'	=> $this->session->userdata('user_id'),
						'modified_date'	=> date("Y-m-d H:i:s",strtotime("now"))
					);

					$result	 = $this->queries->update(array_merge($params,array(
							'data'		=> $static,
							'dataid'	=> $_POST['dataid']
						)));
					$ctr++;	
				}	
			}
			$arr = array('result' => $result);
			echo json_encode($arr);
		}
	}

}
