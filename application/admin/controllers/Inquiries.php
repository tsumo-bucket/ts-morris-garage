<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inquiries extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	   
	    if($this->session->userdata('logged_in') === NULL){
			redirect(base_url());
		}

		$this->module	= $this->uri->segment(1);	// shorten the segment
		$this->table	= 'tbl_inquiries';				// table name
		$this->tbluid	= 'id';						// uniq id of the table
	}

	public function index(){ 
		$pgs = $this->db->get($this->table);

		$nav = array('pgs'	 => $pgs->result());

	    // get dealer 
		$params = array(
			'table'		=> 'tbl_dealers',
			'fields'	=> '*',
			'where'		=> array('status' => '1')
		);
		$dealers = $this->queries->get_data($params);

		$var = array(
			'dealers' 	=> $dealers['data'],
		);

		$data = array(
			'ptitle'	=> 'Manage Inquiries',
			'navs'		=> $this->load->view('tpl/nav_content', $nav, true),
			'content'	=> $this->load->view($this->module.'/content', $var, true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function data($cur) {

		$where = [];
		if(!empty($_POST['keyword']) ) {
			extract($_POST);
			$like 		= array('name' => $keyword);
			$or_like	= array(
				'contact' => $keyword,
				'email'	  => $keyword,
				'postcode' => $keyword,
				// 'DATE_FORMAT(inserted_date, "%M %d %Y")' => $keyword
			);

			$_SESSION['keyword'] = $keyword;
		} else {
			$like		= '';
			$or_like	= '';

			$_SESSION['keyword'] = '';
		}
		
		if (!empty($_POST['date_from']) && !empty($_POST['date_to'])) {
			$_SESSION['date_from'] = $_POST['date_from'];
			$_SESSION['date_to'] = $_POST['date_to'];
		} elseif (empty($_SESSION['date_from']) && empty($_SESSION['date_to'])) {
			$_SESSION['date_from'] = date('Y-m-d', strtotime('-30 days', strtotime(date('Y-m-d'))));
			$_SESSION['date_to'] = date('Y-m-d');
		}
				
		$where = [
			'DATE(inserted_date) <='=>$_SESSION["date_to"],
			'DATE(inserted_date) >='=>$_SESSION["date_from"],
			// 'DATE(inserted_date) <='=>$_SESSION['date_to']
		];

		if (!empty($_POST['dealer'])) {
			$where['dealer'] = $_POST['dealer']; 
			$_SESSION['dealer'] = $_POST['dealer'];
		} elseif ( !empty($_SESSION['dealer'])) {
			$where['dealer'] = $_SESSION['dealer']; 
			
			if (isset($_POST['dealer']) && $_POST['dealer'] == '') {
				unset($where['dealer']);
				unset($_SESSION['dealer']);
			}
		}

		if (!empty($_POST['model'])) {
			$where['model'] = $_POST['model']; 
			$_SESSION['model'] = $_POST['model'];
		} elseif ( !empty($_SESSION['model'])) {
			$where['model'] = $_SESSION['model']; 
		
			if (isset($_POST['model']) && $_POST['model'] == '') {
				unset($where['model']);
				unset($_SESSION['model']);
			}
		}

		
		$limit = '20';
		
		$ps 	= $this->initials->pagination_settings($cur, $limit);
		$params = array('table'		=> $this->table,
						'fields'	=> $this->table . '.*', 
						'like'		=> $like,
						'or_like'	=> $or_like,
						'where' 	=> $where,
						'order' 	=> 'inserted_date DESC',
						'limit' 	=> $ps['limit'],
						'offset' 	=> $ps['offset']);

		$d 		= $this->queries->get_data($params); 
		if($d) {
			if($d['rows'] > $ps['limit']) {
				$pagination = $this->initials->display_pagination($d['rows'], $ps['limit'], $cur, '' . $this->module);
			} else {
				$pagination = '';
			} 

			$data = array('data'		=> $d['data'],
						  'tbluid'		=> $this->tbluid,
						  'pagination'	=> $pagination);

			$this->load->view($this->module.'/data_content', $data, false);
		} else {
			echo '<br /><small>No records found.</small>';
		}
	}

	# delete data
	public function delete(){

		$result	 = $this->queries->delete(
			array(
				'table'		=> $this->table,
				'uniqid'	=> 'id',
			 	'dataid'	=> $this->input->get('id')
			 )
		);

        echo $result;
    }

    # view message pop up
    public function viewMessage(){

		$id = $this->input->get('id');

		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $id),
			'row'		=> true
		);

		$row  = $this->queries->get_data($params);

		$vars	= array(
			'row'        	=> $row
		);
		
		$data = array(
			'content'	=> $this->load->view($this->module.'/pop_content', $vars, true)
		);
	
		$this->load->view('tpl/pop_template', $data, false);
	}

	public function download(){
		$delimiter = ",";
	    $filename = "inquiries.csv";
	    //create a file pointer
	    $f = fopen('php://memory', 'w');
	    //set column headers
	    $fields = array('Name', 'Email', 'Contact', 'Postcode', 'Comments', 'Date of Inquiries', 'Dealer', 'Model');
	    fputcsv($f, $fields, $delimiter);
	    if(!empty($_SESSION['keyword'])) {
			$like 		= array('name' => $_SESSION['keyword']);
			$or_like	= array(
				'contact' => $_SESSION['keyword'],
				'email'	  => $_SESSION['keyword'],
				'postcode' => $_SESSION['keyword'],
				'DATE_FORMAT(inserted_date, "%M %d %Y")' => $_SESSION['keyword']
			);
		}else{
			$like		= '';
			$or_like	= '';
		}
		
		if (!empty($_POST['date_from']) && !empty($_POST['date_to'])) {
			$_SESSION['date_from'] = $_POST['date_from'];
			$_SESSION['date_to'] = $_POST['date_to'];
		} elseif (empty($_SESSION['date_from']) && empty($_SESSION['date_to'])) {
			$_SESSION['date_from'] = date('Y-m-d', strtotime('-30 days', strtotime(date('Y-m-d'))));
			$_SESSION['date_to'] = date('Y-m-d');
		}
		
		$where = [
			'DATE(inserted_date) <='=>$_SESSION["date_to"],
			'DATE(inserted_date) >='=>$_SESSION["date_from"],
			// 'DATE(inserted_date) <='=>$_SESSION['date_to']
		];

		if (!empty($_SESSION['dealer'])) {
			$where['dealer'] = $_SESSION['dealer']; 
		}
		
		if (!empty($_SESSION['model'])) {
			$where['model'] = $_SESSION['model']; 
		}
		// print_r($where);die();
	    $params = array('table'		=> $this->table,
						'fields'	=> 'name, email, contact, postcode, comments, inserted_date, dealer, model', 
						'like'		=> $like,
						'or_like'	=> $or_like,
						'where'		=> $where,
						'order' 	=> 'inserted_date DESC');

		$d 	= $this->queries->get_data($params);
		// print_r($this->db->last_query());print_r($_SESSION);die();
		foreach ($d['data'] as $data) {
			$lineData = array(($data['name']), $data['email'], $data['contact'], $data['postcode'], ($data['comments']), $data['inserted_date'], $data['dealer'], $data['model']);
			fputcsv($f, $lineData, $delimiter);
		}

		//move back to beginning of file
	    fseek($f, 0);
	    
	    //set headers to download file rather than displayed
	    header('Content-Type: text/csv');
	    header('Content-Disposition: attachment; filename="' . $filename . '";');
	    
	    //output all remaining data on a file pointer
	    fpassthru($f);
	}

}
