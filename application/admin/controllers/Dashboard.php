<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
	    parent::__construct();

	    if($this->session->userdata('logged_in') === NULL){
			redirect(base_url());
		}

		$this->module	= $this->uri->segment(1);	// shorten the segment
	}

	public function index(){

		$data = array(
			'ptitle'	=> 'Dashboard',
			'navs'		=> $this->load->view('nav_content', '', true),
			'content'	=> $this->load->view($this->module.'_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	

}
