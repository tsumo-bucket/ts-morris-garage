<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicles extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	   
	    if($this->session->userdata('logged_in') === NULL){
			redirect(base_url());
		}

		$this->module	= $this->uri->segment(1);	// shorten the segment
		$this->table	= 'tbl_vehicles';			// table name
		$this->tbluid	= 'id';						// uniq id of the table
	}

	public function index(){ 
		$whr = 'status = 1';
		$this->db->where($whr);
		$pgs = $this->db->get($this->table);

		$nav = array('pgs'	 => $pgs->result());

		$data = array(
			'ptitle'	=> 'Manage Vehicles',
			'navs'		=> $this->load->view('tpl/nav_content', $nav, true),
			'content'	=> $this->load->view($this->module.'/content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function data($cur) {

		if(!empty($_POST['keyword'])) {
			extract($_POST);
			$like 		= array('model' => $keyword);
			$or_like	= '';

			$_SESSION['keyword'] = $keyword;
		} else {
			$like		= '';
			$or_like	= '';

			$_SESSION['keyword'] = '';
		}

		$limit = '20';

		$ps 	= $this->initials->pagination_settings($cur, $limit);

		$params = array('table'		=> $this->table,
						'fields'	=> '*', 
						'like'		=> $like,
						'or_like'	=> $or_like,
						'order' 	=> 'inserted_date DESC',
						'limit' 	=> $ps['limit'],
						'offset' 	=> $ps['offset']);

		$d 		= $this->queries->get_data($params); 

		if($d) {
			if($d['rows'] > $ps['limit']) {
				$pagination = $this->initials->display_pagination($d['rows'], $ps['limit'], $cur, '' . $this->module);
			} else {
				$pagination = '';
			} 

			$data = array('data'		=> $d['data'],
						  'tbluid'		=> $this->tbluid,
						  'pagination'	=> $pagination);

			$this->load->view($this->module.'/data_content', $data, false);
		} else {
			echo '<br /><small>No records found.</small>';
		}
	}

	# add new page
	public function add(){
		$data = array(
			'ptitle'	=> 'Manage Vehicles',
			'navs'		=> $this->load->view('tpl/nav_content', '', true),
			'content'	=> $this->load->view($this->module.'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	# update page
	public function update($id){

		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $id),
			'row'		=> true
		);

		$vars	= array(
			'row'        => $this->queries->get_data($params)
		);
		
		$data = array(
			'ptitle'	=> 'Manage Vehicles',
			'navs'		=> $this->load->view('tpl/nav_content', $vars, true),
			'content'	=> $this->load->view($this->module .'/form_content', '', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	# change status
	public function toggle($id){
		$status = $this->input->get('id');
		
		if($status == '1'){
			$stat = 0;
		}else{
			$stat = 1;
		}

		$static = array(
			'modified_by'	=> $this->session->userdata('user_id'),
			'modified_date'	=> date("Y-m-d H:i:s",strtotime("now")),
			'status' 		=> $stat
		);

		$result	 = $this->queries->update(
			array(
				'table'		=> $this->table,
				'data'		=> $static,
				'tbluid'	=> $this->tbluid,
			 	'dataid'	=> $id
			 )
		);
		 echo $result;
		
	}

	# delete data
	public function delete(){

		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $this->input->get('id')),
			'row'		=> true
		);

		$d = $this->queries->get_data($params);
		// unlink images
		@unlink('../uploads/images/vehicles/'.$d->banner_image);
		@unlink('../uploads/images/vehicles/'.$d->overview_image);
		@unlink('../uploads/images/vehicles/'.$d->background_image);
		// unlink feature images
		$contents = json_decode($d->feature, TRUE);
		foreach ($contents as $content ) {
			@unlink('../uploads/images/vehicles/'.$content['image']);
		}
		// unlink brochure 
		@unlink('../uploads/brochures/'.$d->brochure);

		$result	 = $this->queries->delete(
			array(
				'table'		=> $this->table,
				'uniqid'	=> 'id',
			 	'dataid'	=> $this->input->get('id')
			 )
		);

        echo $result;
    }

	# add and edit process
	function process($type) {
		$data = $_POST;
		$file = $_FILES;
		unset($data['dataid']);
		unset($data['upload']);
		unset($data['upload2']);
		unset($data['upload3']);
		unset($data['upload4']);
		unset($data['pdf']);
		
		/* upload banner image */
		$banner_img = '';
		if(!empty($_FILES["upload"]["name"]) && empty($data['banner_image'])){
			$res = $this->_uploadResize('1920','885','upload');
			$banner_img = $res['imgs'];
			// if($res['error']){
			// 	$arr = array('result' => $res['error']);
			// 	echo json_encode($arr);
			// 	exit();
			// }
		}else if(isset($data) && @$data['banner_image'] != null){
			$banner_img = $data['banner_image'];  
		}

		/* upload overview image */
		$overview_img = '';
		if(!empty($_FILES["upload2"]["name"])){
			$res = $this->_uploadResize('464','254','upload2');
			$overview_img = $res['imgs'];
			// if($res['error']){
			// 	$arr = array('result' => $res['error']);
			// 	echo json_encode($arr);
			// 	exit();
			// }
		}else if(isset($data) && @$data['overview_image'] != null){
			$overview_img = $data['overview_image'];  
		}

		$brochure = '';
		if(!empty($_FILES["pdf"]["name"])){
			$res = $this->_upload();
			$brochure = $res['docs'];
			// if($res['error']){
			// 	$arr = array('result' => $res['error']);
			// 	echo json_encode($arr);
			// 	exit();
			// }
		}else{
			if(isset($data) && @$data['brochure'] != null){
				$brochure = $data['brochure'];
				unset($data['brochure']);
			}
		}

		/* upload background image */
		$background_img = '';
		if(!empty($_FILES["upload3"]["name"])){
			$res = $this->_uploadResize('1920','900','upload3');
			$background_img = $res['imgs'];
			// if($res['error']){
			// 	$arr = array('result' => $res['error']);
			// 	echo json_encode($arr);
			// 	exit();
			// }
		}else if(isset($data) && @$data['background_image'] != null){
			$background_img = $data['background_image'];  
		}

		/* upload form image */
		$form_img = '';
		if(!empty($_FILES["upload"]["name"])){
			$res = $this->_uploadResize('1900','885','upload');
			$form_img = $res['imgs'];
			// if($res['error']){
			// 	$arr = array('result' => $res['error']);
			// 	echo json_encode($arr);
			// 	exit();
			// }
		}else if(isset($data) && @$data['form_image'] != null){
			$form_img = $data['form_image'];  
		}

		// for update form
		if(!empty($_FILES["upload"]["name"]) && !empty($data['banner_image']) && empty($data['form_image'])){
			$res = $this->_uploadResize('1900','855','upload');
			$form_img = $res['imgs'];
			// if($res['error']){
			// 	$arr = array('result' => $res['error']);
			// 	echo json_encode($arr);
			// 	exit();
			// }
		}

		/* end form image */

		/* upload feature images */
		$feature_img = '';
		if(!empty($_FILES["upload4"]["name"])){
			$res = $this->_uploadResize('939','572','upload4');
			$feature_img = $res['imgs'];
			// if($res['error']){
			// 	$arr = array('result' => $res['error']);
			// 	echo json_encode($arr);
			// 	exit();
			// }
		}

		if(isset($data) && @$data['image'] != null){
			if($feature_img){
				$new_img = implode(',',$data['image']).','.$feature_img;
				$feature_img = $new_img;
			}else{
				$feature_img = implode(',',$data['image']);  
			}
		}

		$counter = array();
		if(isset($data) && @$data['name'] != null){
			foreach ($data['name'] as $key => $value) {
				$counter[] = $key;
			}
		}
		$details = '';
		if($feature_img){
			$vkeys  = array('name','details','image');
			$details = json_encode($this->my_new($counter, $vkeys, $data['name'], $data['desc'],explode(',',$feature_img)));
		}
		unset($data['name']);
		unset($data['desc']);
		unset($data['image']);

		$params = array(
			'table' 	=> $this->table,
			'tbluid'	=> $this->tbluid
		);

		if($type == 'add') {
			$static = array(
				'status'		 	=> '1',
				'banner_image'	 	=> $banner_img,
				'overview_image' 	=> $overview_img,
				'background_image'  => $background_img,
				'form_image'  => $form_img,
				'feature'			=> $details,
				'brochure'			=> $brochure,
				'inserted_by'	 	=> $this->session->userdata('user_id'),
				'inserted_date'	 	=> date("Y-m-d H:i:s",strtotime("now"))
			);

			$result	 = $this->queries->insert(array_merge($params, array('data' => array_merge($data, $static))));
			
		} else if($type == 'update') {
			$static = array(
				'banner_image'	 => $banner_img,
				'overview_image' => $overview_img,
				'background_image'  => $background_img,
				'form_image'  => $form_img,
				'feature'			=> $details,
				'brochure'			=> $brochure,
				'modified_by'	=> $this->session->userdata('user_id'),
				'modified_date'	=> date("Y-m-d H:i:s",strtotime("now"))
			);

			$result	 = $this->queries->update(array_merge($params,array(
					'data'		=> array_merge($data, $static),
					'dataid'	=> $_POST['dataid']
				)));
		} 

		$arr = array('result' => $result);

		echo json_encode($arr);
	}

	# delete image
	public function delImage(){
		$name = $this->input->get('name');
		
		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $this->input->get('id')),
			'row'		=> true
		);

		$d = $this->queries->get_data($params);
		// unlink image cover
		@unlink('../uploads/images/vehicles/'.$d->$name);
		// update table
		$param = array(
			'table' 	=> $this->table,
			'tbluid'	=> $this->tbluid
		);	
		$static = array(
			'modified_by'	=> $this->session->userdata('user_id'),
			'modified_date'	=> date("Y-m-d H:i:s",strtotime("now")),
			$name			=> ''
		);

		$result	 = $this->queries->update(array_merge($param,array(
				'data'		=> $static,
				'dataid'	=> $this->input->get('id')
			)));
		 echo $result;
		 exit();
	}

	# delete file
	public function delFile(){
		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> array($this->tbluid => $this->input->get('id')),
			'row'		=> true
		);

		$d = $this->queries->get_data($params);
		// unlink file 
		@unlink('../uploads/brochures/'.$d->brochure);
		
		// update table
		$param = array(
			'table' 	=> $this->table,
			'tbluid'	=> $this->tbluid
		);	
		$static = array(
			'modified_by'	=> $this->session->userdata('user_id'),
			'modified_date'	=> date("Y-m-d H:i:s",strtotime("now")),
			'brochure'		=> ''
		);
		
		$result	 = $this->queries->update(array_merge($param,array(
				'data'		=> $static,
				'dataid'	=> $this->input->get('id')
			)));

		echo $result;
		exit();
	}

	private function _uploadResize($width,$height,$name) {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			ini_set('memory_limit', '-1');
			$error = '';
			$imgs = '';
			$sep = '';

			$this->load->library('upload'); 

			$config['upload_path']   = realpath('../uploads/');
			$config['allowed_types'] = 'png|jpg|jpeg|gif';
			$config['overwrite']     = TRUE;
			$config['min_width'] 	 = $width;
			$config['min_height'] 	 = $height;

			for ($i = 0; $i < count($_FILES[$name]['name']); $i++){
			  $_FILES['image']['name']     = $_FILES[$name]['name'][$i];
		      $_FILES['image']['type']     = $_FILES[$name]['type'][$i];
		      $_FILES['image']['tmp_name'] = $_FILES[$name]['tmp_name'][$i];
		      $_FILES['image']['error']    = $_FILES[$name]['error'][$i];
		      $_FILES['image']['size']     = $_FILES[$name]['size'][$i];
		      $config['file_name']         = $_FILES[$name]['name'][$i];

		       $this->upload->initialize($config);

		       if (!$this->upload->do_upload('image','')){
					$error = $this->upload->display_errors();
				}else{
					$upload_data = $this->upload->data();
					$img_id = time().rand(100,999);
					$imgs.= $sep.$img_id.'_'.$upload_data['file_name'];
					$sep = ',';
					$this->load->library('image_lib'); 
			        $resize_conf = array(
			            'upload_path'  		=> realpath('../uploads/images/vehicles/'),
			            'source_image' 		=> $upload_data['full_path'], 
			            'new_image'    		=> $upload_data['file_path'].'/images/vehicles/'.$img_id.'_'.$upload_data['file_name'],
			            'maintain_ratio'   	=> TRUE,  
			            'width'        	   	=> $width,
			            'height'       		=> $height
			        );

			        $this->image_lib->initialize($resize_conf);		       
			        if ( !$this->image_lib->resize()){
			             $error = $this->image_lib->display_errors(); 					
			        }else{
			        	unlink('../uploads/'.$upload_data['file_name']);
			        }
					
				}
			}
			return array('error' => $error, 'imgs' => $imgs);
		}
	}

	private function _upload() {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	        $this->load->library('upload');

			$files                   = $_FILES;
			$config['upload_path']   = realpath('../uploads/brochures');
			$config['allowed_types'] = 'pdf';
			$config['max_size']      = 3 * 1024;
			$config['overwrite']     = TRUE;

			$error = '';
			$docs = '';
			$sep = '';
    		for ($a = 0; $a < count($files['pdf']['name']); $a++) {
				$_FILES['userfile']['name']     = $files['pdf']['name'][$a];
				$_FILES['userfile']['type']     = $files['pdf']['type'][$a];
				$_FILES['userfile']['tmp_name'] = $files['pdf']['tmp_name'][$a];
				$_FILES['userfile']['error']    = $files['pdf']['error'][$a];
				$_FILES['userfile']['size']     = $files['pdf']['size'][$a];

				$doc_id = time().rand(100,999);
				$config['file_name'] = $doc_id.'_'.$files['pdf']['name'][$a];
				$this->upload->initialize($config);
				
				if (!$this->upload->do_upload('userfile','')){
					$error = $this->upload->display_errors();
				}else{
					$upload_data = $this->upload->data();
					$docs.= $sep.$upload_data['file_name'];
					$sep = ',';
				}
		    }

		    return array('error' => $error, 'docs' => $docs);
		    
		}
	}

	public function my_new() {
	    $args    = func_get_args();
	    $keys    = array_shift($args);
	    $vkeys   = array_shift($args);
	    $results = array();

	    foreach($args as $key => $array) {
	        $vkey = array_shift($vkeys);

	        foreach($array as $akey => $val) {
	            $result[ $keys[$akey] ][$vkey] = $val;
	        }
	    }

	    return $result;
	}

}
