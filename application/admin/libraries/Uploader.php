<?php
set_time_limit(0);

class Uploader {
	function __construct() {
		$entries	= '../../photos/';
	}
	
	# start uploading
	function upload_now($dir, $file, $out) {
		// 2mb 2097152
		// 4mb 4194304
		if($file['size'] <= 2097152) {
			$output	= $dir . '/' . $out . '.jpg';
			$upload = $this->photo_uploader($file['tmp_name'], $output);	

			if($upload) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}	

	# make directories
	function create_user_directory($root_folder, $fbid = false) {
		# makes root directory for entries
		if(!file_exists($root_folder)) {
			if(!mkdir($root_folder, 0777, TRUE)) {
				die('Failed to create root folder');
			}
		}

		if($fbid) {
			if(!file_exists($root_folder . $fbid)) {
				if(!mkdir($root_folder . $fbid, 0777, TRUE)) {
					die('Failed to create user folder');
				}
			}
		}
	}	

	# process resize
	function photo_uploader($filename, $output) {
		$imginfo = getimagesize($filename);
		$imgtype = $imginfo[2];	

		if($imgtype == IMAGETYPE_JPEG) {
			$image = imagecreatefromjpeg($filename);
		} elseif($imgtype == IMAGETYPE_GIF) {
			$image = imagecreatefromgif($filename);
		} elseif($imgtype == IMAGETYPE_PNG) {
			$image = imagecreatefrompng($filename);
		}		

		$cw = imagesx($image);
		$ch = imagesy($image);	

		$n_image = imagecreatetruecolor($cw, $ch);
		imagecopyresampled($n_image, $image, 0, 0, 0, 0, $cw, $ch, $cw, $ch);
		
		$raw = imagejpeg($n_image, $output, 100);
		
		return $raw;
	}
}


?>