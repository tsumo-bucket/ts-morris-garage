<?php
class Userdata extends CI_Model {

    //auth user
    public function get_user($username, $password) {
        $this->db->select('uid,username,level');
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $this->db->where('isactive', TRUE);
        $this->db->cache_on();
        $query = $this->db->get('tbl_users');
        $this->db->cache_off();
        return $query->row();

    }

    public function check_user($params){
        $this->db->select($params['fields']);   
        $this->db->cache_on();
        $query = $this->db->get_where('tbl_users', $params['conditions']); 
        $this->db->cache_off();
        return $query->row();
    }

}
?>