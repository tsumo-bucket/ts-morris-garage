<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Initials extends CI_Model {

	function __construct() {
		parent::__construct();

		$this->page_validation();
	}

	function page_validation() {
		$nc = array('login', 'ajaxes', 'login-hq', 'reset-password');

		if(!in_array($this->uri->segment(1), $nc) && !$this->session->userdata('username')) {
			redirect('login', 'location');
		}
	}

	# this will validate email address format
	public function email($data){
		if($data){
			if(preg_match("/[a-zA-Z0-9_.-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/", $data)){
				return $data;
			}
		}
    }

	# this will generation options
	function generate_options($arr, $sel = false, $both = false) {
		$opts = '<option value="">- select -</option>';

		if($arr) {
			foreach($arr['data'] as $i => $a) {
				$keys = array_keys($a);

				if($sel == $a[$keys[0]]) {
					$s = ' selected="selected"';
				} else {
					$s = '';
				}

				if(count($keys) == 2) {
					if($both != false) {
						$opts .= '<option value="' . $a[$keys[0]] . '"' . $s . '>' . $a[$keys[0]] . ' - ' . stripslashes($a[$keys[1]]) . '</option>';
					} else {
						$opts .= '<option value="' . $a[$keys[0]] . '"' . $s . '>' . stripslashes($a[$keys[1]]) . '</option>';
					}
				} else {
					$opts .= '<option value="' . $a[$keys[0]] . '"' . $s . '>' . $a[$keys[0]] . '</option>';
				}
			}
		}

		return $opts;
	}

	# array to object
	function array_to_object($array) {
		foreach($array as $key => $value) {
			if(is_array($value)) {
				$array[$key] = $this->array_to_object($value);
			}
		}

		return (object)$array;
	}

	# pagination settings
	function pagination_settings($page, $limit = false) {
		$limit	= $limit ? $limit : 10; // limit rows per record
		# page computations
		$isnum		= is_numeric($page) ? $page : 1;
		$cur_page	= ($isnum < 1) ? 1 : $isnum;
		$offset		= ($cur_page - 1) * $limit;

		$ps['limit']	= $limit;
		$ps['offset']	= $offset;
		$ps['cur_page']	= $cur_page;

		return $ps;
	}

	# display pagination
	function display_pagination($total_rows, $limit, $curpage, $page_name) {
		// vars
		$p_int	= 4;
		$pages	= ceil($total_rows / $limit);
		$pp 	= (!empty($curpage)) ? $curpage : 1;

		// default value of pages
		$p_from	 = 1;
		$p_to	 = $pages;

		// page computations
		if($pages >= ($p_int * 2)) {
			if($pp + $p_int > $pages) {
				$p_from	 	= $pages - $p_int;
				$p_to		= $pages;

				if($pp < $pages) {
					$p_from = ($p_from - 1) + ($pp - $pages) + 1;
				}
			} else {
				$p_from		= $pp - $p_int;
				$p_to		= $pp + $p_int;
			}

			// this computation is for first pages.
			if($pp - $p_int < 1) {
				$p_from		= 1;
				$p_to		= ($p_int + 1);

				if($pp > 1) {
					$p_to	= ($p_int + 1) + ($pp - 1);
				}
			}
		}
		
		$page	= '<ul class="pagination pagination-md">';
		$page  .= '<li><a href="' . site_url($page_name . '/' . (($pp > 1) ? $pp - 1 : 1)) . '" title="Previous">&laquo;</a></li>';

		for($p = $p_from; $p <= $p_to; $p++) {
			if($pp == $p) { $selected = ' class="active"'; }
			else { $selected = ''; }

			$page .= '<li><a href="' . site_url($page_name . '/' . $p) . '"' . $selected . '>' . $p . '</a></li>';
		}

		$page  .= '<li><a href="' . site_url($page_name . '/' . (($pp < $pages) ? $pp + 1 : $pages)) . '">&raquo;</a></li>';
		
		$page  .= '</ul>';

		return $page;
	}

	public function getUserInfo($id){
        $whr = "uid = '".$id."' ";
        $this->db->where($whr);
        $query = $this->db->get('tbl_users');
        return $query->row();
    }

}

?>