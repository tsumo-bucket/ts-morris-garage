<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('Video')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control req" id="name" name="name" title="Please enter Name" placeholder="Name"  value="<?=isset($row) ? $row->name : ''?>">
                <input type="hidden" value="<?=isset($row) ? $row->id : ''?>" id="id" name="id"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Youtube Link</label>
                <input class="form-control req" type="text" id="link" name="link" title="Please enter Youtube Link" placeholder="Youtube Link" value="<?=isset($row) ? $row->link : ''?>" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Order</label>
                <input class="form-control req" type="text" id="order" name="order" title="Please enter Order" placeholder="Order" value="<?=isset($row) ? $row->order : ''?>" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input id="submit" type="submit" class="btn btn-primary"></button>
                <a href="<?=base_url()?>users" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </div>
</form>