<div class="content">
	<div class="row">
	    <div class="col-md-12">
	        <h1 class="page-header"><?=ucfirst($this->uri->segment(1))?></h1>
	    </div>
	</div>
	
	<div class="row">
		<div class="col-md-5">
			<a class="btn btn-primary btn-sm" href="<?=base_url().$this->uri->segment(1)?>/add"><i class="fa fa-plus fa-fw"></i> Add New</a> 
		</div>
	</div>
	
	<div id="record"></div>

</div>
