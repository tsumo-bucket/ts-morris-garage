		<table class="table table-hover">
		    <thead>
		        <tr>
			        <th width="50%">Image</th>
			        <th width="40%">URL</th>
			        <th width="10%">Actions</th>
		        </tr>
		    </thead>
		    <tbody id="sortThis"> 
		        <?php 
		        	$this->load->helper('directory'); 
                    $dir = "../uploads/images/sliders"; 
                    $map = directory_map($dir);  
                    $i = 0;  
		        	foreach ($data as $page) {?>
		        		<tr class="line-color" style="cursor: move" id="<?=$page['id']?>">
		        			<td><?php if($page['image']){ ?><img style="width: 60%" src="<?=base_url($dir).'/'.$page['image']?>" /><?php } ?></td>
		        			<td><?= $page['url']?></td>
		        			<td>		        				
				                <a href="javascript:void(0);" data-url="<?=base_url().$this->uri->segment(1).'/toggle/'.$page['id']?>" data-id="<?= $page['status'] ?>" data-toggle="tooltip" data-placement="top" title="Change Status" class="chgs"><?php if($page['status'] == 1){ echo '<i class="fa fa-toggle-on"></i>'; }else{ echo '<i class="fa fa-toggle-off"></i>'; } ?></a> &nbsp;
				                <a href="<?=base_url().$this->uri->segment(1).'/update/'.$page['id']?>"><span data-toggle="tooltip" data-placement="top" title="Update"><i class="fa fa-edit"></i></span></a> &nbsp;
                                <a href="javascript:void(0);" title="Delete" class="dels" data-url="<?=base_url().$this->uri->segment(1).'/delete'?>" data-id="<?= $page['id'] ?>" data-toggle="tooltip" data-placement="top" >
                                <i class="fa fa-trash delete"></i></a> &nbsp;
                            </td>
		        		</tr>
		        	<?php $i++; } // end foreach?>
		    </tbody>
		</table>
		<div class="col-md-12 text-center">
			<?= $pagination?>
		</div>
		<script language="JavaScript" type="text/javascript"> 
			jQuery(document).ready(function(){
			    jQuery("#sortThis").sortable({   
				    update : function (){ 
				       var order = $(this).sortable('toArray');
				       var url = baseurl+module+'/seqPos';
					    $.get(url,{ids:order}, function(data){
			                //console.log(data);  
			                /*
			                if(data!=0){
			                	location.reload();
			                } 
			                */   
			            });
				    }
			    });  
			});
		</script>