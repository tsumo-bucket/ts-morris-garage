<div class="row">
    <div class="col-sm-12">
        <h1 class="page-header lead"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('Slider')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <?php 
        $method = $this->uri->segment(2, 1); 
        if($method == 'add'){
    ?>
    <div id="mySlider">
        <div class="row">
            <div class="col-sm-4 form-group">
                <label>Slider Image</label> <span style="font-size:12px;"><i>Required dimension 1920x894</i></span>
                <input type="file" class="form-control req upload" id="image[]" name="image[]" title="Please enter Slider Image">
            </div>
            <div class="col-sm-6 form-group">
                <label>URL</label></span>
                <input type="text" class="form-control" id="link[]" name="link[]" title="Please enter URL Link">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-12">
            <a href="javascript:void(0);" id="addSlider"><i class="fa fa-plus"></i> Add Slider Image</i></a>
        </div>
    </div>
    <?php }else{ ?>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Slider Image</label> <span style="font-size:12px;"><i>Required dimension 1920x894</i></span>
            <?php
            if(isset($row) && $row->image){
            $this->load->helper('directory'); 
            $dir = "../uploads/images/sliders/"; 
            $map = directory_map($dir);                      
            echo '
            <input type="hidden" id="image[]" name="image[]" value="'.$row->image.'">
            <br /><img style="width:80%" src="'.base_url($dir).'/'.$row->image.'" />&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delImg('.$row->id.',\'image\')" class="btn btn-danger">Delete</a>';
            }else{ ?>
            <input type="file" class="form-control req upload" id="image[]" name="image[]" title="Please enter Slider Image">
            <?php } ?>
        </div>
        <div class="col-sm-6 form-group">
            <label>URL</label></span>
            <input type="text" class="form-control" id="link[]" name="link[]" value="<?=isset($row) ? $row->url : ''?>" title="Please enter URL Link">
        </div>
    <?php }?>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <input id="submit" type="submit" class="btn btn-primary"></button>&ensp;
            <a href="<?=base_url()?>sliders" class="btn btn-danger">Cancel</a>
        </div>
    </div>    
</form>
