<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$ptitle?></title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"><!-- date picker css !-->
<link href="<?=base_url()?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url()?>bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="<?=base_url()?>bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script>
var mainurl = '<?=base_url()?>';
var baseurl = '<?=site_url()?>';
var module	= '<?=$this->uri->segment(1)?>';
var method	= '<?=$this->uri->segment(2, 1)?>';
var dataid	= '<?=$this->uri->segment(3, '')?>';
</script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="<?=base_url()?>js/tinymce/jquery.tinymce.min.js"></script>

</head>

<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <?=$navs?>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                	<?=$content?>
                </div>

            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    
    <!-- /#wrapper -->
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=base_url()?>bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=base_url()?>bootstrap/metisMenu/dist/metisMenu.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="<?=base_url()?>bootstrap/dist/js/sb-admin-2.js"></script>
    <script src="<?=base_url()?>js/admin.js"></script>

</body>
</html>