
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand">Content Management System</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                    <a href="<?= base_url()?>users/update/<?= $this->session->userdata('user_id')?>"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="<?=base_url()?>logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        Welcome, <?= $this->session->userdata('username')?>!
                    </span>
                    </div>
                </li>
                <li>
                    <a href="<?=base_url()?>pages"><i class="fa fa-copy fa-fw"></i> Pages</a>
                </li>
                <li>
                    <a href="<?=base_url()?>histories"><i class="fa fa-history  fa-fw"></i> Histories</a>
                </li>
                <li>
                    <a href="<?=base_url()?>sliders"><i class="fa fa-sliders fa-fw"></i> Sliders</a>
                </li>
                <li>
                    <a href="<?=base_url()?>vehicles"><i class="fa fa-car fa-fw"></i> Vehicles</a>       
                </li>
                <li>
                    <a href="<?=base_url()?>articles"><i class="fa fa-newspaper-o fa-fw"></i> Articles</a>       
                </li>
                <li>
                    <a href="<?=base_url()?>dealers"><i class="fa fa-list fa-fw"></i> Dealers</a>       
                </li>
                <li>
                    <a href="<?=base_url()?>stores"><i class="fa fa-list fa-fw"></i> Pop-Up Stores & Roadshows</a>       
                </li>
                <li>
                    <a href="<?=base_url()?>inquiries"><i class="fa fa-info fa-fw"></i> Inquiries</a>       
                </li>
                <li>
                    <a href="<?=base_url()?>testdrive"><i class="fa fa-inbox fa-fw"></i> Test Drive</a>       
                </li>
                <li>
                    <a href="<?=base_url()?>videos"><i class="fa fa-newspaper-o fa-fw"></i> Videos</a>       
                </li>
                <li>
                    <a href="<?=base_url()?>contacts"><i class="fa fa-newspaper-o fa-fw"></i> Email Contacts</a>       
                </li>
                <?php if($this->session->userdata('auth_level') == 1 ){?>
                <li>
                    <a href="#"><i class="fa fa-folder-open-o fa-fw"></i> User Management<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?=base_url()?>users"><i class="fa fa-users fa-fw"></i> Users</a>
                        </li>
                        <li>
                            <a href="<?=base_url()?>userlevel"><i class="fa fa-sitemap fa-fw"></i> User Level</a>
                        </li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
