<div class="row">
    <div class="col-sm-12">
        <h1 class="page-header lead"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('Vehicle')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Model Name</label>
            <input type="text" class="form-control req" id="model" name="model" title="Please enter Model Name" placeholder="Model Name"  value="<?=isset($row) ? $row->model : ''?>">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Banner Image</label>
            <?php
            if(isset($row) && $row->banner_image){
            $this->load->helper('directory'); 
            $dir = "../uploads/images/vehicles/"; 
            $map = directory_map($dir);                      
            echo '
            <input type="hidden" id="banner_image" name="banner_image" value="'.$row->banner_image.'">
            <br /><img style="width:85%" src="'.base_url($dir).'/'.$row->banner_image.'" />&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delImg('.$row->id.',\'banner_image\')" class="btn btn-danger">Delete</a>';
            }else{ ?>
            <input type="file" class="form-control upload req" id="banner_image" name="banner_image" title="Please enter Banner Image"  value="<?=isset($row) ? $row->banner_image : ''?>">
            <span style="font-size:12px;"><i>Required dimension 1920x885</i></span>
            <?php } ?>
        </div>
        <div class="col-sm-6 form-group">
            <div>
                <label>Brochure (.PDF)</label>
                <?php 
                if(isset($row) && $row->brochure){
                    echo '
                    <input type="hidden" id="brochure" name="brochure" value="'.$row->brochure.'">
                    <br />';
                    echo $row->brochure.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delFile('.$row->id.')" class="btn btn-danger">Delete</a>';
                }else{ ?>
                    <input class="form-control pdf" type="file" id="file" name="file" />
                    <span style="font-size:12px;"><i>Max Size 3MB</i></span>
                <?php  } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Meta Tags</label>
            <input type="text" class="form-control" name="meta_tags" title="Please enter Meta Tags"  placeholder="Meta Tags" value="<?=isset($row) ? $row->meta_tags : ''?>">
            <span style="font-size:12px;"><i>use comma (,) as separator</i></span>
        </div>
        <div class="col-sm-6 form-group">
            <label>Meta Description</label>
            <input type="text" class="form-control" name="meta_desc" title="Please enter Meta Description" placeholder="Meta Description" value="<?=isset($row) ? $row->meta_desc : ''?>">
        </div>
    </div> 
    <div class="row">
        <div class="col-sm-12 page-header lead">Overview:</div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Details</label>
            <textarea class="text-content form-control req" name="overview_detail" title="Please enter Overview Details" rows="7"><?=isset($row) ? $row->overview_detail : ''?></textarea> 
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Image</label>
            <?php
            if(isset($row) && $row->overview_image){
            $this->load->helper('directory'); 
            $dir = "../uploads/images/vehicles/"; 
            $map = directory_map($dir);                      
            echo '
            <input type="hidden" id="overview_image" name="overview_image" value="'.$row->overview_image.'">
            <br /><img style="width:50%" src="'.base_url($dir).'/'.$row->overview_image.'" />&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delImg('.$row->id.',\'overview_image\')" class="btn btn-danger">Delete</a>';
            }else{ ?>
            <input type="file" class="form-control upload2 req" id="overview_image" name="overview_image" title="Please enter Overview Image"  value="<?=isset($row) ? $row->overview_image : ''?>">
            <span style="font-size:12px;"><i>Required dimension 464x254</i></span>
            <?php } ?>
        </div>
        <div class="col-sm-6 form-group">
            <label>Video</label>
            <textarea class="form-control" cols="5" rows="10" name="video" id="video" title="Please enter Embedded Video" placeholder="Embedded Video"><?=isset($row) ? $row->video : ''?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 page-header lead"><h3>Product Feature:</h3></div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Background Image</label>
            <?php
            if(isset($row) && $row->background_image){
            $this->load->helper('directory'); 
            $dir = "../uploads/images/vehicles/"; 
            $map = directory_map($dir);                      
            echo '
            <input type="hidden" id="background_image" name="background_image" value="'.$row->background_image.'">
            <br /><img style="width:85%" src="'.base_url($dir).'/'.$row->background_image.'" />&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delImg('.$row->id.',\'background_image\')" class="btn btn-danger">Delete</a>';
            }else{ ?>
            <input type="file" class="form-control upload3 req" id="background_image" name="background_image" title="Please enter Background Image"  value="<?=isset($row) ? $row->background_image : ''?>">
            <span style="font-size:12px;"><i>Required dimension 1920x900</i></span>
            <?php } ?>
        </div>
        <div class="col-sm-6 form-group">
            <div>
                <label>Form Image</label>
                <?php
                if(isset($row) && $row->form_image){
                $this->load->helper('directory'); 
                $dir = "../uploads/images/vehicles/"; 
                $map = directory_map($dir);                      
                echo '
                <input type="hidden" id="form_image" name="form_image" value="'.$row->form_image.'">
                <br /><img style="width:85%" src="'.base_url($dir).'/'.$row->form_image.'" />&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delImg('.$row->id.',\'form_image\')" class="btn btn-danger">Delete</a>';
                }else{ ?>
                <input type="file" class="form-control upload req" id="form_image" name="form_image" title="Please enter Banner Image"  value="<?=isset($row) ? $row->form_image : ''?>">
                <span style="font-size:12px;"><i>Required dimension 1900x855</i></span>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-3">
            <label>Name</label>
        </div>
        <div class="form-group col-sm-4">
            <label>Description</label>
        </div>      
        <div class="form-group col-sm-3">
            <label>Image</label>
        </div>
        <div class="form-group col-sm-2">
            <label>Action</label>
        </div>         
    </div>
    <div id="prodFeature">
        <?php if($this->uri->segment(2) == 'add'){ ?>
        <div class="row">
            <div class="form-group col-sm-3">
                <input class="form-control req" type="text" name="name[]" title="Please enter Name" id="name[]" placeholder="Name" value="" />
            </div>
            <div class="form-group col-sm-4">
                <textarea class="form-control req" cols="5" rows="10" name="desc[]" id="desc[]" title="Please enter Description" placeholder="Description"></textarea>
            </div>
            <div class="form-group col-sm-3">
                <input class="form-control upload4 req" type="file" name="image[]" id="image[]" title="Please enter Image"  placeholder="Image" value="">
                <span style="font-size:12px;"><i>Required dimension 939x572</i></span>
            </div>
            <div class="form-group col-sm-2">
               <a id="ibtnDel" class="btn btn-danger"><i class="fa fa-trash-o delete"></i> Delete</a>
            </div>  
        </div>
        <?php } ?>
        <?php
        if(isset($row) && $row->feature){
            $this->load->helper('directory'); 
            $dir = "../uploads/images/vehicles/"; 
            $map = directory_map($dir);  
            $contents = json_decode($row->feature, TRUE);
            $cnt = 0; 
            foreach ($contents as $content ) { ?>
            <div class="row">
                <div class="form-group col-sm-3">
                    <input class="form-control req" type="text" name="name[]" title="Please enter Name" id="name[]" placeholder="Name" value="<?=$content['name']?>" />
                </div>
                <div class="form-group col-sm-4">
                    <textarea class="form-control req" cols="5" rows="10" name="desc[]" id="desc[]" title="Please enter Description" placeholder="Description"><?=$content['details']?></textarea>
                </div>
                <div class="form-group col-sm-3">
                    <input type="hidden" name="image[]" id="image[]" value="<?= $content['image'] ?>" />
                    <img style="width: 100%;" src="<?=base_url($dir).$content['image']?>" />
                </div>
                <div class="form-group col-sm-2">
                   <a id="ibtnDel" class="btn btn-danger"><i class="fa fa-trash-o delete"></i> Delete</a>
                </div>
            </div>
            <?php   
            $cnt++;
            }
        }     
        ?>    
    </div>
    <div class="row">
        <div class="form-group col-sm-12">
            <a href="javascript:void(0);" id="addFeature"><i class="fa fa-plus"></i> Add Product Feature</i></a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <input id="submit" type="submit" class="btn btn-primary"></button>&ensp;
            <a href="<?=base_url()?>vehicles" class="btn btn-danger">Cancel</a>
        </div>
    </div>
</form>
