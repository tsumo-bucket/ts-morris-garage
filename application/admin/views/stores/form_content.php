<div class="row">
    <div class="col-sm-12">
        <h1 class="page-header lead"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('Pop-Up Store and Roadshow')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <?php 
        $method = $this->uri->segment(2, 1); 
        if($method == 'add'){
    ?>
    <div id="myStore">
        <div class="row">
            <div class="col-sm-5 form-group">
                <label>Location</label>
                <input type="text" class="form-control req" id="location[]" name="location[]" title="Please enter Location" placeholder="Location">
            </div>
            <div class="col-sm-3 form-group">
                <label>Start Date</label>
                <div class="input-group margin-bottom-sm"> 
                    <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span> 
                    <input class="form-control req sdate" type="text" id="s_date" name="start_date[]" title="Please enter Start Date" placeholder="Start Date" readonly=""> 
                </div>
            </div>
            <div class="col-sm-3 form-group">
                <label>End Date</label>
                <div class="input-group margin-bottom-sm"> 
                    <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span> 
                    <input class="form-control req edate" type="text" id="e_date" name="end_date[]" title="Please enter End Date" placeholder="End Date" readonly=""> 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-12">
            <a href="javascript:void(0);" id="addStore"><i class="fa fa-plus"></i> Add Pop-Up Store and Roadshow</i></a>
        </div>
    </div>
    <?php }else{ ?>
    <div class="row">
        <div class="col-sm-5 form-group">
            <label>Location</label>
            <input type="text" class="form-control req" id="location[]" name="location[]" value="<?=isset($row) ? $row->location : ''?>" title="Please enter Location" placeholder="Location">
        </div>
        <div class="col-sm-3 form-group">
            <label>Start Date</label>
            <div class="input-group margin-bottom-sm"> 
                <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span> 
                <input class="form-control req sdate" type="text" id="start_date[]" name="start_date[]" title="Please enter Start Date" placeholder="Start Date" value="<?=isset($row) ? $row->start_date : ''?>" readonly=""> 
            </div>
        </div>
        <div class="col-sm-3 form-group">
            <label>End Date</label>
            <div class="input-group margin-bottom-sm"> 
                <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span> 
                <input class="form-control req edate" type="text" id="end_date[]" name="end_date[]" title="Please enter End Date" placeholder="End Date" value="<?=isset($row) ? $row->end_date : ''?>" readonly=""> 
            </div>
        </div>
    <?php }?>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <input id="submit" type="submit" class="btn btn-primary"></button>&ensp;
            <a href="<?=base_url()?>stores" class="btn btn-danger">Cancel</a>
        </div>
    </div>    
</form>