<div class="row">
    <div class="col-sm-12">
        <h1 class="page-header lead"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('History')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <div id="myHistory">
        <div class="row">
            <div class="col-sm-2 form-group">
                <label>Year</label>
                <select class="form-control req" name="year" id="year" title="Please select Year">
                    <option value="">-- Select --</option>
                    <?php
                        for ($syear = 1924; $syear < $cyear = date('Y'); ) { ?>
                           <option value="<?= $syear ?>" <?=isset($row) ? $row->year == $syear ? 'selected' : '' : '' ?> ><?= $syear ?></option>         
                    <?php $syear++; } ?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 form-group">
                <label>Title</label>
                <input type="text" class="form-control req" id="title" name="title" title="Please enter Title" placeholder="Title"  value="<?=isset($row) ? $row->title : ''?>" maxlength="50"><span style="font-size:12px;"><i id="cntr">50 characters remaining</i></span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 form-group">
                <label>Description</label>
                <textarea class="form-control req" name="details" id="details" rows="7" title="Please enter Description" placeholder="Description"><?=isset($row) ? $row->details : ''?></textarea> 
                <span style="font-size:12px;"><i id="word_left">10 words remaining</i></span>
            </div>
            <div class="col-sm-6 form-group">
                <label>Image</label>
                <?php
                if(isset($row) && $row->image){
                $this->load->helper('directory'); 
                $dir = "../uploads/images/milestones/"; 
                $map = directory_map($dir);                      
                echo '
                <input type="hidden" id="image" name="image" value="'.$row->image.'">
                <br /><img style="width:50%" src="'.base_url($dir).'/'.$row->image.'" />&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delImage('.$row->id.')" class="btn btn-danger">Delete</a>';
                }else{ ?>
                <input type="file" class="form-control req upload" id="image" name="image" title="Please enter Image"  value="<?=isset($row) ? $row->image : ''?>">
                <span style="font-size:12px;"><i>Required dimension 765x550</i></span>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <input id="submit" type="submit" class="btn btn-primary"></button>&ensp;
            <a href="<?=base_url()?>histories" class="btn btn-danger">Cancel</a>
        </div>
    </div>    
</form>
