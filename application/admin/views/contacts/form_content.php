<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('Dealer Email Contacts')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Category</label>
                <input type="hidden" value="<?=isset($row) ? $row->id : ''?>" id="id" name="id"/>
                
				<select class="form-control req" name="category" id="category" required>
                    <option <?=(isset($row) && $row->category == "customer care") ? 'selected' : ''?> value="customer care">Customer Care</option>
                    <option <?=(isset($row) && $row->category == "inquiry") ? 'selected' : ''?> value="inquiry">Inquiry</option>
                    <option <?=(isset($row) && $row->category == "test drive") ? 'selected' : ''?> value="test drive">Test Drive</option>
				</select>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Dealer</label>
                <input type="hidden" value="<?=isset($row) ? $row->id : ''?>" id="id" name="id"/>
                
				<select class="form-control req" name="dealer" id="dealer" required>
					<option value="">Select Dealer</option>
					<?php foreach ($dealers as $dealer) { ?>
					    <option <?=(isset($row) && $row->dealer_id == $dealer['id']) ? 'selected' : ''?> value="<?= $dealer['id'].'--'.str_replace('*','',$dealer['name'])?>"><?= str_replace('*','',$dealer['name'])?></option>	
					<?php } ?>
				</select>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Email (comma separated)</label>
                <input class="form-control req" type="text" id="link" name="mail_to" title="Please enter email address/es" placeholder="Email Address/es" value="<?=isset($row) ? $row->mail_to : ''?>" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>CC (comma separated)</label>
                <input class="form-control" type="text" id="link" name="cc" title="Please enter email address/es" placeholder="Email Address/es" value="<?=isset($row) ? $row->cc : ''?>" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>BCC (comma separated)</label>
                <input class="form-control" type="text" id="link" name="bcc" title="Please enter email address/es" placeholder="Email Address/es" value="<?=isset($row) ? $row->bcc : ''?>" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input id="submit" type="submit" class="btn btn-primary"></button>
                <a href="<?=base_url()?>contacts" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </div>
</form>