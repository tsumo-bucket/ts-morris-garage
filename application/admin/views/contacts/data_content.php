<table class="table table-hover">
    <thead>
        <tr>
            <th width="90%">Dealer Name - Category</th>
            <th width="10%">Actions</th>
        </tr>
    </thead>
    <tbody> 
        <?php 
            foreach ($data as $page) {?>
                <tr class=" line-color ">
                    <td><?php echo $page['dealer_category'];?></td>
                    <td>		        				
                        <a href="javascript:void(0);" data-url="<?=base_url().$this->uri->segment(1).'/toggle/'.$page['id']?>" data-id="<?= $page['status'] ?>" data-toggle="tooltip" data-placement="top" title="Change Status" class="chgs"><?php if($page['status'] == 1){ echo '<i class="fa fa-toggle-on"></i>'; }else{ echo '<i class="fa fa-toggle-off"></i>'; } ?></a> &nbsp;
                        <a href="javascript:void(0);" title="Delete" class="dels" data-url="<?=base_url().$this->uri->segment(1).'/delete'?>" data-id="<?= $page['id'] ?>" data-toggle="tooltip" data-placement="top" >
                        <i class="fa fa-trash delete"></i></a> &nbsp;
                        <a href="<?=base_url().$this->uri->segment(1).'/update/'.$page['id']?>"><span data-toggle="tooltip" data-placement="top" title="Update"><i class="fa fa-edit"></i></span></a> &nbsp;

                    </td>
                </tr>
            <?php } // end foreach?>
    </tbody>
</table>
<div class="col-md-12 text-center">
    <?= $pagination?>
</div>