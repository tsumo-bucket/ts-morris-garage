<div class="row">
    <div class="col-sm-12">
        <h1 class="page-header"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('Article')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <div class="row">
        <div class="col-sm-4 form-group">
            <label>Type of Article</label>
            <select class="form-control req" id="type" name="type" title="Please select Type of Article">
                <option value="">-- Select --</option>
                <option value="news" <?php if(isset($row) && $row->type == 'news'){echo 'selected';}?> >News</option>
                <option value="activities" <?php if(isset($row) && $row->type == 'activities'){echo 'selected';}?> >Activities</option>
                <option value="promos" <?php if(isset($row) && $row->type == 'promos'){echo 'selected';}?> >Promos</option>
            </select>
        </div>
        <div class="col-sm-4 form-group">
            <label>Article Date</label>
            <div class="input-group margin-bottom-sm"> 
                <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span> 
                <input class="form-control req" type="text" id="article_date" name="article_date" title="Please enter Date" placeholder="Article Date" value="<?=isset($row) ? $row->article_date : ''?>" readonly=""> 
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 form-group">
            <label>Title</label>
            <input type="text" class="form-control req" id="title" name="title" title="Please enter Page Title" placeholder="Page title"  value="<?=isset($row) ? $row->title : ''?>">
            <input type="hidden" id="slug" name="slug" value="<?=isset($row) ? $row->slug : ''?>" />
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Cover Image</label>
            <?php
            if(isset($row) && $row->image){
            $this->load->helper('directory'); 
            $dir = "../uploads/images/articles/"; 
            $map = directory_map($dir);                      
            echo '
            <input type="hidden" id="image" name="image" value="'.$row->image.'">
            <br /><img style="width:80%" src="'.base_url($dir).'/'.$row->image.'" />&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delImage('.$row->id.')" class="btn btn-danger">Delete</a>';
            }else{ ?>
            <input type="file" class="form-control req upload" id="image" name="image" title="Please enter Cover Image"  value="<?=isset($row) ? $row->image : ''?>">
            <span style="font-size:12px;"><i>Required dimension 320x207</i></span>
            <?php } ?>
        </div>
        <div class="col-sm-6 form-group">
            <label>Teaser</label>
            <textarea class="form-control" name="teaser" id="teaser" rows="7" maxlength="300"><?=isset($row) ? $row->teaser : ''?></textarea>
            <span style="font-size:12px;"><i id="word_left">40 words remaining</i></span>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 form-group">
            <label>Details</label>
            <textarea class="text-content form-control req" name="details" rows="7"><?=isset($row) ? $row->details : ''?></textarea> 
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Meta title</label>
            <input type="text" class="form-control" name="meta_title" title="Please enter Meta Title"  placeholder="Meta Title" value="<?=isset($row) ? $row->meta_title : ''?>">
        </div>
    </div> 
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Meta Tags</label>
            <input type="text" class="form-control" name="meta_tags" title="Please enter Meta Tags"  placeholder="Meta Tags" value="<?=isset($row) ? $row->meta_tags : ''?>">
            <span style="font-size:12px;"><i>use comma (,) as separator</i></span>
        </div>
        <div class="col-sm-6 form-group">
            <label>Meta Description</label>
            <input type="text" class="form-control" name="meta_desc" title="Please enter Meta Description" placeholder="Meta Description" value="<?=isset($row) ? $row->meta_desc : ''?>">
        </div>
    </div> 
    <div class="row">
        <div class="col-sm-12 page-header">
            <b>FB Share</b>
        </div>
    </div>
    <div class="row">
        <!--div class="col-sm-6 form-group">
            <label>URL</label>
            <input type="text" class="form-control" id="fb_url" name="fb_url" title="Please enter URL Link" placeholder="URL Link"  value="<?=isset($row) ? $row->fb_url : ''?>">
        </div!-->
        <div class="col-sm-6 form-group">
            <label>Title</label>
            <input type="text" class="form-control" id="fb_title" name="fb_title" title="Please enter Title" placeholder="Title"  value="<?=isset($row) ? $row->fb_title : ''?>">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <label>Description</label>
            <textarea class="form-control" name="fb_description" id="fb_description" rows="7" maxlength="300"><?=isset($row) ? $row->fb_description : ''?></textarea><span style="font-size:12px;"><i id="cntr">300 characters remaining</i></span>
        </div>
        <div class="col-sm-6 form-group">
            <label>Image</label>
            <?php
            if(isset($row) && $row->fb_image){
            $this->load->helper('directory'); 
            $dir = "../uploads/images/articles/fbimages"; 
            $map = directory_map($dir);                      
            echo '
            <input type="hidden" id="fb_image" name="fb_image" value="'.$row->fb_image.'">
            <br /><img style="width:50%" src="'.base_url($dir).'/'.$row->fb_image.'" />&nbsp;&nbsp;<a href="javascript:void(0)" onclick="delFbImage('.$row->id.')" class="btn btn-danger">Delete</a>';
            }else{ ?>
            <input type="file" class="form-control upload2" id="fb_image" name="fb_image" title="Please enter Share Image"  value="<?=isset($row) ? $row->fb_image : ''?>">
            <span style="font-size:12px;"><i>Required dimension 1200x630</i></span>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <input id="submit" type="submit" class="btn btn-primary"></button>&ensp;
            <a href="<?=base_url()?>articles" class="btn btn-danger">Cancel</a>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () { 
        $( "#article_date" ).datepicker({ dateFormat: "yy-mm-dd" }); 
    });
</script>