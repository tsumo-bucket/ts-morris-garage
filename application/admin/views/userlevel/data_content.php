		<table class="table table-hover">
		    <thead>
		        <tr>
			        <th width="50%">User Leavel</th>
			        <th width="50%">Actions</th>
		        </tr>
		    </thead>
		    <tbody> 
		       <?php 
		        	foreach ($data as $userlevel) {?>
		        		<tr class=" line-color ">
		        			<td><?= $userlevel['level_desc'];?></td>
		        			<td>
		        				<?php if($userlevel['level'] != 1 && $userlevel['level'] != 2) {?>
                                <a href="javascript:void(0);" title="Delete" class="dels" data-url="<?=base_url().$this->uri->segment(1).'/delete'?>" data-id="<?= $userlevel['id'] ?>" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash delete"></i></a> &nbsp;
                                <?php }?>
                                <a href="<?=base_url().$this->uri->segment(1).'/update/'.$userlevel['id']?>"><span data-toggle="tooltip" data-placement="top" title="Update"><i class="fa fa-edit"></i></span></a> &nbsp;
                            </td>
		        		</tr>
		        	<?php } // end foreach?>
		    </tbody>
		</table>
		<div class="col-md-12 text-center">
			<?= $pagination?>
		</div>