<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('User Level')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>User Level Name</label>
                <input type="text" class="form-control req" id="level_desc" name="level_desc" title="Please User Level Name" placeholder="User Level Name"  value="<?=isset($row) ? $row->level_desc : ''?>">
                <input type="hidden" value="<?=isset($row) ? $row->id : ''?>" id="id" name="id"/>
                <input type="hidden" value="<?=isset($row) ? $row->level : ''?>" id="level" name="level"/>
                <input type="hidden" value="<?=isset($row) ? $row->level_name : ''?>" id="level_name" name="level_name"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input id="submit" type="submit" class="btn btn-primary"></button>
                <a href="<?=base_url()?>userlevel" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </div>
</form>