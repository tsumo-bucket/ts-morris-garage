<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <div class="container">
        <div class="row">
        	<div class="col-sm-12" id="logintitle"> <img src="<?php echo base_url(); ?>images/logo.png" class="center-block" /> </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 col-xs-12" id="loginform">
            	<h2>Login</h2>
				<?php
                    $form_attr = array('role' => 'form');
                    echo form_open('login',$form_attr);
                ?>
                <div class="form-group"> 
                	<input type="text" name="username" class="form-control input-lg" placeholder="Username">
                </div>
                <div class="form-group"> 
                	<input type="password" name="password" class="form-control input-lg" placeholder="Password">
                </div>
                <button type="submit" name="login" value="Login" class="btn btn-primary btn-lg btn-block">Sign in</button>
                <h4 class="text-center" style="color: #e30404;">
                	<?php if(isset($error)) { echo $error; } ?>
            	</h4>
            </div>
        </div>
    </div>
