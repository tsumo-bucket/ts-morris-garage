<div class="content">
	<div class="row">
	    <div class="col-md-12">
	        <h1 class="page-header"><?=ucfirst($this->uri->segment(1))?></h1>
	    </div>
	</div>
	
	<div class="row">
		<div class="col-md-5">
			<a class="btn btn-primary btn-sm" href="<?=base_url().$this->uri->segment(1)?>/add"><i class="fa fa-plus fa-fw"></i> Add New</a> 
		</div>
		<form method="post" id="search">
			<div class="col-md-5">
	            <div class="input-group margin-bottom-sm">
				  <span class="input-group-addon"><i class="fa fa-search fa-fw"></i></span>
				  <input class="form-control" type="text" id="keyword" value="<?=isset($_SESSION['keyword']) ? $_SESSION['keyword'] : ''?>" placeholder="Search...">
				</div>
			</div>
			<div class="col-md-2 text-left">
				<button type="submit" class="btn btn-default btn-sm" name="search">Submit</button>
				<button type="button" class="btn btn-default btn-sm" id="clear">Clear</button>
			</div>
		</form>
	</div>
	
	<div id="record"></div>

</div>
