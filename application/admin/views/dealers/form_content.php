<div class="row">
    <div class="col-sm-12">
        <h1 class="page-header lead"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('Dealer')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <?php 
        $method = $this->uri->segment(2, 1); 
        if($method == 'add'){
    ?>
    <div id="myDealer">
        <div class="row">
            <div class="col-sm-4 form-group">
                <label>Name</label>
                <input type="test" class="form-control req" id="name[]" name="name[]" title="Please enter Dealer Name" placeholder="Dealer Name">
            </div>
            <div class="col-sm-4 form-group">
                <label>Location</label>
                <input type="text" class="form-control req" id="location[]" name="location[]" title="Please enter Location" placeholder="Location">
            </div>
            <div class="col-sm-3 form-group">
                <label>Contact No.</label>
                <input type="text" class="form-control req" id="contact[]" name="contact[]" title="Please enter Contact No." placeholder="Contact No.">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-12">
            <a href="javascript:void(0);" id="addDealer"><i class="fa fa-plus"></i> Add Dealer</i></a>
        </div>
    </div>
    <?php }else{ ?>
    <div class="row">
        <div class="col-sm-4 form-group">
                <label>Name</label>
                <input type="test" class="form-control req" id="name[]" name="name[]" value="<?=isset($row) ? $row->name : ''?>" title="Please enter Dealer Name" placeholder="Dealer Name">
        </div>
        <div class="col-sm-4 form-group">
            <label>Location</label>
            <input type="text" class="form-control req" id="location[]" name="location[]" value="<?=isset($row) ? $row->location : ''?>" title="Please enter Location" placeholder="Location">
        </div>
        <div class="col-sm-3 form-group">
            <label>Contact No.</label>
            <input type="text" class="form-control" id="contact[]" name="contact[]" value="<?=isset($row) ? $row->contact : ''?>" title="Please enter Contact No." placeholder="Contact No.">
        </div>
    <?php }?>
    </div>
    <div class="row">
        <div class="col-sm-6 form-group">
            <input id="submit" type="submit" class="btn btn-primary"></button>&ensp;
            <a href="<?=base_url()?>dealers" class="btn btn-danger">Cancel</a>
        </div>
    </div>    
</form>
