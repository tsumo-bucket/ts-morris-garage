<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('Inquiry')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control req" id="title" name="title" title="Please enter Page Title" placeholder="Page title"  value="<?=isset($row) ? $row->title : ''?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Slug:</label>
                <input class="form-control req" type="text" id="slug" name="slug" title="Please enter Slug" placeholder="Slug" value="<?=isset($row) ? $row->slug : ''?>" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Description</label>
                <textarea class="text-content form-control" name="description" rows="7" ><?=isset($row) ? $row->description : ''?></textarea> 
            </div>
        </div>
    </div>
    <div clas="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Meta Title</label>
                <input type="text" class="form-control" name="meta_title" title="Please enter Meta Title"  placeholder="Meta Title" value="<?=isset($row) ? $row->meta_title : ''?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Meta Description</label>
                <input type="text" class="form-control" name="meta_desc" title="Please enter Meta Description" placeholder="Meta Description" value="<?=isset($row) ? $row->meta_desc : ''?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input id="submit" type="submit" class="btn btn-primary"></button>&ensp;
                <a href="<?=base_url()?>pages" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </div>
</form>
