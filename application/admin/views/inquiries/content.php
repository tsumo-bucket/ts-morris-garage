<div class="content">
	<div class="row">
	    <div class="col-md-12">
	        <h1 class="page-header"><?=ucfirst($this->uri->segment(1))?></h1>
	    </div>
	</div>
	
	
	<div class="row">
		<div class="col-md-12 margin-bottom-sm">
			<a class="btn btn-primary btn-sm" href="<?=base_url().$this->uri->segment(1)?>/download"><i class="fa fa-download fa-fw"></i> Download</a> 
		</div>
		<form method="post" id="newSearch">
			<div class="col-md-8 margin-bottom-md">
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<div class="input-group margin-bottom-sm">
								<label for="">From</label>
									<input name="date_from" max="<?=date('Y-m-d'); ?>" class="form-control" type="date" id="date_from" value="<?=isset($_SESSION['date_from']) ? $_SESSION['date_from'] : date('Y-m-d', strtotime('-30 days', strtotime(date('Y-m-d'))))?>" placeholder="Search...">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group margin-bottom-sm">
									<label for="">To</label>
									<input name="date_to" max="<?=date('Y-m-d'); ?>" class="form-control" type="date" id="date_to" value="<?=isset($_SESSION['date_to']) ? $_SESSION['date_to'] : date('Y-m-d')?>" placeholder="Search...">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<div class="input">
									<label for="">Dealer</label>
									<select class="form-control" name="dealer" id="dealerFilter">
										<option <?=(!isset($_SESSION['dealer']) || (isset($_SESSION['dealer']) && $_SESSION['dealer'] == "")) ? 'selected':'' ?> value="">All</option>
										<?php foreach ($dealers as $dealer) { ?>
											<option <?=(isset($_SESSION['dealer']) && $_SESSION['dealer'] == str_replace('*','',$dealer['name']))? 'selected':'' ?> value="<?= str_replace('*','',$dealer['name'])?>"><?= str_replace('*','',$dealer['name'])?></option>	
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="input">
									<label for="">Model</label>
									<select class="form-control" name="model" id="modelFilter">
										<option <?=(!isset($_SESSION['model']) || (isset($_SESSION['model']) && $_SESSION['model'] == "")) ? 'selected':'' ?> value="">All</option>
										<option <?=(isset($_SESSION['model']) && $_SESSION['model'] == "MG6") ? 'selected':'' ?> value="MG6">MG6</option>
										<option <?=(isset($_SESSION['model']) && $_SESSION['model'] == "MG ZS") ? 'selected':'' ?> value="MG ZS">MG ZS</option>
										<option <?=(isset($_SESSION['model']) && $_SESSION['model'] == "MG RX5") ? 'selected':'' ?> value="MG RX5">MG RX5</option>
										<option <?=(isset($_SESSION['model']) && $_SESSION['model'] == "MG 5") ? 'selected':'' ?> value="MG 5">New MG 5</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<label for="">&nbsp;</label>
	            <div class="input-group margin-bottom-sm">
				  <span class="input-group-addon"><i class="fa fa-search fa-fw"></i></span>
				  <input class="form-control" type="text" id="keyword" value="<?=isset($_SESSION['keyword']) ? $_SESSION['keyword'] : ''?>" placeholder="Search...">
				</div>
			</div>
			<div class="col-md-2 text-left">
				<label for="">&nbsp;</label>
				<div class="input">
					<button type="submit" class="btn btn-default btn-sm" name="search">Submit</button>
					<button type="button" class="btn btn-default btn-sm" id="newClear">Clear</button>
				</div>
			</div>
		</form>
	</div>
	
	<div id="record"></div>

</div>
<!-- Modal -->
<div id="form-dialog" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Message</h4>
			</div>
			<div id="pop-content">
			</div>
		</div>

	</div>
</div>
