		<table class="table table-hover">
		    <thead>
		        <tr>
			        <th width="15%">Userame</th>
			        <th width="30%">Full Name</th>
			        <th width="25%">Email Address</th>
			        <th width="20%">User Level</th>
			        <th width="10%">Actions</th>
		        </tr>
		    </thead>
		    <tbody> 
		       <?php 
		        	foreach ($data as $user) {?>
		        		<tr class=" line-color ">
		        			<td><?= $user['username'];?></td>
		        			<td><?= $user['fname'].' '.$user['lname'] ;?></td>
		        			<td><?= $user['email'];?></td>
		        			<td><?= $user['lvlname'];?></td>
		        			<td>		        				
				                <a href="javascript:void(0);" data-url="<?=base_url().$this->uri->segment(1).'/toggle/'.$user['uid']?>" data-id="<?= $user['isactive'] ?>" data-toggle="tooltip" data-placement="top" title="Change Status" class="chgs"><?php if($user['isactive'] == 1){ echo '<i class="fa fa-toggle-on"></i>'; }else{ echo '<i class="fa fa-toggle-off"></i>'; } ?></a> &nbsp;
                                <a href="javascript:void(0);" title="Delete" class="dels" data-url="<?=base_url().$this->uri->segment(1).'/delete'?>" data-id="<?= $user['uid'] ?>" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash delete"></i></a> &nbsp;
                                <a href="<?=base_url().$this->uri->segment(1).'/update/'.$user['uid']?>"><span data-toggle="tooltip" data-placement="top" title="Update"><i class="fa fa-edit"></i></span></a> &nbsp;

                            </td>
		        		</tr>
		        	<?php } // end foreach?>
		    </tbody>
		</table>
		<div class="col-md-12 text-center">
			<?= $pagination?>
		</div>