<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=ucfirst($this->uri->segment(2)).' '.ucfirst('User')?></h1>
    </div>
</div>
<p id="errMsg" class="text-info"></p>
<form id="addform">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>First Name</label>
                <input type="text" class="form-control req" id="fname" name="fname" title="Please enter First Name" placeholder="First Name"  value="<?=isset($row) ? $row->fname : ''?>">
                <input type="hidden" value="<?=isset($row) ? $row->uid : ''?>" id="uid" name="uid"/>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" class="form-control req" id="lname" name="lname" title="Please enter Last Name" placeholder="Last Name"  value="<?=isset($row) ? $row->lname : ''?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Username</label>
                <input class="form-control req" type="text" id="username" name="username" title="Please enter Username" placeholder="Username" value="<?=isset($row) ? $row->username : ''?>" />
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Email Address</label>
                <input class="form-control req" type="text" id="email" name="email" title="Please enter Email Address" placeholder="Email Address" value="<?=isset($row) ? $row->email : ''?>" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
              <label for="user_level">User Level</label>
              <select class="form-control req" id="level" name="level" title="Please select User Level" <?=$this->session->userdata('auth_level') != '1' ? 'disabled' : '' ?> >
                <option value="">-- User Level --</option>
                    <?php 
                        foreach($level['data'] as $index=> $arr){ 
                           $selected = '';
                            if($row->level == $arr['level'] ){
                                $selected = 'selected';
                            }
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $arr['level']; ?>"><?php echo $arr['level_desc']; ?></option>
                    <?php } ?>
                    
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Password</label>
                <input class="form-control req" type="password" id="password" name="password" title="Please enter Password" placeholder="Password" value="<?=isset($row) ? $row->password : ''?>" />
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Confirm Password</label>
                <input class="form-control req" type="password" id="cpassword" name="cpassword" title="Please enter Confirm Password" placeholder="Confirm Password" value="<?=isset($row) ? $row->password : ''?>" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <input id="submit" type="submit" class="btn btn-primary"></button>
                <a href="<?=base_url()?>users" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </div>
</form>