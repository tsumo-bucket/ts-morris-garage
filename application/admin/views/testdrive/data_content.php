		<table class="table table-hover">
		    <thead>
		        <tr>
			        <th width="25%">Name</th>
			        <th width="25%">Email</th>
			        <th width="10%">Contact</th>
			        <th width="10%">Postcode</th>
			        <th width="15%">Model</th>
			        <th width="10%">Date</th>
			        <th width="5%">Actions</th>
		        </tr>
		    </thead>
		    <tbody> 
		        <?php 
		        	foreach ($data as $page) {?>
		        		<tr class=" line-color ">
		        			<td><?php echo $page['name'];?></td>
		        			<td><?php echo $page['email'];?></td>
		        			<td><?php echo $page['contact'];?></td>
		        			<td><?php echo $page['postcode'];?></td>
		        			<td><?php echo $page['model'];?></td>
		        			<td><?php echo date('M d,Y',strtotime($page['inserted_date']));?></td>
		        			<td>		        				
                                <a href="javascript:void(0);" title="Delete" class="dels" data-url="<?=base_url().$this->uri->segment(1).'/delete'?>" data-id="<?= $page['id'] ?>" data-toggle="tooltip" data-placement="top" >
                                <i class="fa fa-trash delete"></i></a> &nbsp;
                                <a href="javascript:void(0);" title="View Message" class="popModal" data-url="<?=base_url().$this->uri->segment(1).'/viewMessage'?>" data-id="<?= $page['id'] ?>"><i class="fa fa-eye"></i></a> &nbsp;

                            </td>
		        		</tr>
		        	<?php } // end foreach?>
		    </tbody>
		</table>
		<div class="col-md-12 text-center">
			<?= $pagination?>
		</div>