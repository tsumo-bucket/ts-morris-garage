<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	   	   
		$this->module		= $this->uri->segment(1);	// shorten the segment
		$this->table		= 'tbl_articles';			// table name
	}

    public function index(){
    	if($this->uri->segment(2) && !is_numeric($this->uri->segment(2)) ){
    		$this->inner($this->uri->segment(2));
    		return false;
    	}
    	
    	$total = $this->total_record();
        // $per_page = 6; //old
        $per_page = 4;
        
    	$this->load->library('pagination');
    	$config = array();
    	$config['base_url'] = base_url() ."news/";
    	$config['total_rows'] = $total;
        $config['per_page'] = $per_page;
        $config["uri_segment"] = 2;
        $config['display_pages'] = TRUE;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';
		$config['cur_tag_open'] = '<a class="o-pagination__page o-pagination__page--active">';
		$config['cur_tag_close'] = '</a>';	
		$config['attributes'] = array('class' => 'o-pagination__page');


        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $news = $this->fetch_data($config["per_page"], $page);
        $str_links = $this->pagination->create_links();
		$links = explode('&nbsp;',$str_links );  
		
		// videos
		
		$this->db->where("status = 1");
		// $this->db->limit('10');
    	$this->db->order_by('order','ASC');
		$videos = $this->db->get('tbl_youtube_videos');
    	$vars = array(	
			'row' 		=> $news,
			'links' 	=> $links,
			'videos'	=> $videos,
			'ptitle'	=> 'News & Activities',
			'mtags'		=> '',
			'mdesc'		=> '',
			'otitle'	=> 'News & Activities',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
		);
		
		$data = array(
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view($this->module.'/index', $vars, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
		$this->load->view('tpl/main_template', $data, false);
    }	

    public function inner($slug){
    	// article list
    	$whr 	= array(
			'status' 	=> 1,
			'type'		=> 'news'
		);

		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> $whr,
			'order'		=> 'article_date DESC'
		);
		$a 	= $this->queries->get_data($params); 

		foreach ($a['data'] as $article) {
			$arr_article[] = $article['slug'];
		}

		$curr_key = array_search($slug, $arr_article);
		$prev = $arr_article[$curr_key];;
		$next = $arr_article[$curr_key];;
		if($curr_key!=0){
			$prev = $arr_article[$curr_key-1];
		}

		if(count($arr_article)> 0 && count($arr_article) > ($curr_key+1)){
			$next = $arr_article[$curr_key+1];
		}
		
		// current article
    	$whr 	= array(
			'status' 	=> 1,
			'slug'		=> $slug,
			'type'		=> 'news'
		);
		
		$params = array(
			'table'		=> $this->table,
			'fields'	=> '*',
			'where'		=> $whr,
			'row'		=> true
		);

		$d 	= $this->queries->get_data($params);  
		
		// fb share
		$otitle = !empty($d->meta_title) ? $d->meta_title : $d->title . ' - MG Philippines';
		$ourl  	= base_url();
		$odesc  = !empty($d->meta_desc) ? $d->meta_desc : 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.';
		$oimage = base_url().'assets/images/mgcars-fb-share-logo.jpg';
		if($d->fb_title){
			$otitle = $d->fb_title;
			$ourl  	= base_url().'news/'.$slug;
			$odesc  = $d->fb_description;
			$oimage = base_url().'uploads/images/articles/fbimages/'.$d->fb_image;
		}

		$vars = array(	
			'row' 		=> $d,
			'prev'  	=> base_url().'news/'.$prev,
			'next'  	=> base_url().'news/'.$next,
			'ptitle'	=> !empty($d->meta_title) ? $d->meta_title : $d->title . ' - MG Philippines',
			'mtags'		=> !empty($d->meta_tags) ? $d->meta_tags : '',
			'mdesc'		=> !empty($d->meta_desc) ? $d->meta_desc : '',
			'otitle'	=> $otitle,
			'ourl'		=> $ourl,
			'odesc'		=> $odesc,
			'oimage'	=> $oimage
		);

		$data = array(
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view($this->module.'/inner', $vars, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
		$this->load->view('tpl/main_template', $data, false);
    }

    private function total_record(){
    	$cond = "status = 1 AND type = 'news' ";
    	$this->db->order_by('article_date','DESC');
    	$query = $this->db->get_where($this->table, $cond);
		return $query->num_rows();
    }

    // Fetch data according to per_page limit.
	private function fetch_data($limit, $start) {
		$cond = "status = 1 AND type = 'news' "; 
		$this->db->limit($limit,$start);
		$this->db->select('*');	
		$this->db->order_by('article_date','DESC');
		$query = $this->db->get_where($this->table, $cond);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		}
		return false;
	}

}
