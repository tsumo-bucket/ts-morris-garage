<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends CI_Controller {
	
	public function __construct() {
	    parent::__construct();
	}
	
	public function contact(){
		$name 		= $this->input->post('name');
		$email 	   	= $this->input->post('email');
		$contact 	= $this->input->post('contact');
		$postcode	= $this->input->post('postcode');
		$comments	= $this->input->post('comments');
		$captcha 	= $this->input->post('g-recaptcha-response');

		$error = '';
		if($name && !$this->initials->alpha($name)){
			$error = 'You have entered invalid characters in your Full Name';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($email && !$this->initials->email($email)){
			$error = 'Sorry. Email address is not in the right format.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($contact && !$this->initials->mobile($contact)){
			$error = 'You have enter invalid characters in your Phone Number.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($postcode && !$this->initials->numeric($postcode)){
			$error = 'You have enter invalid characters in your Postcode.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if(!$captcha){
			$error = 'Please check the the captcha form';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		// recaptcha validation
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secretkey = '6Ldgl3MUAAAAANNa6y5i8ZMsZVNEdHP9VHxdJLjK';
        $response = file_get_contents($url."?secret=".$secretkey."&response=".$captcha."&remoteip=".$_SERVER["REMOTE_ADDR"]);
        $c_data = json_decode($response);
		
		if($name && $email && $contact && $comments && $postcode && $c_data->success == "true"){
			$params = array(
				'table' 	=> 'tbl_inquiries',
				'tbluid'	=> 'id'
			);

			$record = array(
				'name'			=> $name,
				'email' 		=> $email,
				'contact' 		=> $contact,
				'comments'		=> $comments,
				'postcode'		=> $postcode,
				'inserted_date' => date('Y-m-d H:i:s',strtotime("now")),
				'browser' 		=> $_SERVER['HTTP_USER_AGENT'],
				'ipaddress'		=> $this->input->ip_address()
			);

			$result	 = $this->queries->insert(array_merge($params, array('data' => $record)));

			if($result!=0){
				$arr = array(
					'save' => 'yes',
					'msg'  => 'Thank you for taking the time to fill out this form. An authorized MG Philippines representative get in touch with you for all your future needs.'
				);
			}else{
				$arr = array('error' => 'An error occured during processing. Please try again.');
			}	
			echo json_encode($arr);
			exit();
		}else{
			$arr = array('error' => 'Please complete the form.');
			echo json_encode($arr);
			exit();
		}
	}

	public function book(){
		$name 		= $this->input->post('name');
		$email 	   	= $this->input->post('email');
		$contact 	= $this->input->post('contact');
		$postcode	= $this->input->post('postcode');
		$comments	= $this->input->post('comments');
		$model		= $this->input->post('model');
		$captcha 	= $this->input->post('g-recaptcha-response');

		$error = '';
		if($name && !$this->initials->alpha($name)){
			$error = 'You have entered invalid characters in your Full Name';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($email && !$this->initials->email($email)){
			$error = 'Sorry. Email address is not in the right format.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($contact && !$this->initials->mobile($contact)){
			$error = 'You have enter invalid characters in your Phone Number.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($postcode && !$this->initials->numeric($postcode)){
			$error = 'You have enter invalid characters in your Postcode.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if(!$model){
			$error = 'Please pick a model to test drive.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if(!$captcha){
			$error = 'Please check the the captcha form';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		// recaptcha validation
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secretkey = '6Ldgl3MUAAAAANNa6y5i8ZMsZVNEdHP9VHxdJLjK';
        $response = file_get_contents($url."?secret=".$secretkey."&response=".$captcha."&remoteip=".$_SERVER["REMOTE_ADDR"]);
        $c_data = json_decode($response);

		if($name && $email && $contact && $comments && $postcode && $model && $c_data->success == "true"){
			$params = array(
				'table' 	=> 'tbl_test_drive',
				'tbluid'	=> 'id'
			);

			$record = array(
				'name'			=> $name,
				'email' 		=> $email,
				'contact' 		=> $contact,
				'comments'		=> $comments,
				'postcode'		=> $postcode,
				'model'			=> $model,
				'inserted_date' => date('Y-m-d H:i:s',strtotime("now")),
				'browser' 		=> $_SERVER['HTTP_USER_AGENT'],
				'ipaddress'		=> $this->input->ip_address()
			);

			$result	 = $this->queries->insert(array_merge($params, array('data' => $record)));

			if($result!=0){
				$arr = array(
					'save' => 'yes',
					'msg'  => 'Thank you for taking the time to fill out this form. An authorized MG Philippines representative get in touch with you for all your future needs.'
				);
			}else{
				$arr = array('error' => 'An error occured during processing. Please try again.');
			}	
			echo json_encode($arr);
			exit();
		}else{
			$arr = array('error' => 'Please complete the form.');
			echo json_encode($arr);
			exit();
		}
	}

	public function quote(){
		$name 		= $this->input->post('name');
		$email 	   	= $this->input->post('email');
		$contact 	= $this->input->post('contact');
		$postcode	= $this->input->post('postcode');
		$comments	= $this->input->post('comments');
		$captcha 	= $this->input->post('g-recaptcha-response');

		$error = '';
		if($name && !$this->initials->alpha($name)){
			$error = 'You have entered invalid characters in your Full Name';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($email && !$this->initials->email($email)){
			$error = 'Sorry. Email address is not in the right format.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($contact && !$this->initials->mobile($contact)){
			$error = 'You have enter invalid characters in your Phone Number.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($postcode && !$this->initials->numeric($postcode)){
			$error = 'You have enter invalid characters in your Postcode.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		// recaptcha validation
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secretkey = '6Ldgl3MUAAAAANNa6y5i8ZMsZVNEdHP9VHxdJLjK';
        $response = file_get_contents($url."?secret=".$secretkey."&response=".$captcha."&remoteip=".$_SERVER["REMOTE_ADDR"]);
        $c_data = json_decode($response);

		if($name && $email && $contact && $comments && $postcode && $c_data->success == "true"){
			$params = array(
				'table' 	=> 'tbl_request',
				'tbluid'	=> 'id'
			);

			$record = array(
				'name'			=> $name,
				'email' 		=> $email,
				'contact' 		=> $contact,
				'comments'		=> $comments,
				'postcode'		=> $postcode,
				'inserted_date' => date('Y-m-d H:i:s',strtotime("now")),
				'browser' 		=> $_SERVER['HTTP_USER_AGENT'],
				'ipaddress'		=> $this->input->ip_address()
			);

			$result	 = $this->queries->insert(array_merge($params, array('data' => $record)));

			if($result!=0){
				$arr = array(
					'save' => 'yes',
					'msg'  => 'Thank you! Your Request has been sent.'
				);
			}else{
				$arr = array('error' => 'An error occured during processing. Please try again.');
			}	
			echo json_encode($arr);
			exit();
		}else{
			$arr = array('error' => 'Please complete the form.');
			echo json_encode($arr);
			exit();
		}
	}
	
	
	public function download($filename){
		$this->load->helper('download');
		// read file contents
	    $data = file_get_contents(base_url('/uploads/brochures/'.$filename));
	    force_download($filename, $data);
	}

}
