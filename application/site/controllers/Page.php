<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	
	public function __construct() {
	    parent::__construct();
	   
		$this->slug       = $this->uri->segment(1, 'home');
		$this->inner_slug = $this->uri->segment(2);
		$this->table_page	= 'tbl_pages';			// table name
	}
	
	public function _remap(){
		switch($this->slug){
			case null   			:
			case false 				:
			case 'home'				: $this->index(); break;
			case 'test-drive'		: $this->book_page(); break;
			case 'contact-us'		: $this->contact_page(); break;
			case 'thank-you'		: $this->ty_page(); break;
			case 'request-a-qoute'	: $this->quote_page(); break;
			case 'dealers'			: $this->dealers_page(); break;
			case 'mg-zs-t'		:
			case 'mg-zs'			: 
			case 'mg-6'				:
			case 'mg-5'				:
			case 'mg-rx8'			:
			case 'mg-rx5'			: $this->vehicle_page(); break;
			case 'history'			: $this->history_page(); break;
			default 				: $this->page_default(); break;
		}
	}

	public function page_default(){
		$this->output->delete_cache();
		$page = $this->_get_page_info($this->slug); 

		if (!empty($page)) {
			$vars   = array('row' => $page);
			$data = array(
				'ptitle'	=> $page->title . ' - MG Philippines',
				'mtags'		=> $page->meta_tags,
				'mdesc'		=> $page->meta_desc,
				'otitle'	=> $page->title . ' - MG Philippines',
    			'ourl'		=>  base_url(),
    			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
    			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
				'navs'		=> $this->load->view('tpl/nav_content','', true),
				'content'	=> $this->load->view('pages/index', $vars , true),
				'footer'	=> $this->load->view('tpl/footer_content','', true)
			);
			$this->output->cache(1); 	
			$this->load->view('tpl/main_template', $data, false);
		} else {
			show_404();
		}
	}

	public function index(){
		// get sliders
		$params = array(
			'table'		=> 'tbl_sliders',
			'fields'	=> '*',
			'where'		=> array('status' => '1'),
			'order'		=> 'seqno ASC'
		);
		$sliders = $this->queries->get_data($params);
		$vars = array(
			'sliders' 		=> $sliders['data']
		);	

		$data = array(
			'ptitle'	=> 'MG Philippines',
			'mtags'		=> '',
			'mdesc'		=> '',
			'otitle'	=> 'MG Philippines',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('home/index', $vars, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function ty_page(){	

		$params = array(
			'table'		=> 'tbl_test_drive',
			'fields'	=> '*',
			'where'		=> array('id' => $_GET['d'])
		);

		$rec = $this->queries->get_data($params);
		
		$match_tag = "
		<!-- Advenue DMP - User Matching Tag -->
		<script type=\"text/javascript\">
		    window.innityDataLayer = window.innityDataLayer || {};
		    //if user is logged in, output this line of code
		    window.innityDataLayer.user = {
		        id: '".$rec['data']['0']['contact']."'//eg. 5689932 or hashed email address / phone number
		    };
		</script>
		<!--  End Advenue DMP - User Matching Tag -->";

		$data = array(
			'ptitle'	=> 'MG Philippines',
			'mtags'		=> '',
			'mdesc'		=> '',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'match_tag' => $match_tag,
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/thank_you', $rec, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function book_page(){
	    $params = array(
			'table'		=> 'tbl_dealers',
			'fields'	=> '*',
			'where'		=> array('status' => '1')
		);
		$dealers = $this->queries->get_data($params);

		$vars = array(
			'dealers' 	=> $dealers['data']
		);
		
		$data = array(
			'ptitle'	=> 'MG Philippines',
			'mtags'		=> '',
			'mdesc'		=> '',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/test_drive', $vars, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function contact_page(){
		
	    $params = array(
			'table'		=> 'tbl_dealers',
			'fields'	=> '*',
			'where'		=> array('status' => '1')
		);
		$dealers = $this->queries->get_data($params);

		$vars = array(
			'dealers' 	=> $dealers['data']
		);

	    if($this->inner_slug && $this->inner_slug == 'thank-you' && !empty($_GET['d'])){
			// $id = $this->uri->segment(3);
			$id = $_GET['d'];
			$this->contact_ty($id);
		}else{
		
    		$data = array(
    			'ptitle'	=> 'MG Philippines',
    			'mtags'		=> '',
    			'mdesc'		=> '',
    			'otitle'	=> 'MG Philippines | Be Excited',
    			'ourl'		=>  base_url(),
    			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
    			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
    			'navs'		=> $this->load->view('tpl/nav_content','', true),
    			'content'	=> $this->load->view('pages/contact_us', $vars, true),
    			'footer'	=> $this->load->view('tpl/footer_content','', true)
    		);
    	
    		$this->load->view('tpl/main_template', $data, false);
		}
	}
	
	public function dealers_page() {
		// get dealer 
		$params = array(
			'table'		=> 'tbl_dealers',
			'fields'	=> '*',
			'where'		=> array('status' => '1')
		);
		$dealers = $this->queries->get_data($params);

		// get pop-up stores
		$params = array(
			'table'		=> 'tbl_stores',
			'fields'	=> '*',
			'where'		=> array('status' => '1')
		);
		$stores = $this->queries->get_data($params);

		$vars = array(
			'dealers' 	=> $dealers['data'],
			'stores' 	=> $stores['data'],
		);	

		$data = array(
			'ptitle'	=> 'MG Philippines',
			'mtags'		=> '',
			'mdesc'		=> '',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/dealer', $vars, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function quote_page(){		
		$data = array(
			'ptitle'	=> 'MG Philippines',
			'mtags'		=> '',
			'mdesc'		=> '',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/request_quote', '', true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function history_page(){
		$page = $this->_get_page_info($this->slug); 
		$desc = '';
		if(!empty($page)){
			$desc = $page->description;
		}

		$params = array(
			'table'		=> 'tbl_histories',
			'fields'	=> '*',
			'where'		=> array('status' => '1'),
			'order'		=> 'year ASC'
		);

		$histories = $this->queries->get_data($params);

		$vars = array(
			'histories' 	=> $histories['data'],
			'desc'			=> $desc
		);	

		$data = array(
			'ptitle'	=> 'History - MG Philippines',
			'mtags'		=> '',
			'mdesc'		=> '',
			'otitle'	=> 'History - MG Philippines',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/history', $vars, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function vehicle_page(){
		// get dealer 
		$params = array(
			'table'		=> 'tbl_dealers',
			'fields'	=> '*',
			'where'		=> array('status' => '1')
		);
		$dealers = $this->queries->get_data($params);
		

		$vehicle = $this->_get_page_vehicle($this->slug); 

		if (!empty($vehicle)) {
			$vars   = array('row' => $vehicle, 'dealers' => $dealers['data']);
			
			$data = array(
				'ptitle'	=> $vehicle->model . ' - MG Philippines',
				'mtags'		=> $vehicle->meta_tags,
				'mdesc'		=> $vehicle->meta_desc,
				'otitle'	=> $vehicle->model . ' - MG Philippines',
				'ourl'		=>  base_url().'/'.$vehicle->slug,
				'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
				'oimage'	=> base_url().'uploads/images/vehicles/'.$vehicle->banner_image,
				'navs'		=> $this->load->view('tpl/nav_content','', true),
				'content'	=> $this->load->view('pages/vehicle', $vars , true),
				'footer'	=> $this->load->view('tpl/footer_content','', true)
			);	
			$this->load->view('tpl/main_template', $data, false);
		} else {
			show_404();
		}
	}

	private function _get_page_info($slug = '') {
		$result = array();

		if ($slug != '') {
			$where = array(
				'slug'    => $slug,
				'status'  => 1,
			);

			$this->db->where($where);
			$query = $this->db->get($this->table_page, 1);

			if ($query->num_rows() > 0) {
				$result = $query->row();
			}
		}
		return $result;
	}

	private function _get_page_vehicle($slug = '') {
		$result = array();

		if ($slug != '') {
			$where = array(
				'slug'    => $slug,
				'status'  => 1,
			);

			$this->db->where($where);
			$query = $this->db->get('tbl_vehicles', 1);

			if ($query->num_rows() > 0) {
				$result = $query->row();
			}
		}
		return $result;
	}
	
	public function contact_ty($id){	

		$params = array(
			'table'		=> 'tbl_inquiries',
			'fields'	=> '*',
			'where'		=> array('id' => $id)
		);

		$rec = $this->queries->get_data($params);

		$match_tag = "
		<!-- Advenue DMP - User Matching Tag -->
		<script type=\"text/javascript\">
		    window.innityDataLayer = window.innityDataLayer || {};
		    //if user is logged in, output this line of code
		    window.innityDataLayer.user = {
		        id: '".$rec['data']['0']['contact']."'//eg. 5689932 or hashed email address / phone number
		    };
		</script>
		<!--  End Advenue DMP - User Matching Tag -->";

		$data = array(
			'ptitle'	=> 'MG Philippines',
			'mtags'		=> '',
			'mdesc'		=> '',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'match_tag' => $match_tag,
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/contact_ty', $rec, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}
	
}
