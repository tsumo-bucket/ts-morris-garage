<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	
	public function __construct() {
	    parent::__construct();
	   
		$this->slug       = $this->uri->segment(1, 'home');
		$this->inner_slug = $this->uri->segment(2);
		$this->table_page	= 'tbl_pages';			// table name
	}
	
	public function _remap(){
		switch($this->slug){
			case null   			:
			case false 				:
			case 'home'				: $this->index(); break;
			case 'test-drive'		: $this->book_page(); break;
			case 'contact-us'		: $this->contact_page(); break;
			case 'request-a-qoute'	: $this->quote_page(); break;
			case 'dealers'			: $this->dealers_page(); break;
			case 'mg-zs'			: 
			case 'mg-6'				:
			case 'mg-rx5'			: $this->vehicle_page(); break;
			case 'history'			: $this->history_page(); break;
			default 				: $this->page_default(); break;
		}
	}

	public function page_default(){
		$page = $this->_get_page_info($this->slug); 

		if (!empty($page)) {
			$vars   = array('row' => $page);
			$data = array(
				'ptitle'	=> 'MG Philippines - '.$page->title,
				'mtitle'	=> $page->meta_title,
				'mdesc'		=> $page->meta_desc,
				'otitle'	=> 'MG Philippines | Be Excited',
    			'ourl'		=>  base_url(),
    			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
    			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
				'navs'		=> $this->load->view('tpl/nav_content','', true),
				'content'	=> $this->load->view('pages/index', $vars , true),
				'footer'	=> $this->load->view('tpl/footer_content','', true)
			);	
			$this->load->view('tpl/main_template', $data, false);
		} else {
			show_404();
		}
	}

	public function index(){
		// get sliders
		$params = array(
			'table'		=> 'tbl_sliders',
			'fields'	=> '*',
			'where'		=> array('status' => '1')
		);
		$sliders = $this->queries->get_data($params);

		$vars = array(
			'sliders' 		=> $sliders['data']
		);	

		$data = array(
			'ptitle'	=> 'MG Philippines',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('home/index', $vars, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function book_page(){		
		$data = array(
			'ptitle'	=> 'MG Philippines',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/test_drive', '', true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function contact_page(){
		$data = array(
			'ptitle'	=> 'MG Philippines',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/contact_us', '', true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}
	
	public function dealers_page() {
	    // get dealer 
		$params = array(
			'table'		=> 'tbl_dealers',
			'fields'	=> '*',
			'where'		=> array('status' => '1')
		);
		$dealers = $this->queries->get_data($params);

		// get pop-up stores
		$params = array(
			'table'		=> 'tbl_stores',
			'fields'	=> '*',
			'where'		=> array('status' => '1')
		);
		$stores = $this->queries->get_data($params);

		$vars = array(
			'dealers' 	=> $dealers['data'],
			'stores' 	=> $stores['data'],
		);	
		
		$data = array(
			'ptitle'	=> 'MG Philippines',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/dealer', $vars, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function quote_page(){		
		$data = array(
			'ptitle'	=> 'MG Philippines',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/request_quote', '', true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}
	
	public function history_page(){
		$page = $this->_get_page_info($this->slug); 
		$desc = '';
		if(!empty($page)){
			$desc = $page->description;
		}

		$params = array(
			'table'		=> 'tbl_histories',
			'fields'	=> '*',
			'where'		=> array('status' => '1'),
			'order'		=> 'year ASC'
		);

		$histories = $this->queries->get_data($params);

		$vars = array(
			'histories' 	=> $histories['data'],
			'desc'			=> $desc
		);	

		$data = array(
			'ptitle'	=> 'MG Philippines',
			'otitle'	=> 'MG Philippines | Be Excited',
			'ourl'		=>  base_url(),
			'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
			'oimage'	=> base_url().'assets/images/mgcars-fb-share-logo.jpg',
			'navs'		=> $this->load->view('tpl/nav_content','', true),
			'content'	=> $this->load->view('pages/history', $vars, true),
			'footer'	=> $this->load->view('tpl/footer_content','', true)
		);
	
		$this->load->view('tpl/main_template', $data, false);
	}

	public function vehicle_page(){

		$vehicle = $this->_get_page_vehicle($this->slug); 

		if (!empty($vehicle)) {
			$vars   = array('row' => $vehicle);
			
			$data = array(
				'ptitle'	=> 'MG Philippines',
				'otitle'	=> 'MG Philippines | Be Excited',
				'ourl'		=>  base_url().'/'.$vehicle->slug,
				'odesc'		=> 'This is the official site of MG in the Philippines. Learn more about MG’s new era of vehicles and book a test drive, today. Keep up to date with exciting MG promos and launches, and the latest news about MG.',
				'oimage'	=> base_url().'uploads/images/vehicles/'.$vehicle->banner_image,
				'navs'		=> $this->load->view('tpl/nav_content','', true),
				'content'	=> $this->load->view('pages/vehicle', $vars , true),
				'footer'	=> $this->load->view('tpl/footer_content','', true)
			);	
			$this->load->view('tpl/main_template', $data, false);
		} else {
			show_404();
		}
	}

	private function _get_page_info($slug = '') {
		$result = array();

		if ($slug != '') {
			$where = array(
				'slug'    => $slug,
				'status'  => 1,
			);

			$this->db->where($where);
			$query = $this->db->get($this->table_page, 1);

			if ($query->num_rows() > 0) {
				$result = $query->row();
			}
		}
		return $result;
	}

	private function _get_page_vehicle($slug = '') {
		$result = array();

		if ($slug != '') {
			$where = array(
				'slug'    => $slug,
				'status'  => 1,
			);

			$this->db->where($where);
			$query = $this->db->get('tbl_vehicles', 1);

			if ($query->num_rows() > 0) {
				$result = $query->row();
			}
		}
		return $result;
	}
	
}
