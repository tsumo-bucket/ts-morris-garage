<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends CI_Controller {
	
	public function __construct() {
	    parent::__construct();
	}
	
	public function contact(){
		$name 		= $this->input->post('name');
		$email 	   	= $this->input->post('email');
		$contact 	= $this->input->post('contact');
		$postcode	= $this->input->post('postcode');
		$model		= $this->input->post('model');
		$dealer     = $this->input->post('dealer');
		$comments	= $this->input->post('comments');
		$captcha 	= $this->input->post('g-recaptcha-response');
		$category	= $this->input->post('category');
		
		$model = implode(",",$model);
		$input = [];
		$input['name'] = $name;
		$input['email'] = $email;
		$input['contact'] = $contact;
		$input['postcode'] = $postcode;
		$input['model'] = $model;
		$input['dealer'] = $dealer;
		$input['comments'] = $comments;
		$input['category'] = $category;

		$error = '';
		if($name && !$this->initials->alpha($name)){
			$error = 'You have entered invalid characters in your Full Name';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($email && !$this->initials->email($email)){
			$error = 'Sorry. Email address is not in the right format.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($contact && !$this->initials->mobile($contact)){
			$error = 'You have enter invalid characters in your Phone Number.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		/*if($postcode && !$this->initials->numeric($postcode)){
			$error = 'You have enter invalid characters in your Postcode.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}*/
		
		if($postcode && !$this->initials->alpha($postcode)){
			$error = 'You have entered invalid characters in your City';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}
		
		if(!$model){
			$error = 'Please pick a model to test drive.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}
		
		if(!$dealer){
			$error = 'Please select Preferred Dealer.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if(!$category){
			$error = 'Please select category.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if(!$captcha){
			$error = 'Please check the the captcha form';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}
		

		// recaptcha validation
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secretkey = '6Ldgl3MUAAAAANNa6y5i8ZMsZVNEdHP9VHxdJLjK';
        $response = file_get_contents($url."?secret=".$secretkey."&response=".$captcha."&remoteip=".$_SERVER["REMOTE_ADDR"]);
		$c_data = json_decode($response);
		
		
		
		if($name && $email && $contact && $comments && $postcode && $category && $c_data->success == "true"){
			//send email 
			$this->sendInquiryToMg($input);
			$params = array(
				'table' 	=> 'tbl_inquiries',
				'tbluid'	=> 'id'
			);

			$record = array(
				'name'			=> $name,
				'email' 		=> $email,
				'contact' 		=> $contact,
				'comments'		=> $comments,
				'postcode'		=> $postcode,
				'model'			=> $model,
				'dealer'        => $dealer,
				'category'        => $category,
				'inserted_date' => date('Y-m-d H:i:s',strtotime("now")),
				'browser' 		=> $_SERVER['HTTP_USER_AGENT'],
				'ipaddress'		=> $this->input->ip_address()
			);

			$result	 = $this->queries->insert(array_merge($params, array('data' => $record)));

			if($result!=0){
				$arr = array(
					'save' => 'yes',
					'rec' 	 => $result,
					'msg'  => 'Thank you for taking the time to fill out this form. An authorized MG Philippines representative get in touch with you for all your future needs.'
				);
			}else{
				$arr = array('error' => 'An error occured during processing. Please try again.');
			}	
			echo json_encode($arr);
			exit();
		}else{
			$arr = array('error' => 'Please complete the form.');
			echo json_encode($arr);
			exit();
		}
	}

	
	private function sendInquiryToMg($message, $subject = "Inquiry") {
        $this->load->library('EmailHelper');
		
		// get dealer email
		$params = array(
			'table'		=> 'tbl_dealers',
			'fields'	=> '*',
			'where'		=> array('name' => $message['dealer'])
		);

		$rec = $this->queries->get_data($params);


		$params = array();
		$params['mail_to'] = '';
		$params['cc'] = '';
		$params['bcc'] = '';

		if (!empty($rec) && !empty($rec['data'][0]) && !empty($rec['data'][0]['email'])) {
			$params['mail_to'] = $rec['data'][0]['email'];
		}
		if (!empty($rec) && !empty($rec['data'][0]) && !empty($rec['data'][0]['cc'])) {
			$params['cc'] = $rec['data'][0]['cc'];
		}
		if (!empty($rec) && !empty($rec['data'][0]) && !empty($rec['data'][0]['bcc'])) {
			$params['bcc'] = $rec['data'][0]['bcc'];
		}

		if (!empty($message['category']) && !empty($rec['data'][0])) {
			// get dealer email
			$where = [];
			$params2 = array(
				'table'		=> 'tbl_email_contacts',
				'fields'	=> '*'
			);
			$where['dealer_id']  = $rec['data'][0]['id'];
			$where['category'] = $message['category'];
			
			$params2['where'] = $where;
			$rec2 = $this->queries->get_data($params2);
			


			if (!empty($rec2) && !empty($rec2['data'][0]) && !empty($rec2['data'][0]['mail_to'])) {
				$params['mail_to'] = $rec2['data'][0]['mail_to'];
			}
			if (!empty($rec2) && !empty($rec2['data'][0]) && !empty($rec2['data'][0]['cc'])) {
				$params['cc'] = $rec2['data'][0]['cc'];
			}
			if (!empty($rec2) && !empty($rec2['data'][0]) && !empty($rec2['data'][0]['bcc'])) {
				$params['bcc'] = $rec2['data'][0]['bcc'];
			}
			
		}
		
		if ($subject == "Inquiry") {
			$params['subject'] = ucwords($message['category']);
		} else {
			$params['subject'] = $subject;
		}

		$params['content']  = "<p>Sender Name: <b>" . $message['name'] . "</b></p>";
		$params['content'] .= "<p>Sender Email: <b>" . $message['email'] . "</b></p>";
		$params['content'] .= "<p>Sender Contact: <b>" . $message['contact'] . "</b></p>";
		$params['content'] .= "<p>Dealer: <b>" . $message['dealer'] . "</b></p>";
		if (isset($message['model']) && count($message['model']) > 0) {
			$params['content'] .= "<p>Model Pick: <b>".$message['model']."</b></p>";
		}
		$params['content'] .= "<p>Message:</p>";
		$params['content'] .= "<p><b>" . $message['comments'] . "</b></p>";

		
		// $params['debug'] = 'preview';
		
		$success = $this->emailhelper->send_mail($params,'email', 'template');
	}

	public function book(){
		$name 		= $this->input->post('name');
		$email 	   	= $this->input->post('email');
		$contact 	= $this->input->post('contact');
		$postcode	= $this->input->post('postcode');
		$comments	= $this->input->post('comments');
		$model		= $this->input->post('model');
		$dealer     = $this->input->post('dealer');
		$captcha 	= $this->input->post('g-recaptcha-response');
		$model = implode(",",$model);
		$category	= $this->input->post('category');

		$input = [];
		$input['name'] = $name;
		$input['email'] = $email;
		$input['contact'] = $contact;
		$input['postcode'] = $postcode;
		$input['model'] = $model;
		$input['dealer'] = $dealer;
		$input['comments'] = $comments;
		$input['category'] = $category;
		
		$error = '';
		if($name && !$this->initials->alpha($name)){
			$error = 'You have entered invalid characters in your Full Name';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($email && !$this->initials->email($email)){
			$error = 'Sorry. Email address is not in the right format.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($contact && !$this->initials->mobile($contact)){
			$error = 'You have enter invalid characters in your Phone Number.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($postcode && !$this->initials->alpha($postcode)){
			$error = 'You have entered invalid characters in your City';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if(!$model){
			$error = 'Please pick a model to test drive.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}
		
		if(!$dealer){
			$error = 'Please select Preferred Dealer.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if(!$captcha){
			$error = 'Please check the the captcha form';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		// recaptcha validation
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secretkey = '6Ldgl3MUAAAAANNa6y5i8ZMsZVNEdHP9VHxdJLjK';
        $response = file_get_contents($url."?secret=".$secretkey."&response=".$captcha."&remoteip=".$_SERVER["REMOTE_ADDR"]);
        $c_data = json_decode($response);

		
		
		if($name && $email && $contact && $comments && $postcode && $model && $dealer && $c_data->success == "true"){
		// if($name && $email && $contact && $comments && $postcode && $model && $dealer){
			//send email
			$this->sendInquiryToMg($input,"Book a Test Drive");
			$params = array(
				'table' 	=> 'tbl_test_drive',
				'tbluid'	=> 'id'
			);

			$record = array(
				'name'			=> $name,
				'email' 		=> $email,
				'contact' 		=> $contact,
				'comments'		=> $comments,
				'postcode'		=> $postcode,
				'model'			=> $model,
				'dealer'        => $dealer,
				'inserted_date' => date('Y-m-d H:i:s',strtotime("now")),
				'browser' 		=> $_SERVER['HTTP_USER_AGENT'],
				'ipaddress'		=> $this->input->ip_address()
			);

			$result	 = $this->queries->insert(array_merge($params, array('data' => $record)));

			if($result!=0){
				$arr = array(
					'save' => 'yes',
					'rec' 	 => $result,
					'msg'  => 'Thank you for taking the time to fill out this form. An authorized MG Philippines representative get in touch with you for all your future needs.'
				);
			}else{
				$arr = array('error' => 'An error occured during processing. Please try again.');
			}	
			echo json_encode($arr);
			exit();
		}else{
			$arr = array('error' => 'Please complete the form.');
			echo json_encode($arr);
			exit();
		}
	}

	public function quote(){
		$name 		= $this->input->post('name');
		$email 	   	= $this->input->post('email');
		$contact 	= $this->input->post('contact');
		$postcode	= $this->input->post('postcode');
		$comments	= $this->input->post('comments');
		$captcha 	= $this->input->post('g-recaptcha-response');

		$error = '';
		if($name && !$this->initials->alpha($name)){
			$error = 'You have entered invalid characters in your Full Name';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($email && !$this->initials->email($email)){
			$error = 'Sorry. Email address is not in the right format.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($contact && !$this->initials->mobile($contact)){
			$error = 'You have enter invalid characters in your Phone Number.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		if($postcode && !$this->initials->numeric($postcode)){
			$error = 'You have enter invalid characters in your Postcode.';
			$arr = array('error' => $error);
			echo json_encode($arr);
			exit();
		}

		// recaptcha validation
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secretkey = '6Ldgl3MUAAAAANNa6y5i8ZMsZVNEdHP9VHxdJLjK';
        $response = file_get_contents($url."?secret=".$secretkey."&response=".$captcha."&remoteip=".$_SERVER["REMOTE_ADDR"]);
        $c_data = json_decode($response);

		if($name && $email && $contact && $comments && $postcode && $c_data->success == "true"){
			$params = array(
				'table' 	=> 'tbl_request',
				'tbluid'	=> 'id'
			);

			$record = array(
				'name'			=> $name,
				'email' 		=> $email,
				'contact' 		=> $contact,
				'comments'		=> $comments,
				'postcode'		=> $postcode,
				'inserted_date' => date('Y-m-d H:i:s',strtotime("now")),
				'browser' 		=> $_SERVER['HTTP_USER_AGENT'],
				'ipaddress'		=> $this->input->ip_address()
			);

			$result	 = $this->queries->insert(array_merge($params, array('data' => $record)));

			if($result!=0){
				$arr = array(
					'save' => 'yes',
					'msg'  => 'Thank you! Your Request has been sent.'
				);
			}else{
				$arr = array('error' => 'An error occured during processing. Please try again.');
			}	
			echo json_encode($arr);
			exit();
		}else{
			$arr = array('error' => 'Please complete the form.');
			echo json_encode($arr);
			exit();
		}
	}
	
	
	public function download($filename){
		$this->load->helper('download');
		// read file contents
	    $data = file_get_contents(base_url('/uploads/brochures/'.$filename));
	    force_download($filename, $data);
	}

}
