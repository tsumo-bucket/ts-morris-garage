<!--div class="o-imgWrap o-bg__geometry">
	<img src="<?=base_url()?>assets/images/img-geometry.png" alt="">
</div!-->
<div class="c-article-inner">
	<div class="o-heading__wrap">
		<h1><?=isset($row) ? $row->title : ''?></h1>
	</div>
	<div class="o-article__mainWrap">

		<div class="o-article-inner__date"><?=isset($row) ? date('F d, Y',strtotime($row->article_date)) : ''?></div>

		<div class="o-article-inner__content">
			<?=isset($row) ? $row->details : ''?>					
		</div>

	</div>

	<div class="c-pagination c-pagination--articlesInner">
		<a href="<?=$prev?>" class="o-pagination__prev"><span>Previous</span></a>
		<a href="<?=base_url()?>news" class="o-pagination__back"><span>Back to News & Activities</span></a>
		<a href="<?=$next?>" class="o-pagination__next"><span>Next</span></a>
	</div>
	
</div>