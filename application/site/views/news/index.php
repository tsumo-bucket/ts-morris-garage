<div class="o-imgWrap o-bg__geometry">
	<img src="<?=base_url()?>assets/images/img-geometry.png" alt="">
</div>
<div class="grid-container">
	<div class="column-1">
		<div class="c-article" style="max-width: 1200px">
			<div class="o-heading__wrap">
				<h1>News & Activities</h1>
			</div>
			<div class="o-article__mainWrap">
				<?php
				$this->load->helper('directory'); 
				$dir = "uploads/images/articles"; 
				$map = directory_map($dir);    
				foreach ($row as $data) {
				?>
				<div class="o-article__cardWrap">
					<div class="o-article__card">
						<div class="o-imgWrap">
							<img src="<?=base_url($dir).'/'.$data['image']?>" alt="">
						</div>
						<div class="o-article__infoWrap">
							<div class="o-article__date">
								<?=date('F d, Y', strtotime($data['article_date']))?>
							</div>
							<div class="o-article__title">
								<?=$data['title']?>
							</div>
							<div class="o-article__excerpt">
								<?=$data['teaser']?>
							</div>
							<a class="o-button" href="<?=base_url()?>news/<?=$data['slug']?>">Read More</a>							
						</div>
					</div>	
				</div>
				<?php } ?>
			</div>

			<div class="c-pagination c-pagination--articles">
				<?php foreach ($links as $link) {
					echo $link;
				}?>
			</div>
		</div>
	</div>
	<div class="column-2">
		<div class="c-article" style="max-width: 1200px;padding-bottom:30px;">
			<div class="o-heading__wrap">
				<h1>Videos</h1>
			</div>

			<div class="video-list">
				<?php foreach ($videos->result() as $video): ?>
					<div class="video">
						<div class="video-container">
							<iframe height="200"
								src="<?php echo $video->link?>">
							</iframe>
						</div>
						<div class="video-name">
							<?php echo $video->name ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>