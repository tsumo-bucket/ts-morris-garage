<div class="c-home">
	<div id="js-slider__home" class="c-slider c-slider--home">
		<?php 
		$this->load->helper('directory'); 
		$dir = "uploads/images/sliders"; 
		$map = directory_map($dir);      
		foreach ($sliders as $slider) { ?>  
		<a class="o-slider__wrap" href="<?= $slider['url']?>" target="_blank">
		  	<div class="o-imgWrap">
		  		<img src="<?=base_url($dir).'/'.$slider['image']?>" alt="">
		  	</div>
		</a>
		<?php } ?>
	</div>
	<div class="c-carmodels">
		<h2>The Time Is Now</h2>
		<p>Stimulate your driving senses by getting behind the wheel of a stylish and fun MG. This is the future of MG. The time to experience the rebirth of this automotive icon is now. </p>
		<div class="o-carmodels">
		    <div class="o-car">
				<h2>MG 5</h2>
				<div class="o-imgWrap">
					<a href="<?=base_url()?>mg-5"><img src="<?=base_url()?>assets/images/img-car__5.png" alt=""></a>
				</div>
			</div>
			<div class="o-car">
				<h2>MG RX5</h2>
				<div class="o-imgWrap">
					<a href="<?=base_url()?>mg-rx5"><img src="<?=base_url()?>assets/images/img-car__RX5.png" alt=""></a>
				</div>
			</div>
			<div class="o-car">
				<h2>MG 6</h2>
				<div class="o-imgWrap">
					<a href="<?=base_url()?>mg-6"><img src="<?=base_url()?>assets/images/img-car__6.png" alt=""></a>
				</div>
			</div>
			<div class="o-car">
				<h2>MG ZS</h2>
				<div class="o-imgWrap">
					<a href="<?=base_url()?>mg-zs"><img src="<?=base_url()?>assets/images/img-car__ZS.png" alt=""></a>
				</div>
			</div>
		</div>
	</div>

	<div class="c-boxlinks">
		<div class="o-boxlinks__card o-boxlinks__card--full">
			<div class="o-imgWrap">
				<img src="<?=base_url()?>assets/images/bg-home-cars.png" alt="">
			</div>
			<div class="o-textWrap">
				<h2>MG Philippines Offers</h2>
				<p>Check out the latest offers from MG Philippines and drive home an MG today. Great deals on the latest MG models are just a click away.</p>
				<a href="<?=base_url()?>promos" class="o-button o-button--white" >View Promos</a>
				<img class="o-boxlinks__imgBg" src="<?=base_url()?>assets/images/bg-unionjack.png" alt="">
			</div>
			
		</div>
		<a class="o-boxlinks__card" href="<?=base_url()?>history">
			<div class="o-textWrap">
				<h2>A British Icon</h2>
				<p>For the better part of a century, the MG badge has been synonymous with cars that excite, inspire, and are a delight to drive. From the first Super Sports model that was produced in the UK in 1924, up to today's modern, global releases, MG vehicles tap into the natural joy that comes with driving.</p>
			</div>
		</a>
		<a class="o-boxlinks__card" href="<?=base_url()?>news">
			<div class="o-textWrap">
				<h2>MG News</h2>
				<p>Read up on what’s new with <br/>MG Philippines.</p>
			</div>
		</a>
		<a class="o-boxlinks__card" href="<?=base_url()?>dealers">
			<div class="o-textWrap">
				<h2>Dealers</h2>
				<p>Find a dealer near you.</p>
			</div>
		</a>
		<a class="o-boxlinks__card" href="<?=base_url()?>services">
			<div class="o-textWrap">
				<h2>MG Genuine Servicing</h2>
				<p>Your MG will be taken care of by MG-trained technicians, at authorized MG
				dealerships and accredited MG service outlets, using the latest diagnostic
				tools, and fitted with genuine MG parts. Only trust official MG-trained
				service staff members to handle the maintenance of your MG vehicle</p>
			</div>
		</a>
	</div>

	<div class="o-section">
		<h2>Experience an exciting drive with MG</h2>
		<p>To book a test drive, select a vehicle and fill out the customer information form below. <br />An MG representative will be on standby to address your needs.</p>
		<a href="<?=base_url()?>test-drive" class="o-button">Let's Go!</a>
	</div>	
</div>
