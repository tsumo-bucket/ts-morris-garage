<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?=$mdesc?>" />
		<meta name="keywords" content="<?=$mtags?>" />
		<meta property="og:url" content="<?=$ourl?>" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="<?=$otitle?>" />
		<meta property="og:description" content="<?=$odesc?>" />
		<meta property="og:image" content="<?=$oimage?>" />
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i,700,700i,800,800i|Raleway:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/css/mainstyle.css" />
		<link rel="shortcut icon" href="<?=base_url()?>assets/images/favicon.ico" type="image/x-icon">
		<title><?php echo !empty($ptitle) ? $ptitle : 'MG Philippines'; ?></title>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<script>
			var mainurl = '<?=base_url()?>';
			var baseurl = '<?=site_url()?>';
			var module	= '<?=$this->uri->segment(1)?>';
			var method	= '<?=$this->uri->segment(2, 1)?>';
			var dataid	= '<?=$this->uri->segment(3, '')?>';
		</script>
		<!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MJ7KWFG');</script>
        <!-- End Google Tag Manager -->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110254835-2"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-110254835-2');
		</script>
	</head>

	<body>
	    <?= @$match_tag?>
	    <!-- Advenue DMP Container - MG Cars PH Website -->
        <script type="text/javascript" charset="UTF-8">(function(w,d,s,i,c){var f=d.createElement(s);f.type="text/javascript";f.async=true;f.src="https://avd.innity.net/"+i+"/container_"+c+".js";var g=d.getElementsByTagName(s)[0];g.parentNode.insertBefore(f, g);})(window, document, "script", "519", "5bc548ce47e7048a6b7f57ef");</script>
        <!-- End Advenue DMP Container -->
		<!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MJ7KWFG"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
		<!-- HEADER -->
		<header>
			<?=$navs?>
		</header>
		<!-- MAIN WRAP -->
		<div class="o-mainwrap">
			<?=$content?>
			<footer>
				<?=$footer?>
			</footer>
		</div> <!-- end mainwrap -->
	    <!-- JAVASCRIPT -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

		<!-- VENDOR HERE -->
		<!-- enquire --> <script src="https://cdnjs.cloudflare.com/ajax/libs/enquire.js/2.1.2/enquire.min.js" type="text/javascript"></script>
		<!-- popup --><script src="<?=base_url()?>assets/js/vendor/popup.js" type="text/javascript"></script>
		<!-- videoBG --><script src="<?=base_url()?>assets/js/vendor/covervid.min.js" type="text/javascript"></script>
		<!-- slick --><script src="<?=base_url()?>assets/js/vendor/slick.min.js" type="text/javascript"></script>
		<!-- fitvids --><script src="<?=base_url()?>assets/js/vendor/jquery.fitvids.js" type="text/javascript"></script>
		<!-- ellipsis plain js --><script src="<?=base_url()?>assets/js/vendor/ellipsis.min.js" type="text/javascript"></script>
		<!-- END VENDOR -->
		<script src="<?=base_url()?>assets/js/init.min.js" type="text/javascript"></script>
		<script src="<?=base_url()?>assets/js/jscripts.js" type="text/javascript"></script>
	</body>
</html>