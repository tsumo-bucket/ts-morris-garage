<div class="c-header">
    <div class="o-header__left">
        <a href="<?=base_url()?>" class="o-header__logo"></a>
        <div id="js-mobileMenu" class="o-header__menu">
            <img src="<?=base_url()?>assets/images/list-menu.svg" alt="">
        </div>  
    </div>
    <div id="js-headerMenu" class="o-header__right">
        <ul>
            <li>
                <a <?php if($this->uri->segment(1)=='home'|| $this->uri->segment(1)==''){echo 'class="o-header__active"';}?> href="<?=base_url()?>">Home</a>
            </li>
            <li class="o-header__model">
                <a <?php if($this->uri->segment(1)=='mg-zs' || $this->uri->segment(1)=='mg-6' ||  $this->uri->segment(1)=='mg-rx5' ){echo 'class="o-header__active"';}?> href="javascript:void(0)">Models</a>
                <div class="o-header__carWrap">
                    <div class="o-header__cars">
                        <a class="o-header__car" href="<?=base_url()?>mg-5">
                            <div class="o-header__thumbnail">
                                <img src="<?=base_url()?>assets/images/img-car__5.png" alt="">
                                <span>MG 5</span>
                            </div>
                        </a>  
                        <a class="o-header__car" href="<?=base_url()?>mg-rx5">
                            <div class="o-header__thumbnail">
                                <img src="<?=base_url()?>assets/images/img-car__RX5.png" alt="">
                                <span>MG RX5</span> 
                            </div>
                        </a>
                        <a class="o-header__car" href="<?=base_url()?>mg-6">
                            <div class="o-header__thumbnail">
                                <img src="<?=base_url()?>assets/images/img-car__6.png" alt="">
                                <span>MG 6</span>   
                            </div>
                        </a>
                        <a class="o-header__car" href="<?=base_url()?>mg-zs">
                            <div class="o-header__thumbnail">
                                <img src="<?=base_url()?>assets/images/img-car__ZS.png" alt="">
                                <span>MG ZS</span>  
                            </div>
                        </a>
                    </div>  
                </div>
            </li>
            <li>
                <a <?php if($this->uri->segment(1)=='news'){echo 'class="o-header__active"';}?> href="<?=base_url()?>news">News & Activities</a>
            </li>
            <li>
                <a <?php if($this->uri->segment(1)=='history'){echo 'class="o-header__active"';}?> href="<?=base_url()?>history">History</a>
            </li>
            <li>
                <a <?php if($this->uri->segment(1)=='services'){echo 'class="o-header__active"';}?> href="<?=base_url()?>services">Services</a>
            </li>
             <li>
                <a <?php if($this->uri->segment(1)=='prormos'){echo 'class="o-header__active"';}?> href="<?=base_url()?>promos">Promos</a>
            </li>
             <li>
                <a <?php if($this->uri->segment(1)=='dealers'){echo 'class="o-header__active"';}?> href="<?=base_url()?>dealers">Dealers</a>
            </li>
            <li>
                <a class="o-header__btn <?php if($this->uri->segment(1) =='contact-us'){echo 'o-header__active';}?> " href="<?=base_url()?>contact-us">
                    <span style="background-color:white;color: black">Contact Us</span>
                </a>
            </li>
            <li>
                <a class="o-header__btn <?php if($this->uri->segment(1) =='test-drive'){echo 'o-header__active';}?> " href="<?=base_url()?>test-drive">
                    <span >Test Drive</span>
                </a>
            </li>
            <li>
                <a class="o-header__btn" href="https://buyanmg.com/">
                    <span style="background-color:#007bff;color: white">BuyAnMG.com</span>
                </a>
            </li>
        </ul>   
    </div>
    
</div>