<div class="c-footer">
    <div class="o-footer__upper">
        <ul class="o-footer__menu">
            <!--li>
                <a href="<?=base_url()?>request-a-qoute"">Request A Quote</a>
            </li!-->
            <li>
                <a href="<?=base_url()?>faqs">FAQs</a>
            </li>
            <li>
                <a href="<?=base_url()?>privacy-policy">Privacy Policy</a>
            </li>
            <li>
                <a href="<?=base_url()?>terms-of-use">Terms of Use</a>
            </li>
            <li>
                <a href="<?=base_url()?>contact-us">Contact Us</a>
            </li>
            <li>
                <a href="<?=base_url()?>dealers">Dealers</a>
            </li>
        </ul>
        <div class="o-footer__logo">
            <a href="<?=base_url()?>" class="o-imgWrap">
                <img src="<?=base_url()?>assets/images/footer-logo.png" alt="">
            </a>
        </div>
    </div>
    <div class="o-footer__lower">
        <ul class="o-footer__social">
            <li>
                <a href="https://www.facebook.com/pg/OfficialMGPhilippines/">
                    <img src="<?=base_url()?>assets/images/facebook.svg" alt="">
                </a>
            </li>
            <li>
                <a href="https://www.twitter.com/MG_Philippines/">
                    <img src="<?=base_url()?>assets/images/twitter-logo-silhouette.svg" alt="">
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/MG_Philippines/">
                    <img src="<?=base_url()?>assets/images/instagram-logo.svg" alt="">
                </a>
            </li>
            <li>
                <a href="https://www.youtube.com/channel/UCcbuSmvjkUkO46sQATtscOQ?view_as=subscriber">
                    <img src="<?=base_url()?>assets/images/youtube-play-button.svg" alt="">
                </a>
            </li>
        </ul>
        <ul class="o-footer__address">
            <li>MG Philippines - The Covenant Car Company, Inc.</li>
            <li>5th Floor, ALCO Building, 391 Sen. Gil Puyat Avenue,</li>
            <li>Makati City 1200, Metro Manila</li>
            <li class="o-footer__bold">Call MG (02) 5328-4664</li>
        </ul>
    </div>
</div>
<div class="c-cookie">
    <div class="o-cookie__content">
        <div class="o-cookie__leftWrap">
            <h4>Cookies</h4>
            <p>This website uses cookies in order to offer you relevant and updated information. Please accept cookies for optimal performance.</p>
        </div>
        <div class="o-cookie__rightWrap">
            <a class="o-cookie__link" href="<?=base_url()?>privacy-policy">Privacy Policy</a>
            <button class="o-button" onclick="allowBtn();">Allow</button>
        </div>
    </div>
</div>