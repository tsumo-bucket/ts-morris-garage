<!--div class="o-imgWrap o-bg__geometry">
	<img src="<?=base_url()?>assets/images/img-geometry.png" alt="">
</div!-->
<div class="c-article-inner">
	<div class="o-heading__wrap">
		<h1><?=isset($row) ? $row->title : ''?></h1>
	</div>
	<div class="o-article__mainWrap">

		<div class="o-article-inner__date"><?=isset($row) ? date('F d, Y',strtotime($row->article_date)) : ''?></div>

		<div class="o-article-inner__content">
			<?=isset($row) ? $row->details : ''?>					
		</div>

	</div>

	<div class="c-model">
	<div class="o-model__form">
		<div class="o-model__overview" style="padding:0">
			<div class="form-container">
				<!-- <div class="img-container" style="background-image:url(<?=base_url()?>assets/images/promo.jpg)">
				</div> -->
				<div class="form-content">
					<form style="max-width:100%" class="c-form" id="c-Form" action="<?=base_url()?>process/contact" method="post" onsubmit="AJAXSubmit(this); return false;">
						<h3 class="o-heading__h3">Contact Details</h3>
						<div class="o-input__wrap">
							<div class="o-input">
								<input name="name" id="name" class="o-input__text" type="text" required>
								<span class="o-input__label--float">Name (required)</span>
							</div>
						</div>
						<div class="o-input__wrap">
							<div class="o-input">
								<input name="email" id="email" class="o-input__text" type="text" required>
								<span class="o-input__label--float">Email Address (required)</span>
							</div>
						</div>
						<div class="o-input__wrap">
							<div class="o-input">
								<input name="contact" id="contact" class="o-input__text" type="text" required>
								<span class="o-input__label--float">Phone (required)</span>
							</div>
						</div>
						<div class="o-input__wrap">
							<div class="o-input">
								<input name="postcode" id="postcode" class="o-input__text" type="text" required>
								<span class="o-input__label--float">City (required)</span>
							</div>
						</div>
						<div class="o-input__wrap o-input__wrap--checkbox">
							<h3 class="o-heading__h3">Your preferred MG</h3>	
							<div class="o-input__checkboxWrap">
								<input name="model[]" id="model" value="MG6" class="o-input__checkbox" type="checkbox">
								<div class="o-input__checkboxIndicator"></div>
								<label>New MG6</label>
							</div>
							<div class="o-input__checkboxWrap">
								<input name="model[]" id="model" value="MG ZS" class="o-input__checkbox" type="checkbox">
								<div class="o-input__checkboxIndicator"></div>
								<label>New MG ZS</label>
							</div>
							<div class="o-input__checkboxWrap">
								<input name="model[]" id="model" value="MG RX5" class="o-input__checkbox" type="checkbox">
								<div class="o-input__checkboxIndicator"></div>
								<label>New MG RX5</label>
							</div>
							<div class="o-input__checkboxWrap">
								<input name="model[]" id="model" value="MG 5" class="o-input__checkbox" type="checkbox">
								<div class="o-input__checkboxIndicator"></div>
								<label>New MG 5</label>
							</div>	
						</div>
						<h3 class="o-heading__h3">Preferred Dealer</h3>
						<div class="o-input__selectWrap">
							<select class="o-input__select" name="dealer" id="dealer" required>
								<option value="">Select Dealer</option>
								<?php foreach ($dealers as $dealer) { ?>
								<option value="<?= $dealer['name']?>"><?= str_replace('*','',$dealer['name'])?></option>	
								<?php } ?>
							</select>
						</div>
						<h3 class="o-heading__h3" style="margin-top:10px;">Category</h3>
						<div class="o-input__selectWrap">
							<select class="o-input__select" name="category" id="category" required>
								<option value="inquiry">Inquiry</option>
								<option value="customer care">Customer Care</option>
							</select>
						</div>
						<div class="o-textarea__wrap">
							<h3 class="o-heading__h3">Comments</h3>
							<textarea class="o-textarea" name="comments" id="comments" cols="30" rows="10"></textarea>
						</div>

						<div class="g-recaptcha" data-sitekey="6Ldgl3MUAAAAAHg7YX7ddxsxmFKpo14PXf7RGcFv"></div>
						
						<div class="o-notification__wrap">
							<div class="o-notification__error" id="errMsg"></div>
							<div class="o-notification__success" id="successMsg"></div>
						</div>
						<!-- <div class="o-paragraph__wrap">
							<div class="o-paragraph">
								By agreeing to TCCCI - MG Philippines' privacy policies, you acknowledge that you head read, understood, and freely agreed to our privacy policy, and consent to the collection, use, and processing of your personal data. You can opt-out at any time. I. Company: We, The Covenant Car Company, Inc. (TCCCI), the exclusive importer and distributor of Morris Garages (MG) automobiles and parts in the Philippines, conscientiously understand the importance of data security and data privacy of all your personal information. We acknowledge that protecting the data (information) we collected is vital to building trust and accountability with clients who expect privacy. We strictly observe Republic Act No. 10173, or The Data Privacy Act of 2012, and its implementing rules and regulations in collecting, processing, and using sensitive personal information. We adhere to the principles of data privacy, ensure lawful processing, respect the rights of data subjects, and implement security measures. II. Collection of Personal Information: TCCCI collects your personal information you provide directly to it, such as when you request for an Inquiry, Test Drive, Brochure, Vehicle Valuation, Sales and Aftersales Service, or complete a form on our website. Please note that by clicking "I Agree," you voluntarily agreed that we can collect, process, use, and maintain and personal information about and/or relating to you. III. Processing of Personal Information: We process your personal information to make it possible to visit the website and guarantee the long term functionality and security of our systems. The data is often shared by TCCCI to its subsidiaries, business partners, dealers, and service providers to provide you with the best possible products, services, and offers based on your requirements and preferences. The data will also allow our agents to assist you in complying with the requirements of your account or for you to participate in the event. IV. Use of Personal Information: We will use your personal information only for the purposes officially announced in advance or informed at the time of collection and to the extent necessary to carry out business. Except when required by existing laws and regulations, we will not provide personal information to any third party, other than our subsidiaries, including our business partners, dealers, and service providers. Consistently, your personal data shall not be used for any purposes that are contrary to law, morals, and public policy. V. Maintaining/Storage and Protection: We are committed to maintain your personal information accurate and up-to-date, and delete any personal information that is no longer needed without delay. We will not allow any personal information to be leaked by taking it off premises or transmitting it externally. We will implement reasonable and appropriate organizational, physical, and technical security measures for the protection of your personal data. We will institute safeguards that are sufficient to prevent the unauthorized disclosure of personal data by requiring the execution of non-disclosure agreement by our employees, consultants, vendors, suppliers, and contractors, among others.
							</div>
						</div>				 -->
						<button class="o-button" type="submit">Send</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="c-pagination c-pagination--articlesInner" style="margin-bottom:0px">
		<a href="<?=$prev?>" class="o-pagination__prev"><span>Previous</span></a>
		<a href="<?=base_url()?>promos" class="o-pagination__back"><span>Back to Promos</span></a>
		<a href="<?=$next?>" class="o-pagination__next"><span>Next</span></a>
	</div>
	
</div>
