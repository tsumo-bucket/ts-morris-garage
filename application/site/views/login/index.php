<div>
    <div>
        <div>
        	<h2>Login</h2>
			<?php
                $form_attr = array('role' => 'form');
                echo form_open('login',$form_attr);
            ?>
            <div><input type="text" name="email" id="email" placeholder="Email Address"></div>
            <br />
            <div><input type="password" name="password" id="password" placeholder="Password"></div>
            <br />
            <button type="submit" name="login" value="Login">Sign in</button>
            <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>
            <h4 class="text-center" style="color: #e30404;">
            	<?php if(isset($error)) { echo $error; } ?>
        	</h4>
        </div>
    </div>
</div>