<div class="o-imgWrap o-bg__geometry">
	<img src="<?=base_url()?>assets/images/img-geometry.png" alt="">
</div> 
<div class="c-about">
	<div class="o-heading__wrap">
		<h1><?=isset($row) ? $row->title : ''?></h1>
	</div>
	<div class="o-textWrap">
		<?=isset($row) ? $row->description : ''?>
	</div>
</div>