<div class="o-imgWrap o-bg__geometry">
    <img src="<?=base_url()?>assets/images/img-geometry.png" alt="">
</div> 
<div class="c-history">
    <div class="o-heading__wrap">
        <h1>History</h1>
    </div>
    <div class="o-history__mainWrap">
        <div class="o-history__intro">
            <?= $desc?>
        </div>
        <div class="c-timeline">
            <div id="js-timeline" class="o-timeline__content">
                <?php foreach ($histories as $history){?>
                <div class="o-timeline__yearWrap">
                    <div class="o-timeline__year"><?=$history['year']?></div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="c-timeinfo">
            <div class="o-timeinfo__divider"></div>
            <div class="o-timeinfo__wrap">
                <?php 
                $this->load->helper('directory'); 
                $dir = "/uploads/images/milestones"; 
                $map = directory_map($dir);   
                foreach ($histories as $history){
                ?>
                <div class="o-timeinfo__card">
                    <div class="o-timeinfo__upper">
                        <div class="o-imgWrap">
                            <img src="<?=base_url($dir)?>/<?=$history['image']?>" alt="">
                        </div>
                    </div>
                    <div class="o-timeinfo__lower">
                        <div class="o-timeinfo__content">
                            <div class="o-timeinfo__year"><?=$history['year']?></div>
                            <div class="o-timeinfo__title">
                                <?=$history['title']?>
                            </div>
                            <div class="o-timeinfo__detail">
                               <?= nl2br($history['details'])?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>