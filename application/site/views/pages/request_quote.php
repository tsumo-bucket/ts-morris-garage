<div class="o-imgWrap o-bg__geometry">
	<img src="<?=base_url()?>assets/images/img-geometry.png" alt="">
</div> 
<div class="c-book">
	<div class="o-wrap__content">
		<div class="o-heading__wrap">
			<h1 class="o-heading__h1">Request A Quote</h1>
		</div>
		
		<form class="c-form" id="c-Form" action="<?=base_url()?>process/quote" method="post" onsubmit="AJAXSubmit(this); return false;">
			<h3 class="o-heading__h3">Contact Details</h3>
			<div class="o-input__wrap">
				<div class="o-input">
					<input name="name" id="name" class="o-input__text" type="text" required>
					<span class="o-input__label--float">Name (required)</span>
				</div>
			</div>
			<div class="o-input__wrap">
				<div class="o-input">
					<input name="email" id="email" class="o-input__text" type="text" required>
					<span class="o-input__label--float">Email Address (required)</span>
				</div>
			</div>
			<div class="o-input__wrap">
				<div class="o-input">
					<input name="contact" id="contact" class="o-input__text" type="text" required>
					<span class="o-input__label--float">Phone (required)</span>
				</div>
			</div>
			<div class="o-input__wrap">
				<div class="o-input">
					<input name="postcode" id="postcode" class="o-input__text" type="text" required>
					<span class="o-input__label--float">Postcode (required)</span>
				</div>
			</div>
			<div class="o-textarea__wrap">
				<h3 class="o-heading__h3">Comments</h3>
				<textarea class="o-textarea" name="comments" id="comments" cols="30" rows="10"></textarea>
			</div>
			<div class="o-textarea__wrap">
				
			<div class="g-recaptcha" data-sitekey="6Ldgl3MUAAAAAHg7YX7ddxsxmFKpo14PXf7RGcFv"></div>

			<div class="o-notification__wrap">
				<div class="o-notification__error" id="errMsg"></div>
				<div class="o-notification__success" id="successMsg"></div>
			</div>
			<div class="o-paragraph__wrap">
				<div class="o-paragraph">
					<span>Privacy:</span> Your contact details are being collected by MG Motor Philippines to enable us to record the details of the vehicle you are interested in and to consider the purchase of your trade-in vehicle. We may disclose your personal information to third parties to check whether and how, your vehicle is encumbered. From time to time, MG Motor Philippines would like to contact you and to let you know about our products and services, including special offers. If you do not want us to do so, please let us know by contacting us. You can let us know at any time if you no longer wish to be contacted for these purposes. Your consent will remain current until you advise us otherwise. If you do not provide us with your personal information, we may not be able to consider any offer made by you. If you would like to access the information we hold about you or more information on our privacy policy, please contact us at https://mgmotor.com.ph/.
				</div>
			</div>			
			<button class="o-button" type="submit">Send</button>
		</form>
	</div>
</div>