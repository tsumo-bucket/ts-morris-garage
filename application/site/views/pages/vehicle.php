<?php 
$this->load->helper('directory'); 
$dir = "/uploads/images/vehicles"; 
$map = directory_map($dir);    
?>
<div class="c-model">

	<div class="o-model__banner">
		<div class="o-imgWrap">
			<a style="padding:0" href="https://www.mgmotor.com.ph/promos/gear-up-and-go-with-mg-">
				<img src="<?=base_url($dir)?>/<?=$row->banner_image?>" alt="">
			</a>
		</div>
		<ul>
			<li>
				<a class="js-scroll" href="#js-model__overview">Overview</a>
			</li>
			<li>
				<a class="js-scroll" href="#js-model__slider">Product Features</a>
			</li>
			<li>
				<a class="o-model__text--bold" href="<?=base_url()?>test-drive">Book a Test Drive!</a>
			</li>
		</ul>
	</div>
	
	<div id="js-model__overview" class="o-model__overview">
		<div class="o-wrap">
			<div class="o-model__overviewInfo">
				<?=isset($row) ? $row->overview_detail : ''?>
			</div>
			<div class="o-imgWrap">
				<img src="<?=base_url($dir)?>/<?=$row->overview_image?>" alt="">
			</div>						
		</div>
	</div>
	<?php if(isset($row) && (!empty($row->banner_image) || !empty($row->form_image))): ?>
	<?php $image_to_use = $row->banner_image?>
	<?php if(!empty($row->form_image)) { $image_to_use = $row->form_image; }?>
	<div class="o-model__form">
		<div class="o-model__overview">
			<div class="form-container">
				<?php if(isset($row) && $row->slug == 'mg-zs-t'): ?>
					<img src="<?=base_url($dir)?>/<?=$image_to_use?>" class="image-bg-v2">
				<?php else: ?>	
					<div class="img-container" style="background-image:url(<?=base_url($dir)?>/<?=$image_to_use?>)"></div>
				<?php endif; ?>
				<div class="form-content">
					<form class="c-form" id="c-Form" action="<?=base_url()?>process/book" method="post" onsubmit="AJAXSubmit(this); return false;">
						<h3 class="o-heading__h3">Contact Details</h3>
						<div class="o-input__wrap">
							<div class="o-input">
								<input name="name" id="name" class="o-input__text" type="text" required>
								<span class="o-input__label--float">Name (required)</span>
							</div>
						</div>
						<div class="o-input__wrap">
							<div class="o-input">
								<input name="email" id="email" class="o-input__text" type="text" required>
								<span class="o-input__label--float">Email Address (required)</span>
							</div>
						</div>
						<div class="o-input__wrap">
							<div class="o-input">
								<input name="contact" id="contact" class="o-input__text" type="text" required>
								<span class="o-input__label--float">Phone (required)</span>
							</div>
						</div>
						<div class="o-input__wrap">
							<div class="o-input">
								<input name="postcode" id="postcode" class="o-input__text" type="text" required>
								<span class="o-input__label--float">City (required)</span>
							</div>
						</div>
						<input type="hidden" name="model[]" value="<?= $row->model ?>">
						<h3 class="o-heading__h3">Preferred Dealer</h3>
						<div class="o-input__selectWrap">
							<select class="o-input__select" name="dealer" id="dealer" required>
								<option value="">Select Dealer</option>
								<?php foreach ($dealers as $dealer) { ?>
								<option value="<?= $dealer['name']?>"><?= str_replace('*','',$dealer['name'])?></option>	
								<?php } ?>
							</select>
						</div>
						<input name="category" id="category" value="test drive" type="hidden">
						<div class="o-textarea__wrap">
							<h3 class="o-heading__h3">Comments</h3>
							<textarea class="o-textarea" name="comments" id="comments" cols="30" rows="10"></textarea>
						</div>

						<div class="g-recaptcha" data-sitekey="6Ldgl3MUAAAAAHg7YX7ddxsxmFKpo14PXf7RGcFv"></div>
						
						<div class="o-notification__wrap">
							<div class="o-notification__error" id="errMsg"></div>
							<div class="o-notification__success" id="successMsg"></div>
						</div>
						<!-- <div class="o-paragraph__wrap">
							<div class="o-paragraph">
								By agreeing to TCCCI - MG Philippines' privacy policies, you acknowledge that you head read, understood, and freely agreed to our privacy policy, and consent to the collection, use, and processing of your personal data. You can opt-out at any time. I. Company: We, The Covenant Car Company, Inc. (TCCCI), the exclusive importer and distributor of Morris Garages (MG) automobiles and parts in the Philippines, conscientiously understand the importance of data security and data privacy of all your personal information. We acknowledge that protecting the data (information) we collected is vital to building trust and accountability with clients who expect privacy. We strictly observe Republic Act No. 10173, or The Data Privacy Act of 2012, and its implementing rules and regulations in collecting, processing, and using sensitive personal information. We adhere to the principles of data privacy, ensure lawful processing, respect the rights of data subjects, and implement security measures. II. Collection of Personal Information: TCCCI collects your personal information you provide directly to it, such as when you request for an Inquiry, Test Drive, Brochure, Vehicle Valuation, Sales and Aftersales Service, or complete a form on our website. Please note that by clicking "I Agree," you voluntarily agreed that we can collect, process, use, and maintain and personal information about and/or relating to you. III. Processing of Personal Information: We process your personal information to make it possible to visit the website and guarantee the long term functionality and security of our systems. The data is often shared by TCCCI to its subsidiaries, business partners, dealers, and service providers to provide you with the best possible products, services, and offers based on your requirements and preferences. The data will also allow our agents to assist you in complying with the requirements of your account or for you to participate in the event. IV. Use of Personal Information: We will use your personal information only for the purposes officially announced in advance or informed at the time of collection and to the extent necessary to carry out business. Except when required by existing laws and regulations, we will not provide personal information to any third party, other than our subsidiaries, including our business partners, dealers, and service providers. Consistently, your personal data shall not be used for any purposes that are contrary to law, morals, and public policy. V. Maintaining/Storage and Protection: We are committed to maintain your personal information accurate and up-to-date, and delete any personal information that is no longer needed without delay. We will not allow any personal information to be leaked by taking it off premises or transmitting it externally. We will implement reasonable and appropriate organizational, physical, and technical security measures for the protection of your personal data. We will institute safeguards that are sufficient to prevent the unauthorized disclosure of personal data by requiring the execution of non-disclosure agreement by our employees, consultants, vendors, suppliers, and contractors, among others.
							</div>
						</div>				 -->
						<button class="o-button" type="submit">Send</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<!-- model video -->
	<?php if(isset($row) && $row->video){?>
    <div class="o-model__video">
        <?=$row->video?>
    </div>
	<?php } ?>
	<div id="js-model__slider" class="o-model__slider" style="background-image:url('<?=base_url($dir)?>/<?=$row->background_image?>');">

		<div class="o-wrap">

			<div id="js-slider__modelMain" class="c-slider c-slider--modelMain" >
				<?php
				$features = json_decode($row->feature, TRUE); 
				$thumb_images = '';
				$sep = '';
				foreach ($features as $feature) { 
				$thumb_images.= $sep.$feature['image'];
				$sep = ',';
				?>
				<div class="o-slider__wrap">
					<div class="o-imgWrap">
						<img src="<?=base_url($dir)?>/<?=$feature['image']?>" alt="">
					</div>
					<div class="o-slider__info">
						<h4><?=$feature['name']?></h4>
						<p>
							<?=$feature['details']?>
						</p>
					</div>
				</div>	
			<?php } ?>
			</div>
			<div id="js-slider__modelThumbnail" class="c-slider c-slider--modelThumbnail">
			  <?php
			  $imgs = explode(',', $thumb_images);
			  foreach ($imgs as $img) { ?>
			  <div class="o-slider__wrap">
			  	<div class="o-slider__content">
			  		<div class="o-slider__thumbnail" style="background-image:url('<?=base_url($dir)?>/<?=$img?>');"></div>
			  	</div>
			  </div>		
			  <?php	} ?>
			</div>						
		</div>

	</div>

	<?php if(isset($row) && $row->brochure){ ?>
	<a class="o-button" href="javascript:void(0)" onclick="dlFile('<?=$row->brochure?>')" download >Download Brochure</a>
	<?php } ?>


</div>