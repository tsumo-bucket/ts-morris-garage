<div class="o-imgWrap o-bg__geometry">
    <img src="<?=base_url()?>assets/images/img-geometry.png" alt="">
</div> 
<div class="c-dealers">
    <div class="o-heading__wrap">
        <h1>Dealers</h1>
    </div>
    <div class="o-dealers__mainWrap">
        <div class="o-dealers__regionWrap">
            <div class="o-dealers__region">
                <!-- <h2>Metro Manila</h2> -->
                <?php
                foreach ($dealers as $dealer) {
                ?>  
                <div class="o-dealers__cardWrap">
                    <div class="o-dealers__card">
                        <h4><?= $dealer['name']?></h4>
                        <p>
                            <span>Location:</span>
                            <?= $dealer['location']?>
                        </p>
                        <p>
                            <span>Contact No.:</span>
                            <?php if(!empty($dealer['contact'])): ?>
                                <?php $contacts = explode(" | ",$dealer['contact']); ?>
                                <?php foreach($contacts as $contact): ?>
                                    <a href="tel:<?= $contact ?>">
                                        <?= $contact ?>
                                    </a>
                                    <span class="separator"> | </span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </p>
                    </div>	
                </div>
                <?php } ?>
            </div>

            <div class="o-dealers__legend">
                <h4>Legend</h4>
                <p>*Rising Soon</p>
                <!--p>**Temporary Satellite Office</p!-->
            </div>	

        </div>

        

        <div class="o-heading__wrap--sub">
            <h1>Pop-up Stores & Roadshows</h1>
        </div>
        <div class="o-dealers__regionWrap">
            <div class="o-dealers__region">
                <?php
                foreach ($stores as $store) {
                ?>
                <div class="o-dealers__cardWrap">
                    <div class="o-dealers__card">
                        <h4><?=$store['location']?></h4>
                        <p>
                            <span>Date:</span>
                            <?= date('F d, Y', strtotime($store['start_date']))?> to <?= date('F d, Y', strtotime($store['end_date'])) ?>
                        </p>
                    </div>	
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>