<div class="o-imgWrap o-bg__geometry">
	<img src="<?=base_url()?>assets/images/img-geometry.png" alt="">
</div> 
<div class="c-thankyou">
	<div class="o-heading__wrap">
		<h1>Thank you for choosing MG, <?= $data[0]['name'] ?>!</h1>
	</div>
    <div class="o-thankyou__summary">
        <h5>Here's a summary of your contact details:</h5>
        <div class="o-thankyou__infoWrap">
            <div class="o-thankyou__label">Name</div>
            <div class="o-thankyou__info"><?= $data[0]['name'] ?></div>
        </div>
        <div class="o-thankyou__infoWrap">
            <div class="o-thankyou__label">Phone</div>
            <div class="o-thankyou__info"><?= $data[0]['contact'] ?></div>
        </div>
        <div class="o-thankyou__infoWrap">
            <div class="o-thankyou__label">Email Address</div>
            <div class="o-thankyou__info"><?= $data[0]['email'] ?></div>
        </div>
        <div class="o-thankyou__infoWrap">
            <div class="o-thankyou__label">City</div>
            <div class="o-thankyou__info"><?= $data[0]['postcode'] ?></div>
        </div>
        <div class="o-thankyou__infoWrap">
            <div class="o-thankyou__label">Comments</div>
            <div class="o-thankyou__info"><?= $data[0]['comments'] ?></div>
        </div>
    </div>
    <h5 class="o-thankyou__greet">Please expect for an MG agent to contact you shortly. <span>Thank you!</span></h5>
    <div class="o-buttonWrap">
        <a href="<?=base_url()?>" class="o-button">Back to Homepage</a>       
    </div>
</div>