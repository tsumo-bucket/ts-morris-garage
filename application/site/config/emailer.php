<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** Base Configurations ***/
$config['email']['useragent'] = 'CodeIgniter';
$config['email']['protocol'] = 'smtp';
$config['email']['mailpath'] = '/usr/sbin/sendmail';

$config['email']['smtp_host'] = 'mail.mgmotor.com.ph';
$config['email']['smtp_user'] = 'reachus@mgmotor.com.ph';
$config['email']['smtp_pass'] = 'mgm@ilRMS';
$config['email']['smtp_port'] = 587;
$config['email']['smtp_timeout'] = 5;
$config['email']['smtp_crypto'] = 'tls';

$config['email']['wordwrap'] = false;
$config['email']['wrapchars'] = 76;
$config['email']['mailtype'] = 'html';
$config['email']['charset'] = 'utf-8';

$config['email']['validate'] = false;
$config['email']['priority'] = 3;

$config['email']['crlf'] = "\r\n";
$config['email']['newline'] = "\r\n";

$config['email']['bcc_batch_mode'] = false;
$config['email']['bcc_batch_size'] = 200;

$config['email']['from_email'] = "no-reply@mgmotor.com.ph";
$config['email']['from_email_name'] = "MG Philippines";

// Set to preview to return email output
// Set to simulation to simulate sending without actually sending the email
$config['email']['debug'] = false;

//saves the email on the database
$config['email']['store_email']= true;