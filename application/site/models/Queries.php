<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Queries extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	// echo $this->db->last_query(); # display the query

	function get_max($table, $field) {
		$this->db->select_max($field, 'max');
		$sql = $this->db->get($table);

		if($sql->num_rows() > 0) {
			return $sql->row(0);
		} else {
			return false;
		}
	}

	function get_data($params) {
		# extracts array
		extract($params);

		if(!empty($fields)) {
			$this->db->select($fields);
		}

		if(!empty($join)) {
			foreach($join as $j) {
				$this->db->join($j['tbljoin'], $j['onjoin'], 'left');
			}
		}

		if(!empty($where)) {
			$this->db->where($where);
		}

		if(!empty($like)) {
			foreach($like as $k => $v) {
				$this->db->like($k, $v);
			}
		}

		if(!empty($or_like)) {
			foreach($or_like as $k => $v) {
				$this->db->or_like($k, $v);
			}
		}

		if(!empty($groupby)) {
			$this->db->group_by($groupby);
		}

		if(!empty($order)) {
			$this->db->order_by($order);
		}

		if(!empty($limit)) {
			$this->db->limit($limit, $offset);
		}

		// generate FROM TABLE
		$this->db->from($table);
		$sql = $this->db->get();

		// echo $this->db->last_query();

		if($sql->num_rows() > 0) {
			if(!empty($row)) {
				$rs = $sql->row(0);
			} else {
				$rs['rows'] = $this->count_rows($params);
				$rs['data'] = $sql->result_array();
			}

			return $rs;
		} else {
			return false;
		}
	}

	// count rows of the query
	function count_rows($params) {
		# extracts array
		extract($params);

		if(!empty($fields)) {
			$this->db->select($fields);
		}

		if(!empty($join)) {
			foreach($join as $j) {
				$this->db->join($j['tbljoin'], $j['onjoin'], 'left');
			}
		}

		if(!empty($where)) {
			$this->db->where($where);
		}

		if(!empty($like)) {
			$this->db->like($like, 'after');
		}

		if(!empty($groupby)) {
			$this->db->group_by($groupby);
		}

		if(!empty($order)) {
			$this->db->order_by($order);
		}

		// generate FROM TABLE
		$this->db->from($table);
		$sql = $this->db->get();

		return $sql->num_rows();
	}

	function check_if_exist($params) {
		extract($params);

		$exist = false;

		if(!empty($uniqfld) && count($uniqfld) > 0) {
			for($i = 0; $i < count($uniqfld); $i++) {
				$this->db->where($uniqfld[$i], $data[$uniqfld[$i]]);
			}

			if(!empty($dataid)) {
				$this->db->where($tbluid . ' <>', $dataid);
			}

			$this->db->select($tbluid);
			$sql = $this->db->get($table);

			if($sql->num_rows() > 0) {
				$exist = true;
			}
		}

		return $exist;
	}

	function insert($params) {
		extract($params);

		$exist = $this->check_if_exist($params);

		if($exist == false) {
			$this->db->insert($table, $data);

			if($this->db->affected_rows() > 0) {
				return $this->db->insert_id();
			} else {
				return 0;
			}
		} else {
			return 'exist';
		}
	}

	function update($params) {
		extract($params);
		
		$exist = $this->check_if_exist($params);

		if($exist == false) {
			$this->db->where($tbluid . ' = ', $dataid);
			$this->db->update($table, $data);

			if($this->db->affected_rows() > 0) {
				return $dataid;
			} else {
				return 0;
			}
		} else {
			return 'exist';
		}
	}

	function delete($params) {
		extract($params);

		$this->db->where($uniqid, $dataid);
		$this->db->delete($table);

		if($this->db->affected_rows() > 0) {
			return $dataid;
		} else {
			return 0;
		}
	}

	function get_user($email, $password) {
        $this->db->select('uid,username,email,level');
        $this->db->where('email', $email);
        $this->db->where('password', md5($password));
        $this->db->where('isactive', TRUE);
        $query = $this->db->get('tbl_users');
        return $query->row();
    }

    function get_fbuser($fbuid) {
        $this->db->select('id','fbuid');
        $this->db->where('fbuid', $fbuid);
        $query = $this->db->get('tbl_fbusers');
        return $query->row();
    }

}

?>