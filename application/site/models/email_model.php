<?php
// Extend Base_model instead of CI_model
class Email_model extends CI_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('id', 'mail_to', 'cc', 'bcc', 'subject', 'message', 'date_sent','from','from_name','status','debug');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('email', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.

}