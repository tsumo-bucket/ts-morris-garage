<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Initials extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	# TO CHECK IF ALPHANUMERIC, SPACE AND "-,._#" ONLY 
	public function mixed($data) {
		 if($data){
			if(!preg_match('/[^a-z0-9 .\-\,\#]/i', $data)){
		  		return $data;
			}
		}
	}

	# TO CHECK IF ALPHANUMERIC AND SPACE ONLY 
	public function alphanumeric($data) {
		 if($data){
			if(!preg_match('/[^a-z\ \0-9]/i', $data)){
			  return $data;
			}
		}
	}

	# TO CHECK IF ALPHA, SPACE AND "." ONLY 
	public function alpha($data) {
		if($data){
			if(!preg_match('/[^a-zÑñ\. ]/i', $data)){
			  return $data;
			}
		}
	}


	# this will validate email address format
	public function email($data){
		if(filter_var($data, FILTER_VALIDATE_EMAIL) === FALSE){
	        return FALSE;
	    }

	    $domain = explode("@", $data, 2);
	    if(checkdnsrr($domain[1])){
	        return $data;
	    }
    }

    public function mobile($data) {
		 if($data){
			if(!preg_match('/[^0-9+()]/i', $data)){
		  		return $data;
			}
		}
	}

    # TO CHECK IF NUMERIC ONLY 
	public function numeric($data){
		if($data){
			if(!preg_match('/[^0-9]/i', $data)){
			  return $data;
			}
		}
    }

    // Filter data from XSS
	# Applicable for RTF forms
	function anti_xss_low($data){
		if (get_magic_quotes_gpc()){
			$data=stripslashes($data);
		}
	
		// Clean Links
		preg_match_all('@<a[^>]*?>.*?</a>@si',$data , $links);
		for($i=0; $i<(count($links[0])); $i++){
			preg_match("/\< *a[^\>]*href *= *[\"\']{0,1}([^\"\'\ >]*)/i",$links[0][$i],$href);
			preg_match("@<a[^>]*?>@si",$links[0][$i],$fullhref);

			if(!eregi("javascript",$href[1])&&!eregi("#",$href[1])&&!eregi("\(",$href[1])&&!eregi("\)",$href[1])){
				$data=str_replace($fullhref,"[a href=\"".$href[1]."\"]",$data);
			} else {
				$data=str_replace($links[0][$i]," ",$data);
			}
		}
		$data=str_replace("</a>","[/a]",$data);
		$data=str_replace("</A>","[/A]",$data);

		// Clean Font
		preg_match_all('@<font([^>]+)>@si',$data , $font);
		for($f=0; ($f<=count($font[0])-1); $f++){
			$data = str_replace($font[0][$f], "[font".$font[1][$f]."]", $data);
		}
		$data=str_replace("</font>","[/font]",$data);
		$data=str_replace("</FONT>","[/FONT]",$data);

		// Clean Image
		preg_match_all('@<img([^>]+)>@si',$data , $image);
		for($p=0; ($p<=count($image[0])-1); $p++){
			$data = str_replace($image[0][$p], "[img".$image[1][$p]."]", $data);
		}

		// Clean data and Strip Tags
		// PARSE CONENT REMOVE JAVASCRIPT AND CSS
		$search = array(
			'@<script[^>]*?>.*?</script>@si',  // Strip out javascript
			'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
			'@<![\s\S]*?--[ \t\n\r]*>@',        // Strip multi-line comments including CDATA            
			'@<link[^>]*?>@si'
		);

		//    allowed tags
		$allowed = array(
			'@<img.*?/>@si',
			'@<font.*?>@si',
			'@<br.*?/>@si','@<br.*?>@si',
			'@<hr.*?/>@si','@<hr.*?>@si',
			'@<ul.*?>@si','@</ul>@si',
			'@<li.*?>@si','@</li>@si',
			'@<ol.*?>@si','@</ol>@si',
			'@<a.*?>@si','@</a>@si',
			'@<b.*?>@si','@</b>@si',
			'@<center.*?>@si','@</center>@si',
			'@<em.*?>@si','@</em>@si',
			'@<h1.*?>@si','@</h1>@si',
			'@<h2.*?>@si','@</h2>@si',
			'@<h3.*?>@si','@</h3>@si',
			'@<h4.*?>@si','@</h4>@si',
			'@<h5.*?>@si','@</h5>@si',
			'@<h6.*?>@si','@</h6>@si',
			'@<i.*?>@si','@</i>@si',
			'@<p.*?>@si','@</p>@si',
			'@<q.*?>@si','@</q>@si',
			'@<small.*?>@si','@</small>@si',
			'@<strike.*?>@si','@</strike>@si',
			'@<strong.*?>@si','@</strong>@si',
			'@<sub.*?>@si','@</sub>@si',
			'@<sup.*?>@si','@</sup>@si',
			'@<u.*?>@si','@</u>@si',
		);
		$areplace = array(
			'[img /]',
			'[font]',
			'[br /]','[br]',
			'[hr /]','[hr]',
			'[ul]','[/ul]',
			'[li]','[/li]',
			'[ol]','[/ol]',
			'[a]','[/a]',
			'[b]','[/b]',
			'[center]','[/center]',
			'[em]','[/em]',
			'[h1]','[/h1]',
			'[h2]','[/h2]',
			'[h3]','[/h3]',
			'[h4]','[/h4]',
			'[h5]','[/h5]',
			'[h6]','[/h6]',
			'[i]','[/i]',
			'[p]','[/p]',
			'[q]','[/q]',
			'[small]','[/small]',
			'[strike]','[/strike]',
			'[strong]','[/strong]',
			'[sub]','[/sub]',
			'[sup]','[/sup]',
			'[u]','[/u]',		
			);
		$rallowed = array(
			'@\[img /\]@si',
			'@\[font\]@si',
			'@\[br /\]@si','@\[br\]@si',
			'@\[hr /\]@si','@\[hr\]@si',
			'@\[ul\]@si','@\[/ul\]@si',
			'@\[li\]@si','@\[/li\]@si',
			'@\[ol\]@si','@\[/ol\]@si',
			'@\[a\]@si','@\[/a\]@si',
			'@\[b\]@si','@\[/b\]@si',
			'@\[center\]@si','@\[/center\]@si',
			'@\[em\]@si','@\[/em\]@si',
			'@\[h1\]@si','@\[/h1\]@si',
			'@\[h2\]@si','@\[/h2\]@si',
			'@\[h3\]@si','@\[/h3\]@si',
			'@\[h4\]@si','@\[/h4\]@si',
			'@\[h5\]@si','@\[/h5\]@si',
			'@\[h6\]@si','@\[/h6\]@si',
			'@\[i\]@si','@\[/i\]@si',
			'@\[p\]@si','@\[/p\]@si',
			'@\[q\]@si','@\[/q\]@si',
			'@\[small\]@si','@\[/small\]@si',
			'@\[strike\]@si','@\[/strike\]@si',
			'@\[strong\]@si','@\[/strong\]@si',
			'@\[sub\]@si','@\[/sub\]@si',
			'@\[sup\]@si','@\[/sup\]@si',
			'@\[u\]@si','@\[/u\]@si',
		);
		$rreplace = array(
			'<img />',
			'<font>',
			'<br />','<br>',
			'<hr />','<hr>',
			'<ul>','</ul>',
			'<li>','</li>',
			'<ol>','</ol>',
			'<a>','</a>',
			'<b>','</b>',
			'<center>','</center>',
			'<em>','</em>',
			'<h1>','</h1>',
			'<h2>','</h2>',
			'<h3>','</h3>',
			'<h4>','</h4>',
			'<h5>','</h5>',
			'<h6>','</h6>',
			'<i>','</i>',
			'<p>','</p>',
			'<q>','</q>',
			'<small>','</small>',
			'<strike>','</strike>',
			'<strong>','</strong>',
			'<sub>','</sub>',
			'<sup>','</sup>',
			'<u>','</u>',
		);

		$data=preg_replace($rallowed,$rreplace,strip_tags(preg_replace($search,' ',preg_replace($allowed,$areplace,$data))));

		//    Restore Links
		preg_match_all('@\[a[^>]*?\].*?@si',$data,$links);
		for($i=0; $i<(count($links[0])); $i++){
			preg_match("/\[ *a[^\>]*href *= *[\"\']{0,1}([^\"\'\ \]]*)/i",$links[0][$i],$href);
			$data=str_replace($links[0][$i],"<a href=\"".$href[1]."\" >",$data);
		}

		//	Restore Font
		preg_match_all('@\[font([^\]]+)\]@si',$data , $fonts);
		for($f2=0; $f2<count($fonts[0]); $f2++){
			$data = str_replace($fonts[0][$f2], "<font".$fonts[1][$f2].">", $data);
		}
		
		//	Restore Image
		preg_match_all('@\[img([^\]]+)\]@si',$data , $images);
		for($p2=0; $p2<count($images[0]); $p2++){
			$data = str_replace($images[0][$p2], "<img".$images[1][$p2].">", $data);
		}

		$data = str_replace("[/font]","</font>",$data);
		$data = str_replace("[/a]","</a>",$data);
		return $data;
	}

    # Applicable for all forms except RTF
    function anti_xss_high($data){
		if (get_magic_quotes_gpc()){
			return htmlentities(strip_tags(stripslashes($data)));
		} else {
			return htmlentities(strip_tags($data));
		}
	}

	# this will generation options
	function generate_options($arr, $sel = false, $both = false) {
		$opts = '<option value="">- select -</option>';

		if($arr) {
			foreach($arr['data'] as $i => $a) {
				$keys = array_keys($a);

				if($sel == $a[$keys[0]]) {
					$s = ' selected="selected"';
				} else {
					$s = '';
				}

				if(count($keys) == 2) {
					if($both != false) {
						$opts .= '<option value="' . $a[$keys[0]] . '"' . $s . '>' . $a[$keys[0]] . ' - ' . stripslashes($a[$keys[1]]) . '</option>';
					} else {
						$opts .= '<option value="' . $a[$keys[0]] . '"' . $s . '>' . stripslashes($a[$keys[1]]) . '</option>';
					}
				} else {
					$opts .= '<option value="' . $a[$keys[0]] . '"' . $s . '>' . $a[$keys[0]] . '</option>';
				}
			}
		}

		return $opts;
	}

	# array to object
	function array_to_object($array) {
		foreach($array as $key => $value) {
			if(is_array($value)) {
				$array[$key] = $this->array_to_object($value);
			}
		}

		return (object)$array;
	}

	# pagination settings
	function pagination_settings($page, $limit = false) {
		$limit	= $limit ? $limit : 10; // limit rows per record
		# page computations
		$isnum		= is_numeric($page) ? $page : 1;
		$cur_page	= ($isnum < 1) ? 1 : $isnum;
		$offset		= ($cur_page - 1) * $limit;

		$ps['limit']	= $limit;
		$ps['offset']	= $offset;
		$ps['cur_page']	= $cur_page;

		return $ps;
	}

	# display pagination
	function display_pagination($total_rows, $limit, $curpage, $page_name) {
		// vars
		$p_int	= 4;
		$pages	= ceil($total_rows / $limit);
		$pp 	= (!empty($curpage)) ? $curpage : 1;

		// default value of pages
		$p_from	 = 1;
		$p_to	 = $pages;

		// page computations
		if($pages >= ($p_int * 2)) {
			if($pp + $p_int > $pages) {
				$p_from	 	= $pages - $p_int;
				$p_to		= $pages;

				if($pp < $pages) {
					$p_from = ($p_from - 1) + ($pp - $pages) + 1;
				}
			} else {
				$p_from		= $pp - $p_int;
				$p_to		= $pp + $p_int;
			}

			// this computation is for first pages.
			if($pp - $p_int < 1) {
				$p_from		= 1;
				$p_to		= ($p_int + 1);

				if($pp > 1) {
					$p_to	= ($p_int + 1) + ($pp - 1);
				}
			}
		}
		
		$page	= '<ul class="pagination pagination-md">';
		$page  .= '<li><a href="' . site_url($page_name . '/' . (($pp > 1) ? $pp - 1 : 1)) . '" title="Previous">&laquo;</a></li>';

		for($p = $p_from; $p <= $p_to; $p++) {
			if($pp == $p) { $selected = ' class="active"'; }
			else { $selected = ''; }

			$page .= '<li><a href="' . site_url($page_name . '/' . $p) . '"' . $selected . '>' . $p . '</a></li>';
		}

		$page  .= '<li><a href="' . site_url($page_name . '/' . (($pp < $pages) ? $pp + 1 : $pages)) . '">&raquo;</a></li>';
		
		$page  .= '</ul>';

		return $page;
	}

	function teaser($string,$len=200){
		//Search the end of a word after [, int offset] and set the result as limit
		if($string){
			$limit=strip_tags($string);
			$source_char=strlen($string);
			$maxchar = $len;
			
			if($source_char >= $maxchar){
				$limit = strpos(strip_tags($string), " ",$maxchar);
				
				if($limit){ // if string is larger than [, int offset]
						//Use $limit to replace with ...Text
						return substr_replace(strip_tags($string), "...",$limit);
				}
				else{ //just return text without html tags
					return strip_tags($string);
				}
			}else{
				return strip_tags($string);
			}
		}
	}

	function limit_words($words, $limit, $append = ' &hellip;') {
       // Add 1 to the specified limit becuase arrays start at 0
       $limit = $limit+1;
       // Store each individual word as an array element
       // Up to the limit
       $words = explode(' ', $words, $limit);
       // Shorten the array by 1 because that final element will be the sum of all the words after the limit
       array_pop($words);
       // Implode the array for output, and append an ellipse
       $words = implode(' ', $words) . $append;
       // Return the result
       return $words;
}

}

?>