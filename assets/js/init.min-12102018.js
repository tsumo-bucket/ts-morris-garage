/*
 CoreGruntTemplate 2018-12-10 
*/

$(document).ready(function() {
    $("#js-mobileMenu").click(function() {
        $("#js-headerMenu").animate({
            height: "toggle"
        }, 300);
    }), enquire.register("screen and (min-width: 1000px)", {
        setup: function() {},
        match: function() {
            $("#js-headerMenu").css({
                display: ""
            });
        },
        unmatch: function() {}
    }), $("#js-slider__home").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !0,
        arrows: !1,
        infinite: !0,
        autoplay: !0,
        autoplaySpeed: 1e4
    }), $("#js-slider__modelMain").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !1,
        arrows: !1,
        autoplay: !1,
        autoplaySpeed: 3e3,
        swipe: !1,
        infinite: !1,
        adaptiveHeight: !0,
        asNavFor: "#js-slider__modelThumbnail"
    }), $("#js-slider__modelThumbnail").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: !1,
        arrows: !0,
        autoplay: !1,
        autoplaySpeed: 3e3,
        mobileFirst: !0,
        infinite: !1,
        focusOnSelect: !0,
        asNavFor: "#js-slider__modelMain",
        responsive: [ {
            breakpoint: 500,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 5
            }
        }, {
            breakpoint: 700,
            settings: {
                slidesToShow: 6
            }
        }, {
            breakpoint: 800,
            settings: {
                slidesToShow: 7
            }
        }, {
            breakpoint: 900,
            settings: {
                slidesToShow: 8
            }
        }, {
            breakpoint: 1e3,
            settings: {
                slidesToShow: 9
            }
        }, {
            breakpoint: 1100,
            settings: {
                slidesToShow: 10
            }
        }, {
            breakpoint: 1300,
            settings: {
                slidesToShow: 11
            }
        } ]
    });
    var e = $("#js-slider__home");
    e.find(".o-slider__wrap").length <= 1 && e.find(".slick-dots").hide(), $(".js-scroll").on("click", function(e) {
        if ("" !== this.hash) {
            e.preventDefault();
            var s = this.hash;
            $("html, body").animate({
                scrollTop: $(s).offset().top
            }, 800, function() {
                window.location.hash = s;
            });
        }
    }), Ellipsis({
        ellipsis: "…",
        debounce: 0,
        responsive: !0,
        className: ".o-article__title",
        lines: 4,
        portrait: null,
        break_word: !0
    }), Ellipsis({
        ellipsis: "…",
        debounce: 0,
        responsive: !0,
        className: ".o-article__excerpt",
        lines: 8,
        portrait: null,
        break_word: !0
    }), $(".o-article-inner__content").fitVids();
});