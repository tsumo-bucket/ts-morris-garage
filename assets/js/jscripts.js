
checkCookie();

$('.c-article').find('.c-pagination').find('a[rel=next]').removeClass('o-pagination__page');
$('.c-article').find('.c-pagination').find('a[rel=next]').addClass('o-pagination__next');
$('.c-article').find('.c-pagination').find('a[rel=prev]').removeClass('o-pagination__page');
$('.c-article').find('.c-pagination').find('a[rel=prev]').addClass('o-pagination__prev');

function AJAXSubmit (oFormElement){
	if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var d = JSON.parse(this.responseText);
            document.getElementById("errMsg").innerHTML = '';
            if(d.error != null){
            	document.getElementById("errMsg").innerHTML = d.error;
            	grecaptcha.reset();
            } else if (module != 'test-drive' && module != 'contact-us' && module != 'mg-5' && module != 'mg-rx5' && module != 'mg-6' && module != 'mg-zs' && module != 'promos') {
                document.getElementById("c-Form").reset();
                document.getElementById("successMsg").innerHTML = d.msg;
                setTimeout(function(){ location.reload(); }, 3000);
            } else {
                if(module == 'contact-us' || module == 'promos'){
                    // window.location.href = baseurl + module +'/thank-you?d=' + d.rec;
                    window.location.href = baseurl +'contact-us/thank-you?d=' + d.rec;

                }else{
                    window.location.href = baseurl+'thank-you?d='+d.rec;
                }
            }
        }
    };
    xmlhttp.open("post",oFormElement.action,true);
    xmlhttp.send(new FormData(oFormElement));
}

function checkCookie() {
    var cookie_enabled = getCookie("mgcars_cookiesenbaled");
    if (cookie_enabled != "") {
        document.querySelector('.c-cookie').style.display = 'none';
    }else{
        document.querySelector('.c-cookie').style.display = 'block';
    }
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function allowBtn(){
    var d = new Date();
    var cname = 'mgcars_cookiesenbaled';
    var cvalue = 'true';
    var exdays = '1';
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    document.querySelector('.c-cookie').style.display = 'none';
}

function dlFile(file){
    window.location.href = baseurl+'process/download/'+file;
}